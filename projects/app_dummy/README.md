# APP DUMMY

Aplicacion generada unicamente para realizar pruebas de la librería `@rid/design-system`.

## Visualizar proyecto

Ejecutar el comando `dummy:serve`. Se abrirá una ventana en el navegador donde podrás visualizar los distintos ejemplos de la implementacion de la librería.
