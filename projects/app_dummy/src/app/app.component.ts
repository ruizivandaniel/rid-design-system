import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { RidResponsiveService } from '@rid/core';
import { filter } from 'rxjs';

export type BzRouterLinkType = 'components' | 'utils';

export interface BzRouterLinkData {
  path: string;
  label: string;
}

export interface BzRouterLink {
  type: BzRouterLinkType;
  links: BzRouterLinkData[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = '';
  showResponsiveMenu: boolean = false;
  routerLinks: BzRouterLink[] = [
    {
      type: 'components',
      links: [
        // yo:routerlinks
        { path: '/label', label: 'label' },
        { path: '/check-group', label: 'check-group' },
        { path: '/filters', label: 'filters' },
        { path: '/copy', label: 'copy' },
        { path: '/contextual-menu', label: 'contextual-menu' },
        { path: '/check', label: 'check' },
        { path: '/floating-menu', label: 'floating-menu' },
        { path: '/stepper', label: 'stepper' },
        { path: '/upload-file', label: 'upload-file' },
        { path: '/input', label: 'input' },
        { path: '/switch', label: 'switch' },
        { path: '/chips', label: 'chips' },
        { path: '/pagination', label: 'pagination' },
        { path: '/dynamic-table', label: 'dynamic-table' },
        { path: '/dropdown', label: 'dropdown' },
        { path: '/banner', label: 'banner' },
        { path: '/alert', label: 'alert' },
        { path: '/loader', label: 'loader' },
        { path: '/button', label: 'button' },
        { path: '/card', label: 'card' },
        { path: '/tabs', label: 'tabs' },
        { path: '/accordion', label: 'accordion' },
        { path: '/calendar', label: 'calendar' },
        { path: '/carrousel', label: 'carrousel' },
        { path: '/text-truncate', label: 'text-truncate' },
        { path: '/pin-code', label: 'pin-code' },
        { path: '/tooltip', label: 'tooltip' },
      ],
    },
    {
      type: 'utils',
      links: [
        { path: '/kbd', label: 'kbd' },
        { path: '/modal', label: 'modal' },
        { path: '/icons', label: 'icons' },
        { path: '/aspect-ratio', label: 'aspect-ratio' },
        { path: '/responsive-service', label: 'responsive-service' },
        { path: '/logger', label: 'logger' },
      ]
    }
  ];

  get isResponsive(): boolean {
    return this._responsiveService.is(['MOBILE', 'NOOTEBOOK'])
  }

  constructor(
    private _router: Router,
    private _responsiveService: RidResponsiveService,
  ) { }

  ngOnInit(): void {
    this.routerLinks.forEach((data: BzRouterLink) =>
      data.links.sort(this._sortLinks));
    this._router
      .events
      .pipe(filter(n => n instanceof NavigationEnd))
      .subscribe((navigation: any) => {
        this.title = navigation.url;
      });
  }

  /** Order alfabetico */
  private _sortLinks(a: BzRouterLinkData, b: BzRouterLinkData): number {
    return a.label === b.label ? 0 : a.label > b.label ? 1 : -1;
  }

}
