import { Directive, Input, TemplateRef } from '@angular/core';
import { Interaction } from '../models/interaction.model';

@Directive({ selector: 'appTesterPage, [appTesterPage]' })
export class ComponenteTesterPageDirective {

  /** Label de la pagina. */
  @Input('appTesterPage') label: string = '';
  /** Descripcion de la pagina. */
  @Input() description: string = '';
  /** Listado de interacciones. */
  @Input() interactions: Interaction<any>[] = [];

  get template(): TemplateRef<any> {
    return this._el;
  }

  constructor(private _el: TemplateRef<any>) { }

}
