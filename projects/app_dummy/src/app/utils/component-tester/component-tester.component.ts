import { Component, ContentChildren, Input, OnInit, QueryList, TemplateRef, } from '@angular/core';
import { ComponenteTesterPageDirective } from './directives/component-tester-page.directive';

@Component({
  selector: 'app-component-tester',
  templateUrl: './component-tester.component.html',
})
export class ComponentTesterComponent implements OnInit {

  /** Titulo del tester. */
  @Input() title!: string;
  /** Descripcion del tester. */
  @Input() description!: string;

  @ContentChildren(TemplateRef<any>, {
    read: ComponenteTesterPageDirective,
    emitDistinctChangesOnly: true,
  })
  private _pages!: QueryList<ComponenteTesterPageDirective>;

  selectedPage: string = '';

  get pages(): ComponenteTesterPageDirective[] {
    return this._pages.length ? this._pages.toArray() : [];
  }

  constructor(
    // private _scrollService: RidScrollService,
  ) { }

  ngOnInit(): void {
    /** Implementacion de scrollservice. */
    // this._scrollService.scrolled('idErroneoQueNoVasAEscuchar')
    //   .subscribe((event: RidScrollEvent) => {
    //     console.log('suscripto al scroll: ', event);
    //   });
    // this._scrollService.scrolled('routerOutlet')
    //   .subscribe((event: RidScrollEvent) => {
    //     console.log('suscripto al scroll: ', event);
    //   });
    // this._scrollService.scrolledToBottom('routerOutlet')
    //   .subscribe((event: RidScrollEvent) => {
    //     console.log('suscripto a bottom: ', event);
    //   });
    // this._scrollService.scrolledToTop('routerOutlet')
    //   .subscribe((event: RidScrollEvent) => {
    //     console.log('suscripto a top: ', event);
    //   });
  }

  selectPage(label: string): void {
    this.selectedPage = label;
  }


}
