export type InteractionType = 'boolean' | 'select' | 'button' | 'inputText' | 'inputNumber';


export interface Interaction<T> {
  /** Label de la interaccion. */
  label: string;
  /** Descripcion de la interaccion. */
  description: string;
  /** Tipo de la interaccion. */
  type: InteractionType;
  /** Valor inicial de la interaccion. */
  init?: T;
  /** Funcion que se ejecuta al modificarse la interaccion. */
  valueChanged: (newValue: T) => void;
}

/**
 * Override de props para las interacciones de tipo
 * boolean.
 */
export interface InteractionBoolean extends Interaction<boolean> {
  type: 'boolean';
}

/**
 * Override de props para las interacciones de tipo
 * button.
 */
export interface InteractionButton extends Interaction<void> {
  type: 'button';
  init: undefined;
  valueChanged: () => void
}

/**
 * Override de props para las interacciones de tipo
 * select.
 */
export interface InteractionSelect extends Interaction<any> {
  type: 'select';
  /** Opciones del selector. */
  options: string[];
}

/**
 * Override de props para las interacciones de tipo
 * input text.
 */
export interface InteractionInputText extends Interaction<string> {
  type: 'inputText';
}

/**
 * Override de props para las interacciones de tipo
 * input number.
 */
export interface InteractionInputNumber extends Interaction<number> {
  type: 'inputNumber';
}
