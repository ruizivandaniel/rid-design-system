import { Component, Input, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-array-input',
  template: `
    <input type="text" class="form-control form-control-sm me-2" [style.width.px]="200"
      [formControl]="inputCtrl" [placeholder]="placeholder">
  `
})
export class ArrayInputComponent implements OnInit {
  @Input() default: string = '';
  @Input() placeholder: string = '';
  @Input() fromId: string = '';

  inputCtrl = new FormControl('', [Validators.min(0), Validators.required]);

  get value(): string {
    return this.inputCtrl.value || '';
  }

  ngOnInit(): void {
    if (this.default) {
      this.inputCtrl.setValue(this.default);
    }
  }
}
