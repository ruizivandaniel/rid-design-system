import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ArrayInputComponent } from './components/array-input/array-input.component';
import { v4 } from 'uuid';

export type InteractionInputType = 'switch' | 'input' | 'array' | 'inputNumber' | 'select' | 'button';

export interface InteractionSelectOption {
  label: string;
  value?: any;
}

export interface InteractionInput {
  id?: string;
  label: string | string[];
  type: InteractionInputType;
  value?: any;
  placeholder?: string | string[];
  /** Solo para el select. */
  options?: InteractionSelectOption[];
  change: (event: any) => void;
}

@Component({
  selector: 'app-interactions',
  templateUrl: 'interactions.component.html'
})
export class InteractionsComponent implements OnInit {

  @Input() inputs: InteractionInput[] = [];

  @ViewChildren(ArrayInputComponent, { emitDistinctChangesOnly: true, read: ArrayInputComponent })
  private _inputsComps!: QueryList<ArrayInputComponent>;

  ngOnInit(): void {
    this.inputs.map(i => {
      i.id = v4();
      if (!i.value) {
        if (i.type === 'switch') {
          i.value = false;
        }
        if (i.type === 'input') {
          i.value = '';
        }
        if (i.type === 'array') {
          i.value = [];
        }
      }
    });
  }

  emitArrayInputChange(input: InteractionInput): void {
    const value = this._inputsComps.toArray().filter(i => i.fromId === input.id).map(i => i.value);
    input.change(value);
  }

  emitSelectInputChange(input: InteractionInput, event: any): void {
    input.change(event.target.value);
  }
}
