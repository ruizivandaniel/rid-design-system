import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RidDesignSystemModule } from '@rid/design-system';
import { RidCoreModule } from '@rid/core';
import { CommonModule } from '@angular/common';

// yo:components
import { LabelImpComponent } from './components/label-imp/label-imp.component';
import { KbdImpComponent } from './components/kbd-imp/kbd-imp.component';
import { FiltersImpComponent } from './components/filters-imp/filters-imp.component';
import { CopyImpComponent } from './components/copy-imp/copy-imp.component';
import { ContextualMenuImpComponent } from './components/contextual-menu-imp/contextual-menu-imp.component';
import { CheckImpComponent } from './components/check-imp/check-imp.component';
import { FloatingMenuImpComponent } from './components/floating-menu-imp/floating-menu-imp.component';
import { StepperImpComponent } from './components/stepper-imp/stepper-imp.component';
import { UploadFileImpComponent } from './components/upload-file-imp/upload-file-imp.component';
import { InputImpComponent } from './components/input-imp/input-imp.component';
import { SwitchImpComponent } from './components/switch-imp/switch-imp.component';
import { ChipsImpComponent } from './components/chips-imp/chips-imp.component';
import { PaginationImpComponent } from './components/pagination-imp/pagination-imp.component';
import { DynamicTableImpComponent } from './components/dynamic-table-imp/dynamic-table-imp.component';
import { DropdownImpComponent } from './components/dropdown-imp/dropdown-imp.component';
import { BannerImpComponent } from './components/banner-imp/banner-imp.component';
import { ModalImpComponent } from './components/modal-imp/modal-imp.component';
import { AlertImpComponent } from './components/alert-imp/alert-imp.component';
import { LoaderImpComponent } from './components/loader-imp/loader-imp.component';
import { ButtonImpComponent } from './components/button-imp/button-imp.component';
import { CardImpComponent } from './components/card-imp/card-imp.component';
import { TabsImpComponent } from './components/tabs-imp/tabs-imp.component';
import { AccordionImpComponent } from './components/accordion-imp/accordion-imp.component';
import { CalendarImpComponent } from './components/calendar-imp/calendar-imp.component';
import { CarrouselImpComponent } from './components/carrousel-imp/carrousel-imp.component';
import { TextTruncateImpComponent } from './components/text-truncate-imp/text-truncate-imp.component';
import { PinCodeImpComponent } from './components/pin-code-imp/pin-code-imp.component';
import { HomeComponent } from './components/home/home.component';
import { AppComponent } from './app.component';
import { AspectRatioImpComponent } from './components/aspect-ratio-imp/aspect-ratio-imp.component';
import { ResponsiveServiceImpComponent } from './components/responsive-service-imp/responsive-service-imp.component';
import { IconsImpComponent } from './components/icons-imp/icons-imp.component';
import { TooltipImpComponent } from './components/tooltip-imp/tooltip-imp.component';
import { InteractionsComponent } from './utils/interactions/interactions.component';
import { ArrayInputComponent } from './utils/interactions/components/array-input/array-input.component';
import { LoggerImpComponent } from './components/logger-imp/logger-imp.component';
import { ComponentTesterComponent } from './utils/component-tester/component-tester.component';
import { ComponenteTesterPageDirective } from './utils/component-tester/directives/component-tester-page.directive';

export const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  // yo:routes
	{ path: 'label', component: LabelImpComponent },
  { path: 'kbd', component: KbdImpComponent },
  { path: 'filters', component: FiltersImpComponent },
  { path: 'copy', component: CopyImpComponent },
  { path: 'contextual-menu', component: ContextualMenuImpComponent },
  { path: 'check', component: CheckImpComponent },
  { path: 'floating-menu', component: FloatingMenuImpComponent },
  { path: 'stepper', component: StepperImpComponent },
  { path: 'upload-file', component: UploadFileImpComponent },
  { path: 'input', component: InputImpComponent },
  { path: 'switch', component: SwitchImpComponent },
  { path: 'chips', component: ChipsImpComponent },
  { path: 'pagination', component: PaginationImpComponent },
  { path: 'dynamic-table', component: DynamicTableImpComponent },
  { path: 'dropdown', component: DropdownImpComponent },
  { path: 'banner', component: BannerImpComponent },
  { path: 'modal', component: ModalImpComponent },
  { path: 'alert', component: AlertImpComponent },
  { path: 'loader', component: LoaderImpComponent },
  { path: 'button', component: ButtonImpComponent },
  { path: 'card', component: CardImpComponent },
  { path: 'tabs', component: TabsImpComponent },
  { path: 'accordion', component: AccordionImpComponent },
  { path: 'calendar', component: CalendarImpComponent },
  { path: 'carrousel', component: CarrouselImpComponent },
  { path: 'text-truncate', component: TextTruncateImpComponent },
  { path: 'pin-code', component: PinCodeImpComponent },
  { path: 'tooltip', component: TooltipImpComponent },
  { path: 'aspect-ratio', component: AspectRatioImpComponent },
  { path: 'responsive-service', component: ResponsiveServiceImpComponent },
  { path: 'icons', component: IconsImpComponent },
  { path: 'logger', component: LoggerImpComponent },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  declarations: [
    // yo:declarations
		LabelImpComponent,
    KbdImpComponent,
    FiltersImpComponent,
    CopyImpComponent,
    ContextualMenuImpComponent,
    CheckImpComponent,
    FloatingMenuImpComponent,
    StepperImpComponent,
    UploadFileImpComponent,
    InputImpComponent,
    SwitchImpComponent,
    ChipsImpComponent,
    PaginationImpComponent,
    DynamicTableImpComponent,
    DropdownImpComponent,
    BannerImpComponent,
    ModalImpComponent,
    AlertImpComponent,
    LoaderImpComponent,
    ButtonImpComponent,
    CardImpComponent,
    TabsImpComponent,
    AccordionImpComponent,
    CalendarImpComponent,
    CarrouselImpComponent,
    TextTruncateImpComponent,
    PinCodeImpComponent,
    AppComponent,
    HomeComponent,
    AspectRatioImpComponent,
    ResponsiveServiceImpComponent,
    IconsImpComponent,
    TooltipImpComponent,
    InteractionsComponent,
    ArrayInputComponent,
    LoggerImpComponent,
    ComponentTesterComponent,
    ComponenteTesterPageDirective,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    RidDesignSystemModule,
    RidCoreModule.forRoot({
      loggerServiceConfig: {
        prefix: 'dummy',
        canDebug: true,
        // showDate: false,
        // showColors: false,
        // logLevel: RidLogLevel.TRACE,
        // showMetada: false,
        // showLogType: false,
        // showStackTrace: true,
        // showOrigin: false,
      }
    }),
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
