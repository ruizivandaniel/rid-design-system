import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';

@Component({
  selector: 'rid-copy-imp',
  templateUrl: 'copy-imp.component.html',
})
export class CopyImpComponent {
  onCopied(result: boolean): void {
    console.log('se copio!: ', result);
  }
  impInputs: InteractionInput[] = [];
}
