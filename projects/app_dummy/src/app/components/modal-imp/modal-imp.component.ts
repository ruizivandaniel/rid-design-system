import { Component, TemplateRef, ViewChild } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { RidModalService, RidModalSize, RidModalType } from '@rid/core';

@Component({
  selector: 'rid-modal-imp',
  templateUrl: 'modal-imp.component.html',
})
export class ModalImpComponent {

  @ViewChild('modalExample') private _modalExample!: TemplateRef<any>;

  impModalType: RidModalType = 'modal';
  impModalSize: RidModalSize = 'sm';
  impInputs: InteractionInput[] = [
    {
      change: (e: any) => { this.impModalType = e; },
      type: 'select',
      options: [{ label: 'modal' }, { label: 'sidebar' }],
      label: 'Tipo de modal',
    },
    {
      change: (e: any) => { this.impModalSize = e; },
      type: 'select',
      options: [{ label: 'xs' }, { label: 'sm' }, { label: 'md' }, { label: 'lg' }, { label: 'xl' },],
      label: 'Tamaño del modal',
    },
    {
      change: (_: any) => { this._openModal() },
      type: 'button',
      label: 'Abrir modal',
    },
  ];

  constructor(private _modalService: RidModalService) { }

  private _openModal = (): void => {
    const sub = this._modalService.open({
      template: this._modalExample,
      type: this.impModalType,
      size: this.impModalSize,
      closeOnPressEscKey: false,
      actions: [
        {
          action: () => { console.log('accion1'); },
          label: 'accion1',
        },
      ],
      title: 'Titulo del ejemplo'
    }).onClose.subscribe((acepterTYC: boolean) => {
      sub.unsubscribe();
      console.log(acepterTYC ? 'acepto' : 'no acepto');
    });
  }

  doCloseModal(acepted: boolean): void {
    this._modalService.close({ acepted });
  }
}
