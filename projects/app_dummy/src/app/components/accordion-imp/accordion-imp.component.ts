import { Component } from '@angular/core';
import { RidAccordionToggleEvent } from '@rid/design-system';
import { Interaction, InteractionBoolean } from '../../utils/component-tester/models/interaction.model';

@Component({
  selector: 'rid-accordion-imp',
  templateUrl: 'accordion-imp.component.html',
})
export class AccordionImpComponent {

  isMultiple: boolean = false;
  isCompact: boolean = false;

  accordionInteractions: Interaction<any>[] = [
    {
      label: 'Seleccion multiple',
      init: this.isMultiple,
      description: 'Habilitar la seleccion multiple del componente.',
      type: 'boolean',
      valueChanged: (newValue: boolean) => this.isMultiple = newValue,
    } as InteractionBoolean,
    {
      label: 'Modo compacto',
      init: this.isCompact,
      description: 'Mostrar el componente en modo compacto.',
      type: 'boolean',
      valueChanged: (newValue: boolean) => this.isCompact = newValue,
    } as InteractionBoolean,
  ];

  impOnToggle(event: RidAccordionToggleEvent): void {
    console.log('toggled: ', event);
  }

}
