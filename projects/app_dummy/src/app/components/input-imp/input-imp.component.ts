import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'rid-input-imp',
  templateUrl: 'input-imp.component.html',
})
export class InputImpComponent {
  showPassword: boolean = false;
  togglePassword = () => { this.showPassword = !this.showPassword }
  impLabel = 'Usuario *';
  impDescription = '';
  impLeftIconUrl = '/assets/lightbulb.svg';
  impRightIconUrl = '/assets/code-slash.svg';
  impInputs: InteractionInput[] = [
    {
      change: (event: string) => this.impLabel = event,
      label: 'Label',
      type: 'input',
      placeholder: 'Ingrese label del input',
    },
    {
      change: (event: string) => this.impDescription = event,
      label: 'Descripcion',
      type: 'input',
      placeholder: 'Ingrese descripcion del input...',
    },
    {
      change: (event: string) => this.impLeftIconUrl = event,
      label: 'Icono izquierdo',
      type: 'input',
      placeholder: 'Ingrese la url del icono izquierdo del input...',
    },
    {
      change: (event: string) => this.impDescription = event,
      label: 'Icono derecho',
      type: 'input',
      placeholder: 'Ingrese la url del icono derecho del input...',
    },
  ];

  frmDummy = new FormGroup({
    op1: new FormControl('', [Validators.required]),
    op2: new FormControl('', [Validators.required]),
    op3: new FormControl('', []),
    op4: new FormControl('', []),
    op5: new FormControl('', []),
  });

  onSubmit(): void {
    console.log(this.frmDummy.getRawValue());
  }
}
