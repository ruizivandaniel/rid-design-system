import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';

@Component({
  selector: 'rid-pagination-imp',
  templateUrl: 'pagination-imp.component.html',
})
export class PaginationImpComponent {
  impTotalPages = 30;
  impDisablePageInput = true;
  onPageChanged(event: any) {
    console.log('onPageChanged: ', event);
  }

  impInputs: InteractionInput[] = [
    {
      change: (e: any) => { this.impTotalPages = e },
      type: 'inputNumber',
      label: 'Valor total de paginas',
      value: this.impTotalPages
    },
    {
      change: (e: any) => { this.impDisablePageInput = e },
      type: 'switch',
      label: 'Deshabilitar input de paginas',
      value: this.impDisablePageInput
    },
  ];
}
