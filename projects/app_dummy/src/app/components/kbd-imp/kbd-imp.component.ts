import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { RidKbdKey } from '@rid/design-system';

@Component({
  selector: 'rid-kbd-imp',
  templateUrl: 'kbd-imp.component.html',
})
export class KbdImpComponent {
  keysImp: RidKbdKey[] = [RidKbdKey.COMMAND, RidKbdKey.OPTION];
  keysImp2: RidKbdKey[] = [RidKbdKey.CTRL, RidKbdKey.SHIFT];
  impDarkMode: boolean = false;
  impCommand: RidKbdKey = RidKbdKey.COMMAND;
  impInputs: InteractionInput[] = [
    {
      label: 'Dark mode',
      type: 'switch',
      value: this.impDarkMode,
      change: (isDarkMode: boolean) => {
        this.impDarkMode = isDarkMode
        console.log('asdasd')
      },
    },
    {
      label: 'Tecla',
      type: 'select',
      options: [
        { label: 'command' }, { label: 'shift' }, { label: 'ctrl' },
        { label: 'option' }, { label: 'enter' }, { label: 'delete' },
        { label: 'escape' }, { label: 'tab' }, { label: 'capslock' },
        { label: 'up' }, { label: 'right' }, { label: 'down' },
        { label: 'left' }, { label: 'pageup' }, { label: 'pagedown' },
        { label: 'home' }, { label: 'end' }, { label: 'help' },
        { label: 'space' },
      ],
      change: (value: string) => this.impCommand = value as RidKbdKey,
    },
  ];
}
