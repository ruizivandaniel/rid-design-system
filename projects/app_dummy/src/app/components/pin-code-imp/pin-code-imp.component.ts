import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'rid-pin-code-imp',
  templateUrl: 'pin-code-imp.component.html',
})
export class PinCodeImpComponent implements OnInit {
  frmPinCode = new FormGroup({
    pincode: new FormControl('', [
      Validators.required,
      Validators.minLength(14),
      Validators.maxLength(14),
    ]),
  });
  showOnlyNumbers: boolean = false;
  inputsLength: number = 4;

  ngOnInit(): void {
    const ctrl = this.frmPinCode.get('pincode');
    if (ctrl) {
      ctrl.valueChanges.subscribe((change) => console.log('imp: ', change));
      ctrl.setValue('4123122323');
    }
  }

  toggleDisabled(): void {
    this.frmPinCode.get('pincode')?.enabled ?
      this.frmPinCode.get('pincode')?.disable() :
      this.frmPinCode.get('pincode')?.enable();
  }

  setValue(value: string): void {
    this.frmPinCode.get('pincode')?.setValue(value);
  }

  setInputsLength(value: string): void {
    this.inputsLength = Number(value);
  }

}
