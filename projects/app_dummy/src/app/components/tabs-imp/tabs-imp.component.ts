import { Component } from '@angular/core';
import { RidTabItemBadge } from '@rid/design-system';
import { InteractionInput } from '../../utils/interactions/interactions.component';

@Component({
  selector: 'rid-tabs-imp',
  templateUrl: 'tabs-imp.component.html',
})
export class TabsImpComponent {
  showStaticTab = true;
  staticTabLabel = 'Static Label';
  staticTabBadges = [{ value: 'static badge', class: 'bg-danger text-white' }];

  customTabBadges: RidTabItemBadge[] = [
    { value: 'alert', class: 'bg-danger' },
    { value: 'new', class: 'bg-info' },
    { value: '3' }
  ];

  impInputs: InteractionInput[] = [
    {
      value: this.showStaticTab,
      change: (event: any) => { this.showStaticTab = event; },
      label: 'Mostrar static label',
      type: 'switch'
    },
    {
      label: 'Modificar static label',
      value: 'Static Label',
      type: 'input',
      change: (event: any) => { this.staticTabLabel = event; },
    },
    {
      label: 'Agregar custom badge',
      placeholder: ['Label...', 'Class...'],
      value: ['', ''],
      type: 'array',
      change: (event: any) => { this._addBadge(event[0], event[1]) },
    },
  ];

  private _addBadge(label: string, classes: string): void {
    this.customTabBadges.push({
      value: label,
      class: classes,
    });
  }

}
