import { Component } from '@angular/core';
import { RidSliceSelectedEvent } from '@rid/design-system';
import { InteractionInput } from '../../utils/interactions/interactions.component';

@Component({
  selector: 'rid-carrousel-imp',
  templateUrl: 'carrousel-imp.component.html',
})
export class CarrouselImpComponent {
  impCountSlides = 10;
  impSlidesPerView = 5
  impAutoplayDelay = 1000;
  impContainerHeight = 200;
  impShowError = false;
  impShowNavigationsArrows = true;
  impShowNavigationsSteps = true;
  impCyclic = true;
  impDraggable = false;
  impAutoplay = true;
  impStopOnHover = true;
  impScrollOnSelect = true;
  impShowSlides = true;

  impInputs: InteractionInput[] = [
    {
      change: (e: number) => { this.impCountSlides = Number(e); },
      label: 'Cantidad de slides totales',
      type: 'inputNumber',
      value: this.impCountSlides,
    },
    {
      change: (e: number) => { this.impSlidesPerView = Number(e); },
      label: 'Cantidad de slides a la vista',
      type: 'inputNumber',
      value: this.impSlidesPerView,
    },
    {
      change: (e: number) => { this.impContainerHeight = Number(e); },
      label: 'Height minimo del container',
      type: 'inputNumber',
      value: this.impContainerHeight,
    },
    {
      change: (event: any) => { this.impShowError = event; },
      label: 'Mostrar slide de error',
      type: 'switch',
      value: this.impShowError,
    },
    {
      change: (event: any) => { this.impShowNavigationsArrows = event; },
      label: 'Mostrar los navigations',
      type: 'switch',
      value: this.impShowNavigationsArrows,
    },
    {
      change: (event: any) => { this.impShowNavigationsSteps = event; },
      label: 'Mostrar los steps',
      type: 'switch',
      value: this.impShowNavigationsSteps,
    },
    {
      change: (event: any) => { this.impCyclic = event; },
      label: 'Es ciclico',
      type: 'switch',
      value: this.impCyclic,
    },
    {
      change: (event: any) => { this.impDraggable = event; },
      label: 'Es arrastrable',
      type: 'switch',
      value: this.impDraggable,
    },
    {
      change: (event: any) => { this.impAutoplay = event; },
      label: 'Autoplay',
      type: 'switch',
      value: this.impAutoplay,
    },
    {
      change: (e: number) => { this.impAutoplayDelay = Number(e); },
      label: 'Delay del autoplay',
      type: 'inputNumber',
      value: this.impAutoplayDelay,
    },
    {
      change: (event: any) => { this.impStopOnHover = event; },
      label: 'Frenar autoplay en hover',
      type: 'switch',
      value: this.impStopOnHover,
    },
    {
      change: (event: any) => { this.impScrollOnSelect = event; },
      label: 'Arrastrar al clickear',
      type: 'switch',
      value: this.impScrollOnSelect,
    },
    {
      change: (event: any) => { this.impShowSlides = event; },
      label: 'Ocultar slides',
      type: 'switch',
      value: this.impShowSlides,
    },
  ];

  onSliceChange(event: RidSliceSelectedEvent): void {
    console.log('change: ', event);
  }
}
