import { Component } from '@angular/core';

@Component({
  selector: 'app-aspect-ratio-imp',
  templateUrl: 'aspect-ratio-imp.component.html'
})
export class AspectRatioImpComponent {
  aspect = '16:9';

  changeAspect(): void {
    this.aspect = this.aspect === '16:9' ? '4:3' : '16:9';
  }

}
