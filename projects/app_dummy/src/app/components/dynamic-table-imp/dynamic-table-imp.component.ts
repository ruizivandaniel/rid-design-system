import { Component, OnInit, ViewChild } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import {
  RidDynamicTableHeader, RidDynamicTableRenderOptions, RidDynamicTablePaginationType,
  RidDynamicTablePlaceholderConfig, RidDynamicTableComponent,
} from '@rid/design-system';

@Component({
  selector: 'rid-dynamic-table-imp',
  templateUrl: 'dynamic-table-imp.component.html',
})
export class DynamicTableImpComponent implements OnInit {
  @ViewChild(RidDynamicTableComponent) private dynamicTableComp!: RidDynamicTableComponent;

  impHeaders: RidDynamicTableHeader[] = [
    { label: 'Titulo', sortBy: 'data.title' },
    { label: 'Role' },
    { label: 'Status', sortBy: 'data.status' },
    { label: 'Indice' },
  ];
  impDataRows: any[] = [];
  impRenderOptions: RidDynamicTableRenderOptions = {};
  impPaginationType: RidDynamicTablePaginationType = 'button';
  impPlaceholderConfig: RidDynamicTablePlaceholderConfig = {
    description: 'Lo que dice arriba, no hay datos.',
    iconUrl: 'assets/x.svg',
    message: 'No hay ni una dato.'
  }
  impInputs: InteractionInput[] = [
    {
      change: (value: any) => { this.impPaginationType = value; },
      label: 'Tipo de paginación',
      type: 'select',
      options: [
        { label: 'none' },
        { label: 'pagination' },
        { label: 'scroll' },
        { label: 'button' },
      ],
    },
    {
      change: (event: any) => { !event ? this._poblateDataRows() : this.impDataRows = [] },
      label: 'Mostrar placeholder',
      type: 'switch',
    },
    {
      label: 'Modificar mensaje por defecto',
      placeholder: ['Mensaje...', 'Descripcion...', 'Url icono...'],
      value: ['No hay ni una dato.', 'Lo que dice arriba, no hay datos.', 'assets/x.svg'],
      type: 'array',
      change: (event: any) => { this._setPlaceholderConfig({ message: event[0], description: event[1], iconUrl: event[2] }) },
    },
    {
      type: 'button',
      change: () => { this.renderNewItems() },
      label: 'Forzar render',
    }
  ];

  ngOnInit(): void {
    this._poblateDataRows();
  }

  renderNewItems(): void {
    this.dynamicTableComp.render();
  }

  private _poblateDataRows(): void {
    this.impDataRows = [].constructor(50).fill(0).map((a: any, i: number) => {
      const random = Math.round(Math.random() * 10)
      return {
        data: {
          title: `titulo ${i * random}`,
          subtitle: `subtitulo ${i * random}`,
          index: i,
          status: `status ${i * random}`,
          role: `role ${i * random}`,
        }
      };
    }).concat([]);
  }

  private _setPlaceholderConfig(config: RidDynamicTablePlaceholderConfig): void {
    this.impPlaceholderConfig = config;
  }

}
