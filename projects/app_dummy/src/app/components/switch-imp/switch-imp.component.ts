import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { RidSwitchItem } from '@rid/design-system';

@Component({
  selector: 'rid-switch-imp',
  templateUrl: 'switch-imp.component.html',
})
export class SwitchImpComponent {
  impSwitchItemsOne: RidSwitchItem[] = [
    { iconUrl: 'assets/sun.svg', value: true },
    { iconUrl: 'assets/moon.svg', value: false },
  ];
  impSwitchItems: RidSwitchItem[] = [
    { label: 'item 1', },
    { label: 'item 2', value: { name: 'ivan', age: 31 }, id: 'asdasd' },
    { label: 'item 3', value: { name: 'ana', age: 33 } },
    { label: 'item 4', iconUrl: 'assets/puzzle.svg' },
  ];
  impInputs: InteractionInput[] = [];

  onSwitchChange(event: any): void {
    console.log('switch change: ', event);
  }
}
