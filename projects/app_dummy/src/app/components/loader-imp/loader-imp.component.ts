import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { RidLoaderSize } from '@rid/design-system';

@Component({
  selector: 'rid-loader-imp',
  templateUrl: 'loader-imp.component.html',
})
export class LoaderImpComponent {
  impLoaderSize: RidLoaderSize = 'lg';
  impLoaderDark: boolean = false;

  impInputs: InteractionInput[] = [
    {
      change: (value: any) => { this.impLoaderSize = value; },
      label: 'Tamaño',
      type: 'select',
      options: [
        { label: 'xs' },
        { label: 'sm' },
        { label: 'md' },
        { label: 'lg' },
        { label: 'xl' },
      ],
    },
    {
      change: (value: any) => { this.impLoaderDark = value; },
      label: 'Modo dark',
      type: 'switch',
      value: this.impLoaderDark,
    }
  ];
}
