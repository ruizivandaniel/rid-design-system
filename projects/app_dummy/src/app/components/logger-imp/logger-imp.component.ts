import { Component, OnInit } from '@angular/core';
import { RidLoggerService } from '@rid/core';

@Component({
  selector: 'app-logger-imp',
  template: `
    <p>
      Mi valor: {{ value | ridLogger: 'Cambio valor variable' }}
    </p>
    <div class="my-3">
      <button ridButton (click)="callInfo()">call info</button>
    </div>
    <div class="my-3">
      <button ridButton (click)="callError()">call error</button>
    </div>
    <div class="my-3">
      <button ridButton (click)="changeValue()">change value</button>
    </div>
    <div class="my-3">
      <button ridButton (click)="changeValue2()">change value2</button>
    </div>
  `
})
export class LoggerImpComponent implements OnInit {

  private _value = false;

  get value() {
    return this._value;
  }

  constructor(private _loggerService: RidLoggerService) { }

  ngOnInit(): void {
    this.initCalls();
  }

  callInfo(): void {
    this._loggerService.info('Descripcion de la informacion!!!');
  }

  callError(): void {
    this._loggerService.info('Aca estoy haciendo tal cosa.', this.value);
    this._loggerService.error('Descripcion del error!!!', { meta: 'meta1', meta2: 'meta2' });
  }

  changeValue(): void {
    this._value = true;
  }

  changeValue2(): void {
    this._value = false;
  }

  initCalls(): void {
    this._loggerService.trace('Descripcion del trace!!!', 'initCalls se ejecuto');
    this._loggerService.debug('Descripcion del debug!!!', this._value);
    this._loggerService.warn('Descripcion del warn!!!');
    this._loggerService.fatal('Descripcion del fatal!!!', { error: 'adawhdawhd', errorCode: 500 });
  }

}
