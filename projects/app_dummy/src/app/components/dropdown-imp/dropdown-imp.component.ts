import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { DropdownToggledEvent, RidDropdownAlignment, RidDropdownItem } from '@rid/design-system';

@Component({
  selector: 'rid-dropdown-imp',
  templateUrl: 'dropdown-imp.component.html',
})
export class DropdownImpComponent {
  impItems: RidDropdownItem[] = [
    { action: () => { console.log(''); }, iconUrl: 'assets/puzzle.svg', label: 'Opcion 1', },
    { action: () => { console.log(''); }, iconUrl: 'assets/puzzle.svg', label: 'Opcion 2', },
    { action: () => { console.log(''); }, iconUrl: 'assets/puzzle.svg', label: 'Opcion 3', },
    { action: () => { console.log(''); }, iconUrl: 'assets/puzzle.svg', label: 'Opcion 4', type: 'danger' },
  ];
  impShowChevron = true;
  impShowFooter = true;
  impShowHeader = true;
  impChevronLight = true;
  impMaxWidth: number = 140;
  impMinWidth: number = 140;
  impAlignment: RidDropdownAlignment = 'left';
  impInputs: InteractionInput[] = [
    {
      change: (e: any) => { this.impMaxWidth = e; },
      label: 'Maximo largo del dropdown',
      type: 'inputNumber',
      value: this.impMaxWidth,
    },
    {
      change: (e: any) => { this.impMinWidth = e; },
      label: 'Minimo largo del dropdown',
      type: 'inputNumber',
      value: this.impMinWidth,
    },
    {
      change: (event: any) => { this.impShowChevron = event; },
      label: 'Mostrar el chevron',
      type: 'switch',
      value: this.impShowChevron,
    },
    {
      change: (event: any) => { this.impChevronLight = event; },
      label: 'Mostrar el chevron blanco',
      type: 'switch',
      value: this.impChevronLight,
    },
    {
      change: (event: any) => { this.impShowFooter = event; },
      label: 'Mostrar el footer',
      type: 'switch',
      value: this.impShowFooter,
    },
    {
      change: (event: any) => { this.impShowHeader = event; },
      label: 'Mostrar el header',
      type: 'switch',
      value: this.impShowHeader,
    },
    {
      change: (event: any) => { this.impAlignment = event; },
      label: 'Alineamiento del menu',
      type: 'select',
      options: [{ label: 'left' }, { label: 'center' }, { label: 'right' }],
    },
  ];

  onToggledDropdown(event: DropdownToggledEvent): void {
    console.log('toggled: ', event);
  }
}
