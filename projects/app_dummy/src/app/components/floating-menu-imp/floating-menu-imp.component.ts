import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { RidFloatingMenuItem } from '@rid/design-system';

@Component({
  selector: 'rid-floating-menu-imp',
  templateUrl: 'floating-menu-imp.component.html',
})
export class FloatingMenuImpComponent {
  floatingMenuItems: RidFloatingMenuItem[] = [
    { iconUrl: 'assets/puzzle.svg', action: () => { console.log('accion 1') } },
    { iconUrl: 'assets/lightbulb.svg', action: () => { console.log('accion 2') } },
    { iconUrl: 'assets/list.svg', action: () => { console.log('accion 3') } },
    { iconUrl: 'assets/phone.svg', action: () => { console.log('accion 4') } },
    { iconUrl: 'assets/sun.svg', action: () => { console.log('accion 5') } },
    { iconUrl: 'assets/moon.svg', action: () => { console.log('accion 6') } },
    { iconUrl: 'assets/lightbulb.svg', action: () => { console.log('accion 7') } },
    { iconUrl: 'assets/puzzle.svg', action: () => { console.log('accion 8') } },
  ];
  floatingMenuItems2: RidFloatingMenuItem[] = [
    { iconUrl: 'assets/puzzle.svg', action: () => { console.log('accion 1') } },
    { iconUrl: 'assets/moon.svg', action: () => { console.log('accion 2') } },
    { iconUrl: 'assets/phone.svg', action: () => { console.log('accion 3') } },
  ];

  impMaxVisibleItems = 2;
  impOffsetTop = 15;
  impCloseOnBackDropClick = true;

  impInputs: InteractionInput[] = [
    {
      label: 'Cerrar al clickear fuera: ',
      type: 'switch',
      change: (e: boolean) => { this.impCloseOnBackDropClick = e; },
      value: this.impCloseOnBackDropClick,
    },
    {
      label: 'Maximo de items: ',
      type: 'inputNumber',
      change: (e: number) => { console.log(e); this.impMaxVisibleItems = e; },
      value: this.impMaxVisibleItems,
    },
    {
      label: 'Padding Top en pixeles: ',
      type: 'inputNumber',
      change: (e: number) => { this.impOffsetTop = e; },
      value: this.impOffsetTop,
    },
  ];
}
