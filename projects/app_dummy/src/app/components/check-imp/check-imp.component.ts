import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'rid-check-imp',
  templateUrl: 'check-imp.component.html',
})
export class CheckImpComponent {

  frmDummy = new FormGroup({
    chk: new FormControl<boolean>(true, []),
    chk2: new FormControl<boolean>(false, []),
    chk3: new FormControl<boolean>(true, []),
  });
  frmDummy2 = new FormGroup({
    chk21: new FormControl<boolean>(true, []),
    chk22: new FormControl<boolean>(false, []),
    chk23: new FormControl<boolean>(true, []),
  });

  chkModel1: boolean = false;
  chkModel2: boolean = true;

  impInputs: InteractionInput[] = [];

  setChk(): void {
    this.frmDummy.get('chk')?.setValue(false);
  }

  onSubmit(): void {
    console.log(this.frmDummy.value);
  }
  onSubmit2(): void {
    console.log(this.frmDummy2.value);
  }
  onChecked(event: any): void {
    console.log('onChecked: ', event);
  }
  onChange(event: any): void {
    console.log('onChange: ', event);
  }


}
