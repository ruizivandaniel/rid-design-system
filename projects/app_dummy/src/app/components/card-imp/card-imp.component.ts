import { Component } from '@angular/core';
import { RidCardHeaderAction, RidCardFooterAction } from '@rid/design-system';
import { InteractionInput } from '../../utils/interactions/interactions.component';

@Component({
  selector: 'rid-card-imp',
  templateUrl: 'card-imp.component.html',
})
export class CardImpComponent {
  impShowBody = true;
  impHeaderActions: RidCardHeaderAction[] = [
    {
      label: 'Label1',
      icon: 'assets/puzzle.svg',
      action: () => { console.log('Label1'); }
    },
    {
      label: 'Label2',
      icon: 'assets/puzzle.svg',
      action: () => { console.log('Label2'); }
    },
    {
      label: 'Label3',
      icon: 'assets/puzzle.svg',
      action: () => { console.log('Label3'); }
    },
  ];
  impFooterActions: RidCardFooterAction[] = [
    {
      label: 'AccionF1',
      icon: 'assets/code-slash.svg',
      action: () => { console.log('AccionF1'); },
    },
    {
      label: 'AccionF2',
      icon: 'assets/code-slash.svg',
      action: () => { console.log('AccionF2'); },
      type: 'secondary'
    },
    {
      label: 'AccionF3',
      action: () => { console.log('AccionF3'); },
      type: 'warning'
    },
  ];

  impInputs: InteractionInput[] = [
    {
      change: (event: any) => { this.impShowBody = event; },
      label: 'Deshabilitar body',
      type: 'switch',
      value: this.impShowBody
    },
  ];
}
