import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';

@Component({
  selector: 'rid-text-truncate-imp',
  templateUrl: 'text-truncate-imp.component.html',
})
export class TextTruncateImpComponent {
  text: string = 'ejemplo de texto';
  limit: number = 15;
  cancelTruncate: boolean = false;
  tooltipMaxWidth: number = 350;
  reverse: boolean = false;
  disableTooltip: boolean = false;


  impInputs: InteractionInput[] = [
    {
      value: this.text,
      change: (event: string) => { this.text = event; },
      label: 'Text',
      type: 'input'
    },
    {
      value: this.limit,
      change: (event: number) => { this.limit = event; },
      label: 'Limite de caracteres',
      type: 'inputNumber'
    },
    {
      value: this.cancelTruncate,
      change: (event: any) => { this.cancelTruncate = event; },
      label: 'Activar truncate',
      type: 'switch'
    },
    {
      value: this.tooltipMaxWidth,
      change: (event: number) => { this.tooltipMaxWidth = event; },
      label: 'Tooltip max width',
      type: 'inputNumber'
    },
    {
      value: this.reverse,
      change: (event: boolean) => { this.reverse = event; },
      label: 'Dieresis invertido',
      type: 'switch'
    },
    {
      value: this.disableTooltip,
      change: (event: any) => { this.disableTooltip = event; },
      label: 'Deshabilitar tooltip',
      type: 'switch'
    },
  ];
}

