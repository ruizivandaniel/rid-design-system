import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';

@Component({
  selector: 'rid-label-imp',
  templateUrl: 'label-imp.component.html',
})
export class LabelImpComponent {
  impInputs: InteractionInput[] = [];
}
