import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';

@Component({
  selector: 'rid-banner-imp',
  templateUrl: 'banner-imp.component.html',
})
export class BannerImpComponent {
  impMessage = 'Mensaje del banner';
  impDescription = 'Descripcion del banner';
  impImageUrl = 'assets/icon-banner.svg';
  impForceImageWidth = 120;
  impForceImageHeight = 120;
  impInputs: InteractionInput[] = [
    {
      change: (e: any) => { this.impMessage = e; },
      label: 'Cambiar mensaje',
      type: 'input',
      value: this.impMessage,
    },
    {
      change: (e: any) => { this.impDescription = e; },
      label: 'Cambiar descripcion',
      type: 'input',
      value: this.impDescription,
    },
    {
      change: (e: any) => { this.impImageUrl = e; },
      label: 'Cambiar imagen',
      type: 'input',
      value: this.impImageUrl,
    },
    {
      change: (e: any) => { this.impForceImageWidth = e; },
      label: 'Cambiar largo maximo de la imagen',
      type: 'inputNumber',
      value: this.impForceImageWidth,
    },
    {
      change: (e: any) => { this.impForceImageHeight = e; },
      label: 'Cambiar alto maximo de la imagen',
      type: 'inputNumber',
      value: this.impForceImageHeight,
    },
  ];
}
