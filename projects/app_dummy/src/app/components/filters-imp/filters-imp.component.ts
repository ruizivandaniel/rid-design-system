import { Component, OnInit } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import {
  RidFilterRangeDateConfig, RidFilterRangeDateType, RidFilterRangeNumberConfig,
  RidFilterRangeNumberType, RidFilterStatus,
} from '@rid/design-system';

@Component({
  selector: 'rid-filters-imp',
  templateUrl: 'filters-imp.component.html',
})
export class FiltersImpComponent implements OnInit {
  impInputs: InteractionInput[] = [];

  selectOpts = [
    { text: 'Texto de opcion 1', value: 'value 1' },
    { text: 'Texto de opcion 2', value: 'value 2' },
    { text: 'Texto de opcion 3', value: 'value 3', selected: true },
    { text: 'Texto de opcion 4', value: 'value 4' },
    { text: 'Texto de opcion 5', value: 'value 5' },
  ];

  filterRangeDateConfig!: RidFilterRangeDateConfig;
  filterRangeDateDefault!: RidFilterRangeDateType;

  filterRangeNumberConfig!: RidFilterRangeNumberConfig;
  filterRangeNumberDefault!: RidFilterRangeNumberType;

  ngOnInit(): void {

    // config range date
    const d1 = new Date();
    d1.setMonth(d1.getMonth() - 1);
    const d2 = new Date();
    d2.setMonth(d2.getMonth() + 1);
    this.filterRangeDateConfig = {
      minDate: d1,
      maxDate: d2,
      from: { description: 'descript from', label: 'label from' },
      to: { description: 'descript to', label: 'label to' },
    };
    this.filterRangeDateDefault = { from: new Date(), to: new Date() };

    // config range numbers
    this.filterRangeNumberConfig = {
      min: 4,
      max: 200,
      from: { description: 'descript numbers from', label: 'label numbers from' },
      to: { description: 'descript numbers to', label: 'label numbers to' },
    };
    this.filterRangeNumberDefault = { from: 5, to: 10 };
  }

  onChangeImp(value: RidFilterStatus<string>): void {
    // console.log('onChangeImp: ', value);
  }

  onChangeGroupImp = (values: RidFilterStatus<any>[]): void => {
    console.log('onChangeGroupImp: ', values);
  }

  onExpandImp(expanded: boolean): void {
    // console.log('onExpandImp: ', expanded);
  }

  onFilterImp(): void {
    console.log('onFilterImp');
  }

}
