import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { RidAlertsService } from '@rid/core';
import { Interaction, InteractionBoolean, InteractionButton, InteractionInputNumber, InteractionInputText } from '../../utils/component-tester/models/interaction.model';

@Component({
  selector: 'rid-alert-imp',
  templateUrl: 'alert-imp.component.html',
})
export class AlertImpComponent {
  impShowAlert2: boolean = true;
  impString: number = 0;

  alertSuscriptionsInteractions: Interaction<any>[] = [
    {
      type: 'button',
      label: 'Emitir error',
      description: 'Lanzar un error desde el servicio',
      valueChanged: () => this._alertService.error(`Lorem ipsum dolor sit amet consectetur `
        + `adipisicing elit. In asperiores illum debitis mollitia, nisi `
        + `aperiam, sunt et ratione non suscipit praesentium, ex nesciunt `
        + `voluptate sapiente excepturi eius explicabo tempore dolores?`,
        undefined, -1),
    } as InteractionButton,
    {
      type: 'button',
      label: 'Emitir info',
      description: 'Lanzar un info desde el servicio',
      valueChanged: () => this._alertService.info(`Lorem ipsum dolor sit amet consectetur `
        + `adipisicing elit. In asperiores illum debitis mollitia, nisi `
        + `aperiam, sunt et ratione non suscipit praesentium, ex nesciunt `
        + `voluptate sapiente excepturi eius explicabo tempore dolores?`,
        'Esta es la descripcion de mi alerta de info la cual se cerrara en los proximos 4 segundos.',
        4000),
    } as InteractionButton,
    {
      type: 'boolean',
      label: 'Sucripto',
      description: 'Suscribirse a las emisiones de alertas',
      init: this.impShowAlert2,
      valueChanged: (newValue: boolean) => this.impShowAlert2 = newValue,
    } as InteractionBoolean,
  ];

  constructor(private _alertService: RidAlertsService) { }
}
