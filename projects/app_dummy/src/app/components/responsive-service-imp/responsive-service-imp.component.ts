import { Component } from '@angular/core';
import { RidResponsiveService } from '@rid/core';

@Component({
  selector: 'app-responsive-service-imp',
  templateUrl: 'responsive-service-imp.component.html'
})
export class ResponsiveServiceImpComponent {
  constructor(public responsiveService: RidResponsiveService) { }
}
