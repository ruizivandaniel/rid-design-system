import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { RidButtonType } from '@rid/design-system';

@Component({
  selector: 'rid-button-imp',
  templateUrl: 'button-imp.component.html',
})
export class ButtonImpComponent {
  impDisabled = false;
  impIsCompact = false;
  impIsLoading = false;
  impBtnType: RidButtonType = 'primary';
  impForceClass: string = 'bg-black border border-warning text-warning';
  impInputs: InteractionInput[] = [
    {
      change: (event: any) => { this.impDisabled = event; },
      label: 'Desactivar botones',
      type: 'switch',
      value: this.impDisabled,
    },
    {
      change: (event: any) => { this.impIsLoading = event; },
      label: 'Mostrar loader',
      type: 'switch',
      value: this.impIsLoading,
    },
    {
      change: (event: any) => { this.impIsCompact = event; },
      label: 'Hacer compactos',
      type: 'switch',
      value: this.impIsCompact,
    },
    {
      change: (event: any) => { this.impIsCompact = event; },
      label: 'Hacer compactos',
      type: 'switch',
      value: this.impIsCompact,
    },
    {
      change: (event: any) => { this.impForceClass = event; },
      label: 'Forzar clase a los botones',
      type: 'input',
      placeholder: 'forceClass',
      value: this.impForceClass,
    },
    {
      change: (event: any) => { this.impBtnType = event; },
      label: 'Cambiar tipo de boton',
      type: 'select',
      options: [
        { label: 'primary' },
        { label: 'secondary' },
        { label: 'danger' },
        { label: 'warning' },
        { label: 'info' },
        { label: 'success' },
      ],
    },
  ];

  onBtnClicked(btnName: string): void {
    console.log('nombre boton: ', btnName);
  }
}
