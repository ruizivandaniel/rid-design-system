import { Component } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { RidChip } from '@rid/design-system';

@Component({
  selector: 'rid-chips-imp',
  templateUrl: 'chips-imp.component.html',
})
export class ChipsImpComponent {
  impItems1: RidChip[] = [].constructor(5).fill('')
    .map((_: any, i: number) => { return { label: `Label ${i}` } });
  impItems2: RidChip[] = [].constructor(18).fill('')
    .map((_: any, i: number) => { return { label: `Label ${i}`, removable: true } });;
  impItems3: RidChip[] = [
    { label: 'label 1', removable: true },
    { label: 'label 2', type: 'danger', iconUrl: 'assets/puzzle.svg' },
    { label: 'label 3', type: 'info', iconUrl: 'assets/puzzle.svg' },
    { label: 'label 4', type: 'primary', iconUrl: 'assets/list.svg' },
    { label: 'label 5', type: 'secondary', iconUrl: 'assets/list.svg' },
    { label: 'label 6', type: 'success', removable: true },
    { label: 'label 7', type: 'warning', iconUrl: 'assets/code-slash.svg', removable: true },
  ];
  impInputs: InteractionInput[] = [
    {
      type: 'input',
      placeholder: 'label...',
      change: (e: string) => { this.addLabel(e) },
      label: 'Agregar label'
    }
  ];

  addLabel(label: string): void {
    this.impItems3 = this.impItems3.concat({ label, removable: true });
  }
}
