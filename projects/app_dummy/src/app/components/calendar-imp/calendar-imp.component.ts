import { Component } from '@angular/core';
import { RidResponsiveService } from '@rid/core';

@Component({
  selector: 'rid-calendar-imp',
  templateUrl: 'calendar-imp.component.html',
})
export class CalendarImpComponent {
  impType: 'multiple' | 'range' | 'single' = 'range';
  impDefault = new Date('2023-02-05T00:00:00');
  impInvalidDates: Date[] = [
    new Date('2023-02-03T00:00:00'),
    new Date('2023-02-04T00:00:00'),
    new Date('2023-02-25T00:00:00'),
    // new Date('2023-02-26T00:00:00'),
    new Date('2023-02-27T00:00:00'),
  ];

  impShowToday: boolean = true;
  impShowClear: boolean = true;
  impJustifyRows: boolean = false;
  impMinDate: Date = new Date('2023-09-02T00:00:00');
  impMaxDate: Date = new Date('2023-12-25T00:00:00');
  impDisabled: boolean = false;
  impShowMonthYearSelectors: boolean = true;
  impMonthYearSelectorsEditable: boolean = true;
  impShowNavigationsArrows: boolean = true;

  onDateChanged(evt: any): void {
    console.log('onDateChanged: ', evt);
  }
  onMonthChanged(evt: any): void {
    console.log('onMonthChanged: ', evt);
  }
  onYearChanged(evt: any): void {
    console.log('onYearChanged: ', evt);
  }
  onTodayClick(): void {
    console.log('onTodayClick!');
  }
  onClearClick(): void {
    console.log('onClearClick!');
  }

  get isResponsive(): boolean {
    return this._responsiveService.is(['MOBILE', 'NOOTEBOOK']);
  }

  constructor(private _responsiveService: RidResponsiveService) { }

}
