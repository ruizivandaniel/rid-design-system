import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { catchError, of, tap, throwError, timer } from 'rxjs';
import { RidStepperComponent, StepperCloseReason } from '@rid/design-system';

@Component({
  selector: 'rid-stepper-imp',
  templateUrl: 'stepper-imp.component.html',
})
export class StepperImpComponent implements AfterViewInit {
  @ViewChild(RidStepperComponent, { static: false })
  private _stepper!: RidStepperComponent;

  ngAfterViewInit(): void {
    console.log('stepper instance: ', this._stepper)
  }

  prevStepOneHandler = () => {
    // this._stepper.goToStep(this._stepper.steps[2]);
    // this._stepper.goToStepByTitle('step 2');
    this._stepper.goToStepById('tresid');
    return of(null);
  }
  goToStepTwo(): void { this._stepper.goToStepByTitle('step 2'); }
  nextStepOneHandler = () => {
    // return of(null);
    return timer(3000)
      .pipe(
        tap((_: any) => {
          throw new Error('error');
        }),
        catchError((err: any) => {
          this._stepper.goToStepById('errorid');
          return throwError(() => new Error(err));
        })
      );
  }
  nextStepErrorHandler = () => {
    return timer(2000)
      .pipe(tap(() => {
        this._stepper.goToStepByTitle('step 1');
      }));
  }
  nextStepTwoHandler = () => { return timer(3000); }
  prevStepThreeHandler = () => { return timer(3000); }
  onStepperClose(reason: StepperCloseReason): void { console.log('stepper close, reason: ', reason); }

  impInputs: InteractionInput[] = [];
}
