import { Component, OnInit } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'rid-upload-file-imp',
  templateUrl: 'upload-file-imp.component.html',
})
export class UploadFileImpComponent implements OnInit {
  impIsMultiple = true;
  impLabel = 'seleccione archivo';
  checkChanges = (e: any) => console.log(e);
  onSubmit = () => console.log(this.frmDummy.getRawValue());

  frmDummy = new FormGroup({
    files: new FormControl([], []),
  });

  impInputs: InteractionInput[] = [
    {
      type: 'input',
      label: 'Label del input',
      value: this.impLabel,
      change: (newLabel: string) => this.impLabel = newLabel,
      placeholder: 'Seleccione label del input',
    },
    {
      type: 'switch',
      label: 'Hacer multiple',
      change: (isMultiple: boolean) => this.impIsMultiple = isMultiple,
      value: this.impIsMultiple,
    },
  ];

  ngOnInit(): void {
    this.frmDummy.get('files')?.valueChanges.subscribe(console.log);
  }
}
