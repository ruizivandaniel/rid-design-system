import { Component, OnInit } from '@angular/core';
import { InteractionInput } from '../../utils/interactions/interactions.component';
import { RidContextualMenuService } from '@rid/core';

@Component({
  selector: 'rid-contextual-menu-imp',
  templateUrl: 'contextual-menu-imp.component.html',
})
export class ContextualMenuImpComponent implements OnInit {
  impInputs: InteractionInput[] = [];

  constructor(private _contextMenuService: RidContextualMenuService) { }

  ngOnInit(): void {
    console.log('options: ', this._contextMenuService.options);
    this._contextMenuService.setContext([
      { label: 'opcion 1', action: () => { console.log('opcion 1'); }, icon: 'assets/puzzle.svg', showDivider: true },
      { label: 'opcion 2', action: () => { console.log('opcion 2'); } },
      { label: 'opcion 3', action: () => { console.log('opcion 3'); }, showDivider: true },
      { label: 'opcion 4', action: () => { console.log('opcion 4'); }, forceClass: 'text-danger fw-semibold' },
    ]);
    console.log('options: ', this._contextMenuService.options);
  }
}
