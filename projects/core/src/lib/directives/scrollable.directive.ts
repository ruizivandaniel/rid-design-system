import { Directive, ElementRef, OnInit, OnDestroy, Input } from '@angular/core';
import { RidScrollService } from '../services/scroll.service';
import { Subject, fromEvent, takeUntil } from 'rxjs';
import { v4 } from 'uuid';

@Directive({
  selector: 'ridScrollable, [ridScrollable]',
  exportAs: 'ridScrollable',
  standalone: true,
})
export class RidScrollableDirective implements OnInit, OnDestroy {

  @Input('ridScrollable')
  set id(value: string) { this._id = value; }
  get id(): string { return this._id; }

  private _id!: string;
  private _unsubscriber = new Subject<void>();

  constructor(
    private _el: ElementRef<any>,
    private _scrollableService: RidScrollService,
  ) { }

  ngOnInit(): void {
    if (!this._id) {
      this._id = v4();
    }
    fromEvent(this._el.nativeElement, 'scroll')
      .pipe(takeUntil(this._unsubscriber))
      .subscribe((event: any) => {
        const { scrollHeight, scrollTop, clientHeight } = event.target;
        this._scrollableService.registerScroll({
          id: this._id,
          element: this._el.nativeElement,
          onBottom: Math.abs(scrollHeight - clientHeight - scrollTop) < 1,
          onTop: scrollTop === 0,
        });
      });
  }

  ngOnDestroy(): void {
    this._unsubscriber.next();
    this._unsubscriber.complete();
  }

}
