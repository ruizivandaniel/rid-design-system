import { Directive, EventEmitter, HostListener, Output } from '@angular/core';

/**
 * Directiva que aporta funcionalidades de drag and drop de
 * archivos al componente o elemento que se la implemente.
 */
@Directive({
  selector: 'ridDragDrop, [ridDragDrop]',
  exportAs: 'ridDragDrop',
  standalone: true,
})
export class RidDragDropDirective {

  /** Informa cuando el archivo se posiciona sobre el componente. */
  @Output() dragLeave = new EventEmitter<void>();
  /** Informa cuando el archivo se posiciona fuera el componente. */
  @Output() dragOver = new EventEmitter<void>();
  /** Informa cuando el/los archivo/s se suelta/n sobre el componente. */
  @Output() dropped = new EventEmitter<FileList>();

  @HostListener('dragover', ['$event'])
  _onDragOver(event: DragEvent): void {
    event.preventDefault();
    event.stopPropagation();
    this.dragOver.emit();
  }

  @HostListener('dragleave', ['$event'])
  _onDragLeave(event: DragEvent): void {
    event.preventDefault();
    event.stopPropagation();
    this.dragLeave.emit();
  }

  @HostListener('drop', ['$event'])
  _onDrop(event: DragEvent): void {
    event.preventDefault();
    event.stopPropagation();
    this.dropped.emit(event?.dataTransfer?.files);
  }

}
