import { Directive, ElementRef, Input, OnDestroy, OnInit } from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

/**
 * Directiva que escucha si se realizó un click dentro o fuera del elemento
 * que la implementa, ejecutando la funcion que se le pasa. Normalmente, el
 * primer click siempre esta por fuera del elemento, por eso se agregó la
 * variable "skipFirstClick", aunque si se desea, se puede quitar.
 *
 * @input clickedOutside : Funcion que se ejecuta cuando se clickea fuera del componente.
 * @input SkipIf: Si es true, obvia la ejecucion.
 * @input whiteList: Lista de elementos que hacen obviar la ejecucion al clickearlos.
 */
@Directive({
  selector: 'ridClickedOutside, [ridClickedOutside]',
  exportAs: 'ridClickedOutside',
  standalone: true,
})
export class RidClickedOutsideDirective implements OnInit, OnDestroy {
  @Input('ridClickedOutside') clickedOutside!: () => void;
  @Input() skipIf = false;
  @Input() whitelist!: HTMLElement[];

  private _unsubscribe: Subject<void> = new Subject<void>();
  private _isFirst = true;

  constructor(private _el: ElementRef) { }

  ngOnInit(): void {
    fromEvent(document, 'click')
      .pipe(takeUntil(this._unsubscribe))
      .subscribe((event: any) => {
        if (this.skipIf) {
          return;
        }
        if (!this._el.nativeElement.contains(event.target)) {
          if (this.whitelist) {
            if (this.whitelist.some((w) => w.contains(event.target as any))) {
              return;
            }
            this.clickedOutside();
          } else {
            if (this._isFirst) {
              this._isFirst = false;
              return;
            }
            this.clickedOutside();
          }
        }
      });
  }

  ngOnDestroy(): void {
    if (this._unsubscribe) {
      this._unsubscribe.next();
      this._unsubscribe.complete();
    }
  }
}
