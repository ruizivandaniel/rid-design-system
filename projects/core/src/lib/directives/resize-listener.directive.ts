import { Directive, OnInit, ElementRef, OnDestroy, Input } from '@angular/core';
import { ResizeSensor } from 'css-element-queries';
import { RidResizeEvent } from '../models/resize-event.model';

/**
 * Directiva encargada de escuchar cambios en los tamaños del elemento.
 * Al detectar un cambio emite el evento 'ridResized' retornando un 'ResizeEvent'
 * con el elemento como tal, y las medidas viejas y nuevas.
 * NOTA: requiere de la libreria 'css-element-queries'.
 */
@Directive({
  selector: 'ridResized, [ridResized]',
  exportAs: 'ridResized',
  standalone: true,
})
export class RidResizeListenerDirective implements OnInit, OnDestroy {

  /** Accion a ejecutar cuando se produce el resize del componente. */
  @Input('ridResized') doAction!: (resizeEvent: RidResizeEvent) => void;

  private _resizeSensor!: ResizeSensor;
  private _width!: number;
  private _height!: number;

  constructor(private _element: ElementRef) { }

  ngOnInit(): void {
    this._resizeSensor = new ResizeSensor(this._element.nativeElement, () => this._onResized());
  }

  ngOnDestroy(): void {
    this._resizeSensor.detach();
  }

  private _onResized() {
    const newWidth = this._element.nativeElement.clientWidth;
    const newHeight = this._element.nativeElement.clientHeight;
    if (this._width === newWidth && this._height === newHeight) { return; }
    const event = {
      element: this._element,
      newWidth,
      newHeight,
      oldWidth: this._width,
      oldHeight: this._height,
    };
    this._width = newWidth;
    this._height = newHeight;
    if (this.doAction) {
      this.doAction(event);
    }
  }

}
