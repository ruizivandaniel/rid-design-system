import {
  Component, Input, ElementRef, Renderer2,
  OnChanges, SimpleChanges, ViewChild, AfterViewInit
} from '@angular/core';

/**
 * Directiva encargada de redimensionar el componente en base a la relacion
 * de aspecto y el tamaño del mismo.
 * ***Nota***: Es obligatorio que el tenga seteado el largo (o ancho) con el que
 * se requiere basar la relación de aspecto, es decir si es byHeight el elemento
 * deberá tener seteado ese height, sino debera tener seteado el width (Ver ejemplos).
 *
 * ***Ejemplos:***
 *
 * ``` html
 * <!-- Aspect ratio en base al width: -->
 * <div class="mb-5 bg-info" [style.width.px]="500" ridAspectRatio="16:9" [byHeight]="false">
 *     <span>Seteado mediante [style.width]</span>
 * </div>
 *
 * <div class="mb-5 bg-info w-50" ridAspectRatio="4:3" [byHeight]="false">
 *     <span>Seteado mediante clase bootstrap</span>
 * </div>
 *
 * <!-- Aspect ratio en base al height: -->
 * <div class="mb-5 bg-warning" [style.height.px]="200" ridAspectRatio="16:10" [byHeight]="true">
 *     <span>Seteado mediante [style.height]</span>
 * </div>
 *
 * <div class="mb-5 bg-warning h-25" ridAspectRatio="4:3" [byHeight]="true">
 *     <span>Seteado mediante clase bootstrap</span>
 * </div>
 * ```
 */
@Component({
  selector: '[ridAspectRatio], ridAspectRatio',
  exportAs: 'ridAspectRatio',
  template: `
    <div #aspectRatioContainer (resized)="onresize()">
      <ng-content></ng-content>
    </div>
  `,
  standalone: true,
})
export class RidAspectRatioComponent implements AfterViewInit, OnChanges {

  /** relacion de aspecto que debe tener el componente. (default: '1:1'). */
  @Input('ridAspectRatio') aspectRatio = '1:1';
  /** si es true, toma como base el alto del componente sino el largo. */
  @Input() byHeight = false;

  @ViewChild('aspectRatioContainer', { static: true })
  private _container!: ElementRef;

  private _height = 0;
  private _width = 0;

  constructor(
    private _element: ElementRef,
    private _renderer: Renderer2,
  ) { }

  ngAfterViewInit(): void {
    this._setBasicStyles();
    this._setSizes();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['aspectRatio']
      && !changes['aspectRatio'].firstChange
      && changes['aspectRatio'].currentValue !== changes['aspectRatio'].previousValue) {
      this._setSizes();
    }
  }

  onresize(): void {
    if (this.byHeight && this._height !== this._element.nativeElement.offsetHeight) {
      this._setSizes();
    } else if (!this.byHeight && this._width !== this._element.nativeElement.offsetWidth) {
      this._setSizes();
    }
  }

  private _aspectRatio(): number[] {
    if (this.aspectRatio !== null && this.aspectRatio !== '') {
      return this.aspectRatio.split(':').map((v: string) => parseFloat(v));
    } else {
      return [1, 1];
    }
  }

  /**
   * Setea las dimensiones del componente en base al aspect ratio tomando
   * como valor el width o height que se paso (Dependiendo de byHeight).
   */
  private _setSizes(): void {
    const [x, y] = this._aspectRatio();
    if (this.byHeight) {
      this._height = this._element.nativeElement.offsetHeight;
      const newWidth = x * this._height / y;
      this._renderer.setStyle(this._element.nativeElement, 'width', `0px`);
      this._renderer.setStyle(this._element.nativeElement, 'paddingLeft', `${newWidth}px`);

    } else {
      this._width = this._element.nativeElement.offsetWidth;
      const newHeight = y * this._width / x;
      this._renderer.setStyle(this._element.nativeElement, 'height', `0px`);
      this._renderer.setStyle(this._element.nativeElement, 'paddingTop', `${newHeight}px`);
    }
  }

  /**
   * Setea los estilos necesarios al child "container" para posicionarlo
   * por encima del padding creado para el aspect ratio. De lo contrario,
   * todo el contenido del elemento se vería arrastrado.
   */
  private _setBasicStyles(): void {
    this._renderer.setStyle(this._element.nativeElement, 'position', 'relative');
    this._renderer.setStyle(this._element.nativeElement, 'overflow', 'hidden');
    this._renderer.setStyle(this._container.nativeElement, 'position', 'absolute');
    this._renderer.setStyle(this._container.nativeElement, 'top', '0');
    this._renderer.setStyle(this._container.nativeElement, 'left', '0');
    this._renderer.setStyle(this._container.nativeElement, 'right', '0');
    this._renderer.setStyle(this._container.nativeElement, 'bottom', '0');
  }
}
