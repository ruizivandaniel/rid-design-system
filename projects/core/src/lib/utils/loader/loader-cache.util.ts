import { Observable, of } from 'rxjs';
import { RidLoader } from './loader.util';
import { tap } from 'rxjs/operators';
import { RidLoaderCacheItem } from './../../models/loader-cache-item.model';

/**
 * Las instancias de `LoaderCache` son `usadas` por un `Loader` para
 * no solo cargar observables y mantener el estado de la llamada, si no también
 * para guardar la primer respuesta según una llave (`key`).
 */
export class RidLoaderCache {

  /** Listado de todo los valores cacheados. */
  private _itemsMap: Map<string, any> = new Map<string, any>();

  constructor(private _loader: RidLoader) { }

  /**
   * Carga el observable si y solo si no existe un elemento en el objeto cache con el mismo
   * nombre que la key pasada por parámetro.
   * @param observableFactory esto es una función factory, que deberá devolver el observable a llamar
   * y sólo se registra si la key no está en el cache.
   */
  load<T>(key: string, observableFactory: () => Observable<T>): Observable<T> {
    if (!this._itemsMap.has(key)) {
      this.set<Observable<T>>(key, this._loader.load(
        observableFactory().pipe(
          tap((result: T) => {
            this.set<T>(key, result);
          }),)
      ));
    }
    return this.get<T>(key);
  }

  /**
   * Limpia la cache según el nombre pasado por parámetro.
   * Si no se pasa un nombre (`key`), se limpiará toda la cache.
   * @param key el nombre de la cache que quiero limpiar.
   */
  clear(key?: string): void | null {
    !key ? this._itemsMap.clear() : this._itemsMap.delete(key);
  }

  /**
   * Obtiene el objeto guardado para el nombre de cache (`key`)
   * pasado por parámetro.
   * @param key el nombre del objeto en cache que quiero obtener.
   */
  get<T>(key: string): Observable<T> {
    if (!this._itemsMap.has(key)) {
      return of<any>(null);
    }
    const value = this._itemsMap.get(key);
    return (value instanceof Observable) ? value : of(value);
  }

  /**
   * Asigna un valor (`value`) con el nombre de cache (`key`)
   * pasado por parámetro.
   * @param key el nombre de la cache que quiero asignar.
   * @param value el valor que quiero guardar.
   */
  set<T>(key: string, value: T): void {
    this._itemsMap.set(key, value);
  }

  /**
   * Método para preguntar si hay un objecto asignado a ese nombre de cache (`key`).
   * @param key el nombre de la cache del cual quiero saber su existencia.
   */
  has(key: any): boolean {
    return this._itemsMap.has(key);
  }
}
