import { Observable, Subject, Subscription, of, throwError } from 'rxjs';
import { catchError, finalize, switchMapTo, tap } from 'rxjs/operators';
import { RidNotifiableEvent } from './../../models/notifiable-event.model';
import { RidLoaderCache } from './loader-cache.util';
import { RidNotifiable } from './../../models/notifiable.model';
import { RidLoaderItem } from './../../models/loader-item.model';

/**
 * Utilidad para realizar multiples llamadas asincrónicas.
 *
 * Un Loader podrá registrar Observables y notificará a un objeto Notifiable 3 eventos 'wait', 'error' y 'resume'.
 * También indicará si hay alguna llamada en espera mediante el método 'isLoading'.
 * Ejecutará la función 'wait' cuando se registre el primer observable.
 * Ejecutará la función 'error' cuando se de un error.
 * Ejecutará la función 'resume' cuando todos los observables que se hayan registrado hayan finalizado (con o sin).
 *
 * Para registrar un observable se usa la función 'load'.
 *
 * ```typescript
 * const loader = new Loader().notifyTo({
 *     wait: ()=>console.log('now I\'m waiting...'),
 *     resume: ()=>console.log('now I resume...')}
 * );
 *
 * const observable = new Observable((observer)=>{
 *    observer.next("first observable");
 *    setTimeout(()=>{observer.complete()},3000); //Wait for 3 seconds
 * });
 *
 * console.log(loader.isLoading())
 * // > false
 *
 * loader.load(observable).subscribe((nextValue)=>console.log(nextValue));
 * console.log("waiting: " , loader.isLoading());
 * // > now I'm waiting...
 * // > first observable
 * // > waiting: true
 * // After 3 seconds:
 * // > now I resume...
 * console.log("waiting: " , loader.isLoading());
 * // > waiting: false
 * ```
 */
export class RidLoader {

  /** listado de notifiables */
  private _notifiables = new Subject<RidNotifiableEvent>();

  /** listado de observables */
  private _observables: RidLoaderItem = {};

  /** retorna un array con las keys de los observables. */
  get keys(): string[] {
    return Object.keys(this._observables);
  }

  /**
   * Memoria cache de este loader, usar para cargar observables cuya respuesta quiero guardar en cache.
   */
  readonly cache: RidLoaderCache = new RidLoaderCache(this);

  /**
   * Subscribe un objeto `Notifiable`.
   * Este objeto será notificado ante los eventos del `Loader`.
   * @param notifiable el objeto al que el loader enviará notificaciones.
   */
  notifyTo(notifiable: RidNotifiable): Subscription {
    return this._notifiables
      .subscribe((event: RidNotifiableEvent) => {
        if (event.event === 'resume' && notifiable.resume) {
          notifiable.resume();
        } else if (event.event === 'wait' && notifiable.wait) {
          notifiable.wait();
        } else if (event.event === 'error' && notifiable.error) {
          notifiable.error(event.data);
        }
      });
  }

  /**
   * Devuelve `true` si alguno de todos los observables registrados
   * esta siendo cargado.
   */
  isLoading(): boolean {
    return this.keys.some((key: string) => this._observables[key].isLoading);
  }

  /**
   * Devuelve `true` si alguno de todos los observables registrados
   * registró algún error en su respuesta.
   */
  hasError(): boolean {
    return this.keys.some((key: string) => this._observables[key].hasError);
  }

  /**
   * Limpia todos los registros de observables (incluyendo los que estan en estado de error)
   * y notifica el evento `resume` de ser necesario.
   */
  clear(): void {
    this._observables = {};
    this._notifiables.next({ event: 'resume' });
  }

  /**
   * Registra un objeto observable. Si es el primero, se ejecutará la función 'wait' del
   * objeto Notifiable (si se registró alguno). Una vez que todos y cada uno de los observables terminen
   * de ejecutarse, se llamará a la función 'resume' del objeto Notifiable (si se registró alguno).
   */
  load<T>(observable: Observable<T>): Observable<T> {
    const auxId = `${Math.random() * 1000}_${new Date().getTime()}`;
    this._observables[auxId] = {
      hasError: false,
      isLoading: false,
    };
    return of(null)
      .pipe(
        tap(() => {
          if (this._observables[auxId]) this._observables[auxId].isLoading = true;
          if (this._observables[auxId]) this._observables[auxId].hasError = false;
          if (Object.keys(this._observables).filter(key => this._observables[key].isLoading).length === 1) {
            this._notifiables.next({ event: 'wait' });
          }
        }),
        switchMapTo(observable.pipe(
          catchError((err: any) => {
            if (this._observables[auxId]) { this._observables[auxId].hasError = true };
            this._notifiables.next({ event: 'error', data: err });
            return throwError(err);
          }),
          finalize(() => {
            if (this._observables[auxId]) this._observables[auxId].isLoading = false;
            if (!this.isLoading()) {
              this._notifiables.next({ event: 'resume' });
            }
          }),
        )),
      );
  }
}
