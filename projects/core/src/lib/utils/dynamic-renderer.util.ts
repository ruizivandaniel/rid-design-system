import { ViewContainerRef, TemplateRef, ChangeDetectorRef } from '@angular/core';
import { interval, take, startWith } from 'rxjs';
import {
  RidDynamicRendererOptions, RidDynamicRendererProps, RidDynamicRendererRestartOptions,
  RidRenderConfig,
} from '../models/dynamic-renderer.model';

const DEF_RENDER_INTERVAL = 0;
const DEF_ITEMS_PER_RENDER = 9;

export class RidDynamicRenderer {
  private _renderConfig!: RidRenderConfig;
  private _contexts: any[] = [];
  private _viewContainerRef!: ViewContainerRef;
  private _template!: TemplateRef<any>;
  private _changesDetectorRef!: ChangeDetectorRef;

  /**
   * Retorna la configuracion actual de la clase.
   */
  get config(): RidRenderConfig {
    return this._renderConfig;
  }

  /**
 * Retorna true en caso de tener mas items que
 * renderizar, caso contario devolverá false.
 * Tambien devolverá false en caso de no tener
 * items o una instancia de ViewContainerRef.
 */
  get canRender(): boolean {
    return this._viewContainerRef &&
      this._contexts.length > 0 &&
      this._renderConfig.currentIndex < this._contexts.length;
  }

  constructor(props: RidDynamicRendererProps) {
    this._viewContainerRef = props.viewContainerRef;
    this._template = props.template;
    this._changesDetectorRef = props.changesDetectorRef;
    const auxIPR =
      props.options && props.options.itemsPerRender
        ? props.options.itemsPerRender
        : DEF_ITEMS_PER_RENDER;
    const auxRI =
      props.options && props.options.renderInterval
        ? props.options.renderInterval
        : DEF_RENDER_INTERVAL;
    this.restart({
      itemsPerRender: auxIPR,
      renderInterval: auxRI,
      itemsData: props.itemsData,
    });
  }

  /**
   * Realiza una ronda de renderizado de items teniendo en cuenta
   * la cantidad de items a renderizar (itemsPerRender).
   */
  render(sliceFrom?: number): void {
    if (sliceFrom !== undefined) {
      this.restart();
    }
    if (!this.canRender) {
      return;
    }
    const from = sliceFrom !== undefined ? sliceFrom : this._renderConfig.currentIndex;
    const to = from + (this._renderConfig.itemsPerRender || DEF_ITEMS_PER_RENDER);
    const auxContexts = this._contexts.slice(from, to);
    if (
      this._renderConfig.renderInterval &&
      this._renderConfig.renderInterval > 0
    ) {
      interval(this._renderConfig.renderInterval || DEF_RENDER_INTERVAL)
        .pipe(startWith(-1), take(this._renderConfig.itemsPerRender || DEF_ITEMS_PER_RENDER))
        .subscribe((index: number) => {
          const ctx = auxContexts[index + 1];
          if (ctx) {
            this._viewContainerRef.createEmbeddedView(this._template, ctx);
          }
        });
    } else {
      auxContexts.forEach((ctx: any) => {
        this._viewContainerRef.createEmbeddedView(this._template, ctx);
      });
    }
    this._renderConfig.currentIndex = to;
    if (this._changesDetectorRef) {
      this._changesDetectorRef.detectChanges();
    }
  }

  /**
   * Reincia el estado actual del dynamic renderer y realiza la
   * limpieza del view container.
   * @param options
   */
  restart(options?: RidDynamicRendererRestartOptions): void {
    this._setConfig({
      itemsPerRender: options?.itemsPerRender,
      renderInterval: options?.renderInterval,
    });
    if (options && options.itemsData) {
      this._contexts = options.itemsData;
    }
    if (options && options.template) {
      this._template = options.template;
    }
    if (this._viewContainerRef) {
      this._viewContainerRef.clear();
    }
  }

  /** Inicializacion de la clase. */
  private _setConfig(options?: RidDynamicRendererOptions): void {
    this._renderConfig = {
      currentIndex: 0,
      renderInterval: options?.renderInterval ?? DEF_RENDER_INTERVAL,
      itemsPerRender: options?.itemsPerRender ?? DEF_ITEMS_PER_RENDER,
    };
  }
}
