import { ModuleWithProviders, NgModule } from '@angular/core';
import { RidCoreModuleConfig } from './models/core-module-config.model';
import { RidFormatterDatePipe } from './pipes/date-formatter.pipe';
import { RidSanitizerPipe } from './pipes/sanitizer.pipe';
import { RidFormatterTimePipe } from './pipes/time-formatter.pipe';
import { RID_RESPONSIVE_BREAKPOINTS } from './tokens/responsive-breakpoints.token';
import { RID_ALERTS_SERVICE_CONFIG } from './tokens/alert-service-config.token';
import { RID_ENCRYPTER_SERVICE_CONFIG } from './tokens/encrypter-secret-config.token';
import { RID_DEFAULT_RESPONSIVE_BREAKPOINTS } from './constants/default-responsive-breakpoints.constant';
import { RID_DEFAULT_ALERTS_SERVICE_CONFIG } from './constants/default-alerts-service-config.constant';
import { RID_DEFAULT_ENCRYPTER_SECRET } from './constants/default-encrypter-secret.constant';
import { RidCustomFormValidatorsService } from './services/custom-form-validators.service';
import { RidDateFormatterService } from './services/date-formatter.service';
import { RidResponsiveService } from './services/responsive.service';
import { RidAlertsService } from './services/alerts.service';
import { RidEncrypterService } from './services/encrypter.service';
import { RidModalService } from './services/modal.service';
import { RidLoggerService } from './services/logger.service';
import { RidStorageService } from './services/storage.service';
import { RidAspectRatioComponent } from './directives/aspect-ratio.directive';
import { RidResizeListenerDirective } from './directives/resize-listener.directive';
import { RidClickedOutsideDirective } from './directives/clicked-outside.directive';
import { RidScrollService } from './services/scroll.service';
import { RidScrollableDirective } from './directives/scrollable.directive';
import { RidContextualMenuService } from './services/contextual-menu.service';
import { RidDragDropDirective } from './directives/drag-drop.directive';
import { RID_DEFAULT_STORAGE_SERVICE_CONFIG } from './constants/default-storage-service-config.constant';
import { RID_STORAGE_SERVICE_CONFIG } from './tokens/storage-service-config.token';
import { RID_DEFAULT_LOGGER_SERVICE_CONFIG } from './constants/default-logger-service-config.constant';
import { RID_LOGGER_SERVICE_CONFIG } from './tokens/logger-service-config.token';
import { RidLoggerPipe } from './pipes/logger.pipe';

@NgModule({
  declarations: [
    RidFormatterTimePipe,
    RidFormatterDatePipe,
    RidSanitizerPipe,
    RidLoggerPipe,
  ],
  imports: [
    RidAspectRatioComponent,
    RidClickedOutsideDirective,
    RidResizeListenerDirective,
    RidScrollableDirective,
    RidDragDropDirective,
  ],
  exports: [
    RidFormatterTimePipe,
    RidFormatterDatePipe,
    RidSanitizerPipe,
    RidLoggerPipe,
    RidAspectRatioComponent,
    RidClickedOutsideDirective,
    RidResizeListenerDirective,
    RidScrollableDirective,
    RidDragDropDirective,
  ],
})
export class RidCoreModule {
  static forRoot(config?: RidCoreModuleConfig): ModuleWithProviders<RidCoreModule> {
    return {
      ngModule: RidCoreModule,
      providers: [
        RidResponsiveService,
        {
          provide: RID_RESPONSIVE_BREAKPOINTS,
          useValue: config && config.responsiveServiceBreakpoints ?
            config.responsiveServiceBreakpoints : RID_DEFAULT_RESPONSIVE_BREAKPOINTS,
        },
        {
          provide: RID_ALERTS_SERVICE_CONFIG,
          useValue: config && config.alertsServiceConfig ?
            config.alertsServiceConfig : RID_DEFAULT_ALERTS_SERVICE_CONFIG,
        },
        {
          provide: RID_ENCRYPTER_SERVICE_CONFIG,
          useValue: config && config.encrypterServiceConfig ?
            config.encrypterServiceConfig : RID_DEFAULT_ENCRYPTER_SECRET,
        },
        {
          provide: RID_STORAGE_SERVICE_CONFIG,
          useValue: config && config.storageServiceConfig ?
            config.storageServiceConfig : RID_DEFAULT_STORAGE_SERVICE_CONFIG,
        },
        {
          provide: RID_LOGGER_SERVICE_CONFIG,
          useValue: config && config.loggerServiceConfig ?
            { ...RID_DEFAULT_LOGGER_SERVICE_CONFIG, ...config.loggerServiceConfig } : RID_DEFAULT_LOGGER_SERVICE_CONFIG,
        },
        RidDateFormatterService,
        RidCustomFormValidatorsService,
        RidAlertsService,
        RidEncrypterService,
        RidModalService,
        RidScrollService,
        RidContextualMenuService,
        RidLoggerService,
        RidStorageService,
      ],
    } as ModuleWithProviders<RidCoreModule>;
  }
}
