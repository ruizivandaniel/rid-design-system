import { ViewContainerRef, TemplateRef, ChangeDetectorRef } from '@angular/core';

export interface RidDynamicRendererOptions {
  /** Sobrescribe el intervalo de tiempo entre cada renderizacion de los items. */
  renderInterval?: number;
  /** Sobrescribe la cantidad de elementos a renderizar por pasada. */
  itemsPerRender?: number;
}

export interface RidRenderConfig extends RidDynamicRendererOptions {
  /** Indice del ultimo item renderizado */
  currentIndex: number;
}

export interface RidDynamicRendererRestartOptions extends RidDynamicRendererOptions {
  /** Sobrescribe el Array de datos a pasar para cada item a renderizar. */
  itemsData?: any[];
  /** Template que se empleará para cada item. */
  template?: TemplateRef<any>;
}

export interface RidDynamicRendererProps {
  /** Array de datos a pasar para cada item a renderizar. */
  itemsData: any[];
  /** Contenedor donde se renderizarán los templates. */
  viewContainerRef: ViewContainerRef;
  /** Referencia del detector ref */
  changesDetectorRef: ChangeDetectorRef;
  /** Template que se empleará para cada item. */
  template: TemplateRef<any>;
  /** Opciones adicionales para el dynamic renderer. */
  options?: RidDynamicRendererOptions;
}
