import { ElementRef } from '@angular/core';

export interface RidScrollEvent {
  /** Identificador del elemento. */
  id: string;
  /** True en caso de haber llegado arriba de todo. */
  onTop: boolean;
  /** True en caso de haber llegado abajo de todo. */
  onBottom: boolean;
  /** Elemento desde donde se implementó la directiva. */
  element: ElementRef<HTMLElement>;
}
