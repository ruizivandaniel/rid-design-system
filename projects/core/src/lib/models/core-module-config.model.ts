import { RidAlertsServiceConfig } from './alerts-service-config.model';
import { RidEncripterServiceConfig } from './encripter-service-config.model';
import { RidStorageServiceConfig } from './storage-service-config.model';
import { RidResponsiveBreakpoints } from './responsive-breakpoints.model';
import { RidLoggerServiceConfig } from './logger-service-config.model';

export interface RidCoreModuleConfig {
  /** Breakpoints para el servicio de responsive. */
  responsiveServiceBreakpoints?: RidResponsiveBreakpoints;
  /** Configuracion del alert service. */
  alertsServiceConfig?: RidAlertsServiceConfig;
  /** Configuracion del encrypter service. */
  encrypterServiceConfig?: RidEncripterServiceConfig;
  /** Configuracion del storage service. */
  storageServiceConfig?: RidStorageServiceConfig;
  /** Configuracion del logger service. */
  loggerServiceConfig?: Partial<RidLoggerServiceConfig>;
}
