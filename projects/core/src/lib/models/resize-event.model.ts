import { ElementRef } from '@angular/core';

export interface RidResizeEvent {
  element: ElementRef;
  newWidth: number;
  newHeight: number;
  oldWidth: number;
  oldHeight: number;
}
