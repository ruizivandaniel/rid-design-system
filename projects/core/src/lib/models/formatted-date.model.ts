import { DateTime } from 'luxon';

export interface RidDatesFormat {
  /** Formato estilo "DD MMM". */
  short: string;
  /** Formato estilo "DD MMM YYYY". */
  long: string;
  /** Formato estilo "DD/MM/YY". */
  normal: string;
  /** Dia de la fecha tipo "DD". */
  day: string;
  /** Mes de la fecha tipo "MMM". */
  month: string;
  /** Año de la fecha tipo "YYYY". */
  year: string;
}

export interface RidTimesFormats {
  /** Formato estilo: "HH:MM:SS". */
  short: string;
  /** Formato estilo: "HH:MM". */
  long: string;
}


export interface RidFormattedDate {
  /** Fecha vanilla de js. */
  raw: Date;
  /** Fecha de luxon. */
  luxon: DateTime;
  /** Distintos formatos para los dias. */
  date: RidDatesFormat;
  /** Distintos formatos para las horas. */
  time: RidTimesFormats;
  /** Formato de fecha tipo "DD/MM/YYY a las HH:MM". */
  complete: string;
}

export type RidDateFormat = 'short' | 'long' | 'normal' | 'day' | 'month' | 'year';

export type RidTimeFormat = 'short' | 'long';
