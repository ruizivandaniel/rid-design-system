/**
 * Interfaz utilizada por el Loader para poder administrar los eventos que
 * se lanzan hacia los `Notifiables`.
 */
export type RidLoaderCacheItem = { [key: string]: any };
