export interface RidResponsiveBreakpoints {
  [key: string]: string;
}
