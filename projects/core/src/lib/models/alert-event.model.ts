import { RidAlertType } from './alerts-types.model';

export interface RidAlertEvent {
  /** Title a mostrar en la alerta. */
  title: string;
  /** Mensaje a mostrar en la alerta. */
  message?: string;
  /** Tipo de alerta */
  type: RidAlertType;
  /**
   * Retraso en milisegundos para cerrar la alerta.
   * Utilizar el -1 si se desea que sea infinito.
   */
  closeDelay: number;
}
