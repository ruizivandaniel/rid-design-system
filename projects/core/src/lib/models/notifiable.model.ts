/**
 * Interfaz  que identifica a un objeto como `Notifiable`,
 * lo que sugiere que puede recibir notificaciones por parte
 * de un `Loader`.
 */
export interface RidNotifiable {
  /**
   * Método a ejecutarse cuando comienza la carga de observables.
   */
  wait?(): any;
  /**
   * Método a ejecutarse cuando termina la carga de todos los observables.
   */
  resume?(): any;
  /**
   * Método a ejecutarse cuando se captura un error.
   * @param error el error capturado.
   */
  error?(error: any): any;
}
