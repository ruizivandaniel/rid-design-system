/**
 * Representacion del objeto con el loader administra los observables.
 */
export type RidLoaderItem = {
  /** identificador del observable */
  [key: string]: {
    /** true si se encuentra cargando el observable */
    isLoading: boolean,
    /** true si fallo el observable */
    hasError: boolean,
  },
};
