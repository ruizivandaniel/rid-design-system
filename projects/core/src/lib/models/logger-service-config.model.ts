/**
 * Niveles de logs. Mientras mas bajo el valor,
 * mas mensajes se mostrarán.
  */
export enum RidLogLevel {
  TRACE = 0,
  DEBUG = 1,
  INFO = 2,
  WARNING = 3,
  ERROR = 4,
  FATAL = 5,
  NEVER = Number.MAX_SAFE_INTEGER,
}

/** Configuracion utilizada por el servicio de logger. */
export interface RidLoggerServiceConfig {
  /** Prefijo que se desee utilizar como identificar el log. */
  prefix: string;
  /** true si se desea mostrar los logs. */
  canDebug: boolean;
  /** Muestra la fecha/hora del log. */
  showDate: boolean;
  /** Muestra colores segun el tipo de log. */
  showColors: boolean;
  /** Que tipos de logs estaran permitidos. */
  logLevel: RidLogLevel;
  /** true si se desea mostrar la metadata. */
  showMetada: boolean;
  /** true si se desea mostrar el tipo del log. */
  showLogType: boolean;
  /** true si se desea mostrar el stackTrace completo. */
  showStackTrace: boolean;
  /** true si se desea mostrar el tipo del dirPath del mensaje. */
  showOrigin: boolean;
}
