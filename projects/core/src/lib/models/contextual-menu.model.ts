export interface RidContextualMenuOption {
  /** Texto de la opcion del menu. */
  label: string;
  /** Icono de la opcion del menu. */
  icon?: string;
  /** Accion al clickear de la opcion del menu. */
  action: () => void;
  /** True si se desea mostrar divisor debajo. */
  showDivider?: boolean;
  /** Clases a agregar a la opcion. */
  forceClass?: string;
}
