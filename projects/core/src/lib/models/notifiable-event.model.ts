/**
 * Interfaz utilizada por el Loader para poder administrar los eventos que
 * se lanzan hacia los `Notifiables`.
 */
export interface RidNotifiableEvent {
  /** Tipo de evento lanzado. */
  event: 'wait' | 'error' | 'resume';
  /** Informacion a pasar por parametro. */
  data?: any;
};
