export interface RidAlertsServiceConfig {
  /** Milisegundos en que tarda en cerrar la alerta. */
  closeDelay: number;
}
