import { TemplateRef } from '@angular/core';

/** Tamaños del modal. */
export type RidModalSize = 'xs' | 'sm' | 'md' | 'lg' | 'xl';

/** Tipos de modal. */
export type RidModalType = 'sidebar' | 'modal';

/** Tipos de opacidades para el background del modal. */
export type RidModalBackgroundOpacity = 10 | 25 | 50 | 75;

/** Tipos de clases bootstrap que utilizara el background del modal. */
export type RidModalBackground = 'danger' | 'warning' | 'info'
  | 'success' | 'light' | 'white' | 'dark' | 'black' | 'transparent';

/** Posibles tipos de botones */
export type RidModalActionType = 'primary' | 'secondary' | 'danger' | 'warning' | 'info' | 'success';

/** Accion para el footer del modal. */
export interface RidModalAction {
  /** texto del boton */
  label: string;
  /** icono del boton */
  icon?: string;
  /** tipo del boton */
  type?: RidModalActionType;
  /** funcion del boton */
  action: () => void;
  /** True si se desea desabilitar el boton. */
  isDisabled?: boolean;
  /** True si se desea mostrar el spinner en el boton. */
  isLoading?: boolean;
}

/** Configuracion general del modal. */
export interface RidModalConfig {
  /** templateref a mostrar en el modal. */
  template: TemplateRef<any>;
  /** titulo a mostrar en el modal. */
  title?: string;
  /** Icono a mostrar en el titulo. */
  titleIconUrl?: string;
  /** Tipo de modal. */
  type?: RidModalType;
  /** tamaño del width que tomara el modal. */
  size?: RidModalSize;
  /** contexto a utilizar por el modal. */
  context?: any;
  /** Muestra la cruz para cerrar el modal */
  hideCancel?: boolean;
  /**
   * Crea un background transparente que tapa
   * la interfaz detras del modal.
   */
  showBackground?: boolean;
  /** Color del background detras del modal. */
  backgroundColor?: RidModalBackground;
  /** Opacidad del background detras del modal. */
  backgroundOpacity?: RidModalBackgroundOpacity;
  /**
   * Si esta en true, al pulsar sobre el background
   * detras del modal, se cerrará el modal.
   */
  closeOnBackDropClick?: boolean;
  /**
   * Si esta en true, al pulsar la tecla escape,
   * se cerrará el modal.
   */
  closeOnPressEscKey?: boolean;
  /** Listado de botones que se agregan a la base del modal. */
  actions?: RidModalAction[];
}
