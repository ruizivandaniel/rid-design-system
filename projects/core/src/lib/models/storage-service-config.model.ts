export interface RidStorageServiceConfig {
  /** true si se desea que se encripten los valores. */
  needEncrypt: boolean;
}
