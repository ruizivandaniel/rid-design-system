/** Posibles tipos de alertas. */
export type RidAlertType = 'info' | 'danger' | 'success' | 'warning';
