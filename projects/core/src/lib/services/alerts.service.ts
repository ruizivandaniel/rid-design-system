import { Inject, Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { RidAlertEvent } from '../models/alert-event.model';
import { RidAlertType } from '../models/alerts-types.model';
import { RID_ALERTS_SERVICE_CONFIG } from '../tokens/alert-service-config.token';
import { RidAlertsServiceConfig } from '../models/alerts-service-config.model';

/**
 * Servicio encargado de notificar las alertas dentro
 * de la aplicacion. Se puede emitir alertas de tipo
 * "success", "warning", "info" o "error" con las
 * funciones publicas y suscribirse a dichas alertas
 * con el get disponible "onNotification".
 */
@Injectable()
export class RidAlertsService {
  private _alertsCloseDelay!: number;
  private _onNotification: Subject<RidAlertEvent> = new Subject<RidAlertEvent>();

  /** Permite suscribirse a las alertas. */
  get onNotification(): Observable<RidAlertEvent> {
    return this._onNotification.asObservable();
  }

  constructor(@Inject(RID_ALERTS_SERVICE_CONFIG) private _alertsConfig: RidAlertsServiceConfig) {
    this._alertsCloseDelay = this._alertsConfig.closeDelay;
  }

  private _notif(type: RidAlertType, title: string, message: string = '', closeDelay: number = this._alertsCloseDelay): void {
    this._onNotification.next({ title, message, type, closeDelay });
  }

  /** shortcut notif success */
  success(title: string, message?: string, delay?: number): void {
    this._notif('success', title, message || '', delay);
  }

  /** shortcut notif warning */
  warning(title: string, message?: string, delay?: number): void {
    this._notif('warning', title, message || '', delay);
  }

  /** shortcut notif info */
  info(title: string, message?: string, delay?: number): void {
    this._notif('info', title, message || '', delay);
  }

  /** shortcut notif error */
  error(title: string, message?: string, delay?: number): void {
    this._notif('danger', title, message || '', delay);
  }
}
