import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { RID_RESPONSIVE_BREAKPOINTS } from '../tokens/responsive-breakpoints.token';
import { RidResponsiveBreakpoints } from '../models/responsive-breakpoints.model';

@Injectable()
export class RidResponsiveService {

  private _breakpoints = new BehaviorSubject<string[]>([]);

  get current(): string[] {
    return this._breakpoints.value;
  }

  get onChanges(): Observable<string[]> {
    return this._breakpoints.asObservable();
  }

  constructor(
    private _breakPointObserver: BreakpointObserver,
    @Inject(RID_RESPONSIVE_BREAKPOINTS) private _responsiveBreakpoints: RidResponsiveBreakpoints
  ) {
    this._breakPointObserver
      .observe(Object.values(this._responsiveBreakpoints))
      .subscribe((change: BreakpointState) => {
        const keysMatched = Object.keys(change.breakpoints)
          .filter((key: string) => change.breakpoints[key]);
        this._breakpoints.next(
          Object.keys(this._responsiveBreakpoints)
            .filter((key: string) => keysMatched.includes(this._responsiveBreakpoints[key]))
        );
      });
  }

  is(breakpoints: string | string[]): boolean {
    if (typeof breakpoints === 'string') {
      return this.current.includes(breakpoints);
    }
    return breakpoints.some((b: string) => this.current.includes(b));
  }

}
