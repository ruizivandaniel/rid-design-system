import { Inject, Injectable } from '@angular/core';
import { RidEncrypterService } from './encrypter.service';
import { RidStorageServiceConfig } from '../models/storage-service-config.model';
import { RidStorageType } from '../models/storage-type.model';
import { RID_STORAGE_SERVICE_CONFIG } from '../tokens/storage-service-config.token';

@Injectable()
export class RidStorageService {
  private _sessionStorage: Storage = sessionStorage;
  private _localStorage: Storage = localStorage;
  private _needEncrypt = false;

  constructor(
    private _encrypterService: RidEncrypterService,
    @Inject(RID_STORAGE_SERVICE_CONFIG) private _storageServiceConfig: RidStorageServiceConfig,
  ) {
    this._needEncrypt = this._storageServiceConfig.needEncrypt;
  }

  /** Obtiene el valor de la key guardada en el storage. */
  get(key: string, storageType: RidStorageType = 'local'): any {
    const value = storageType === 'local' ?
      this._localStorage.getItem(key) :
      this._sessionStorage.getItem(key);
    if (!value) {
      return undefined;
    }
    return this._needEncrypt ?
      this._encrypterService.decrypt(value) :
      this._getValueOrJSON(value);
  }

  /** Guarda la key en el storage */
  set(key: string, value: any, storageType: RidStorageType = 'local'): void {
    const auxValue = typeof value === 'object' ?
      JSON.stringify(value) :
      String(value);
    const encrypted = this._needEncrypt ?
      this._encrypterService.encrypt(auxValue) :
      auxValue;
    storageType === 'local' ?
      this._localStorage.setItem(key, encrypted) :
      this._sessionStorage.setItem(key, encrypted);
  }

  /** Elimina la key del storage */
  remove(key: string, storageType: RidStorageType = 'local'): void {
    storageType === 'local' ?
      this._localStorage.removeItem(key) :
      this._sessionStorage.removeItem(key);
  }

  /** ELimina todas las keys del storage. */
  removeList(keys: string[], storageType: RidStorageType = 'local'): void {
    keys.forEach(k => this.remove(k, storageType));
  }

  /** Limpia el storage completamente. */
  clear(storageType: RidStorageType | 'all' = 'all'): void {
    if (storageType === 'all') {
      this._localStorage.clear();
      this._sessionStorage.clear();
      return;
    }
    storageType === 'local' ?
      this._localStorage.clear() :
      this._sessionStorage.clear();
  }

  /** Valida si el item es un JSON o no. */
  private _getValueOrJSON(text: any): any {
    try {
      const parsed = JSON.parse(text);
      return parsed;
    } catch (_: any) {
      return text;
    }
  }

}
