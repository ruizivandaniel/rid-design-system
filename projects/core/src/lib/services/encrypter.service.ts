import { Injectable, Inject } from '@angular/core';
import * as cryptojs from 'crypto-js';
import { RID_ENCRYPTER_SERVICE_CONFIG } from '../tokens/encrypter-secret-config.token';

@Injectable()
export class RidEncrypterService {
  private _secret!: string;

  constructor(@Inject(RID_ENCRYPTER_SERVICE_CONFIG) private _encrypterSecret: string) {
    this._secret = this._encrypterSecret;
  }

  /** Realiza la encriptacion del texto pasado. */
  encrypt(text: string): string {
    if (!text) {
      return '';
    }
    return cryptojs.AES.encrypt(text, this._secret).toString();
  }

  /** Realiza la desencryptacion del texto encriptado pasado. */
  decrypt(encrypted: string): string {
    if (!encrypted) {
      return '';
    }
    const bytes = cryptojs.AES.decrypt(encrypted, this._secret);
    const value = bytes.toString(cryptojs.enc.Utf8);
    return this._getValueOrJSON(value);
  }

  /** Valida si el item es un JSON o no. */
  private _getValueOrJSON(text: any): any {
    try {
      const parsed = JSON.parse(text);
      return parsed;
    } catch (_) {
      return text;
    }
  }
}
