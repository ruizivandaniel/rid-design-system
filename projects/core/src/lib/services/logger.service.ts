import { Inject, Injectable } from '@angular/core';
import { RID_LOGGER_SERVICE_CONFIG } from '../tokens/logger-service-config.token';
import { RidLogLevel, RidLoggerServiceConfig } from '../models/logger-service-config.model';

@Injectable()
export class RidLoggerService {

  private _dateFormatter = Intl.DateTimeFormat(undefined, {
    timeStyle: 'medium',
    hour12: false,
  });

  constructor(
    @Inject(RID_LOGGER_SERVICE_CONFIG) private _config: RidLoggerServiceConfig,
  ) { }

  /**
   * Shortcut para loggear un log tipo trace (level 0). Este tipo de
   * log se utiliza para trackear flujos.
   * Ejemplo: 'Iniciando funcion X'.
   */
  trace(message: string, metadata?: any): void {
    this._log(message, RidLogLevel.TRACE, metadata);
  }

  /**
   * Shortcut para loggear un log tipo debug (level 1). Este tipo de
   * log se utiliza para mostrar informancion que ayude al debugeo de
   * la aplicación (estados).
   * Ejemplo: 'Valor de x: 45'.
   */
  debug(message: string, metadata?: any): void {
    this._log(message, RidLogLevel.DEBUG, metadata);
  }

  /**
   * Shortcut para loggear un log tipo info (level 2). Este tipo de
   * log se utiliza para mostrar informacion general de la aplicación.
   * Ejemplo: 'Translate cargado'.
   */
  info(message: string, metadata?: any): void {
    this._log(message, RidLogLevel.INFO, metadata);
  }

  /**
   * Shortcut para loggear un log tipo warning (level 3). Este tipo de
   * log se utiliza para mostrar anomalias en el codigo.
   * Ejemplo: 'Id no encontrado'.
   */
  warn(message: string, metadata?: any): void {
    this._log(message, RidLogLevel.WARNING, metadata);
  }

  /**
   * Shortcut para loggear un log tipo error (level 4). Este tipo de
   * log se utiliza para mostrar y describir errores de la aplicación.
   * Ejemplo: 'No se pudo conectar a la base'.
   */
  error(message: string, metadata?: any): void {
    this._log(message, RidLogLevel.ERROR, metadata);
  }

  /**
   * Shortcut para loggear un log tipo fatal. Este tipo de
   * log se utiliza para mostrar errore criticos que bloqueen el flujo
   * correcto de la aplicacion.
   * Ejemplo: 'El sistema crasheo'.
   */
  fatal(message: string, metadata?: any): void {
    this._log(message, RidLogLevel.FATAL, metadata);
  }

  private _getLogDescription(logLevel: RidLogLevel): string {
    switch (logLevel) {
      case 0: return '📌TRACE';
      case 1: return '🐞DEBUG';
      case 2: return '💭INFO';
      case 3: return '⚠️WARNING';
      case 4: return '🚨ERROR';
      case 5: return '🔥FATAL';
      default: return 'na';
    }
  }

  private _getColor(logLevel: RidLogLevel): string {
    switch (logLevel) {
      case 0: return `#A1A1AA`;
      case 1: return `#17C964`;
      case 2: return `#006FEE`;
      case 3: return `#F5A524`;
      case 4: return `#F31260`;
      case 5: return `#9353D3`;
      default: return 'na';
    }
  }

  private _getDate(): string {
    return this._dateFormatter.format(Date.now());
  }

  private _log(message: string, logLevel: RidLogLevel, metadata?: any): void {
    if (!this._config.canDebug || !this._canShowLog(logLevel)) {
      return;
    }
    const _stack = new Error().stack || '';
    const auxPrefix = this._config.prefix ? `[${this._config.prefix.toUpperCase()}] ` : '';
    const auxLogType = this._config.showLogType ? `[${this._getLogDescription(logLevel)}] ` : '';
    const auxDate = this._config.showDate ? `${this._getDate()} ` : '';
    const _errStack = _stack.split('\n')[3].toString().trim().replace('at ', '');
    const auxClass = _errStack.split('.')[0];
    const auxFunction = _errStack.split('.')[1].split(' ')[0];
    const auxMsg = ` ${message}`;
    const auxMeta = this._config.showMetada && metadata !== undefined && metadata !== null ? ` / meta: ${JSON.stringify(metadata)} ` : ' ';
    const auxOrigin = this._config.showOrigin ? `${_errStack.split(' ')[1]}` : '';
    const auxStackTrace = this._config.showStackTrace ? ` / ${_stack}` : '';
    const auxStr = `${auxPrefix}${auxLogType}${auxDate}[${auxClass}>${auxFunction}()]${auxMsg}${auxMeta}${auxOrigin}${auxStackTrace}`;
    this._config.showColors ?
      console.log(`%c${auxStr}`, `color: ${this._getColor(logLevel)}`) :
      console.log(auxStr);
  }

  private _canShowLog(logLevel: RidLogLevel): boolean {
    return logLevel >= this._config.logLevel;
  }

}
