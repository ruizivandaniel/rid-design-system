import { Injectable } from '@angular/core';
import { DateTime } from 'luxon';
import { RidFormattedDate } from '../models/formatted-date.model';

@Injectable()
export class RidDateFormatterService {

  // TODO: Configurar luxon aca en el constructor (locale, etc).

  fromJSDate(date: Date): RidFormattedDate {
    try {
      const aux = DateTime.fromJSDate(date).setLocale('ar');
      return <RidFormattedDate>{
        date: {
          normal: aux.toFormat('DD/MM/YY'),
          short: aux.toFormat('DD MMM'),
          long: aux.toFormat('DD MMM YYYY'),
          day: aux.toFormat('DD'),
          month: aux.toFormat('MMMM'),
          year: aux.toFormat('YYYY'),
        },
        time: {
          long: aux.toFormat('HH:MM:SS'),
          short: aux.toFormat('HH:MM'),
        },
        complete: `${aux.toFormat('DD/MM/YYYY')} `
          + `a las ${aux.toFormat('HH:MM')}hs.`,
      };
    } catch (err: any) {
      throw new Error(`Error de parseo. ERROR: ${err.message ?? err}`);
    }
  }

  fromISO(isoDate: string): RidFormattedDate {
    const date = new Date(isoDate);
    return this.fromJSDate(date);
  }

  fromDateTime(dateTime: DateTime): RidFormattedDate {
    return this.fromJSDate(dateTime.toJSDate());
  }

}
