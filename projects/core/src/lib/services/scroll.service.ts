import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject, filter } from 'rxjs';
import { RidScrollEvent } from '../models/scroll-event.model';
import { scrollableId } from '../models/scrollable-id.model';

@Injectable()
export class RidScrollService implements OnDestroy {

  private _scrolled: Subject<RidScrollEvent> = new Subject<RidScrollEvent>();

  ngOnDestroy(): void {
    this._scrolled.unsubscribe();
  }

  scrolled(id: scrollableId = undefined): Observable<RidScrollEvent> {
    return this._scrolled.asObservable()
      .pipe(
        filter((event: RidScrollEvent, _: number) => {
          return id === undefined
            || typeof id === 'string' && event.id === id
            || id.includes(event.id);
        }));
  }

  scrolledToTop(id: scrollableId = undefined): Observable<RidScrollEvent> {
    return this._scrolled.asObservable()
      .pipe(
        filter((event: RidScrollEvent, _: number) => {
          return event.onTop && (id === undefined
            || typeof id === 'string' && event.id === id
            || id.includes(event.id));
        })
      );
  }

  scrolledToBottom(id: scrollableId = undefined): Observable<RidScrollEvent> {
    return this._scrolled.asObservable()
      .pipe(
        filter((event: RidScrollEvent, _: number) => {
          return event.onBottom &&
            (id === undefined
              || typeof id === 'string' && event.id === id
              || id.includes(event.id));
        })
      );
  }

  registerScroll(event: RidScrollEvent): void {
    this._scrolled.next(event);
  }

}
