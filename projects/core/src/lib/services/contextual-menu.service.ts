import { Injectable } from '@angular/core';
import { RidContextualMenuOption } from '../models/contextual-menu.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class RidContextualMenuService {

  private _options = new BehaviorSubject<RidContextualMenuOption[]>([]);

  get options(): RidContextualMenuOption[] {
    return this._options.value;
  }

  get onChanges(): Observable<RidContextualMenuOption[]> {
    return this._options.asObservable();
  }

  setContext(options: RidContextualMenuOption[]): void {
    this._options.next(options);
  }

  clearContext(): void {
    this._options.next([]);
  }



}
