import { InjectionToken } from '@angular/core';
import { RidAlertsServiceConfig } from '../models/alerts-service-config.model';

export const RID_ALERTS_SERVICE_CONFIG: InjectionToken<RidAlertsServiceConfig>
  = new InjectionToken<RidAlertsServiceConfig>('RID_ALERTS_SERVICE_CONFIG');
