import { InjectionToken } from '@angular/core';
import { RidLoggerServiceConfig } from '../models/logger-service-config.model';

export const RID_LOGGER_SERVICE_CONFIG: InjectionToken<Partial<RidLoggerServiceConfig>>
  = new InjectionToken<Partial<RidLoggerServiceConfig>>('RID_LOGGER_SERVICE_CONFIG');
