import { InjectionToken } from '@angular/core';
import { RidEncripterServiceConfig } from '../models/encripter-service-config.model';

export const RID_ENCRYPTER_SERVICE_CONFIG: InjectionToken<RidEncripterServiceConfig>
  = new InjectionToken<RidEncripterServiceConfig>('RID_ENCRYPTER_SERVICE_CONFIG');
