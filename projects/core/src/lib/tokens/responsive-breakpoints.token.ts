import { InjectionToken } from '@angular/core';
import { RidResponsiveBreakpoints } from '../models/responsive-breakpoints.model';

export const RID_RESPONSIVE_BREAKPOINTS: InjectionToken<RidResponsiveBreakpoints>
  = new InjectionToken<RidResponsiveBreakpoints>('RID_RESPONSIVE_BREAKPOINTS');
