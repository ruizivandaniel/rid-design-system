import { InjectionToken } from '@angular/core';
import { RidStorageServiceConfig } from '../models/storage-service-config.model';

export const RID_STORAGE_SERVICE_CONFIG: InjectionToken<RidStorageServiceConfig>
  = new InjectionToken<RidStorageServiceConfig>('RID_STORAGE_SERVICE_CONFIG');
