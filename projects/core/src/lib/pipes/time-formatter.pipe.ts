import { Pipe, PipeTransform } from '@angular/core';
import { RidTimeFormat } from '../models/formatted-date.model';
import { RidDateFormatterService } from '../services/date-formatter.service';

@Pipe({
  name: 'ridTime',
  pure: true,
})
export class RidFormatterTimePipe implements PipeTransform {

  constructor(private _dateformatter: RidDateFormatterService) { }

  transform(value: string, format: RidTimeFormat): string {
    return this._dateformatter.fromISO(value).time[format];
  }

}
