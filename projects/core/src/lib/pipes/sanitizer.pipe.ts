import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

export type RidSanitizerType = 'html' | 'style' | 'script' | 'url' | 'resourceUrl';

@Pipe({
  name: 'ridSanitize',
  pure: true,
})
export class RidSanitizerPipe implements PipeTransform {

  constructor(private _sanitizer: DomSanitizer) { }

  transform(value: string, type: RidSanitizerType = 'html'): any {
    switch (type) {
      case 'html':
        return this._sanitizer.bypassSecurityTrustHtml(value);
      case 'style':
        return this._sanitizer.bypassSecurityTrustStyle(value);
      case 'script':
        return this._sanitizer.bypassSecurityTrustScript(value);
      case 'url':
        return this._sanitizer.bypassSecurityTrustUrl(value);
      case 'resourceUrl':
        return this._sanitizer.bypassSecurityTrustResourceUrl(value);
      default:
        throw Error('Opcion invalida para sanitizer pipe!');
    }
  }
}
