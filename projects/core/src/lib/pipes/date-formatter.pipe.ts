import { Pipe, PipeTransform } from '@angular/core';
import { RidDateFormat } from '../models/formatted-date.model';
import { RidDateFormatterService } from '../services/date-formatter.service';

@Pipe({
  name: 'ridDate',
  pure: true,
})
export class RidFormatterDatePipe implements PipeTransform {

  constructor(private _dateformatter: RidDateFormatterService) { }

  transform(value: string, format: RidDateFormat): string {
    return this._dateformatter.fromISO(value).date[format];
  }

}
