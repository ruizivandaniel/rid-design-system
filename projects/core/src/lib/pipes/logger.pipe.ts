import { Pipe, PipeTransform } from '@angular/core';
import { RidLogLevel } from '../models/logger-service-config.model';
import { RidLoggerService } from '../services/logger.service';

@Pipe({
  name: 'ridLogger',
  pure: true,
})
export class RidLoggerPipe implements PipeTransform {

  constructor(private _loggerService: RidLoggerService) { }

  transform(value: any, msg: string, logLevel: RidLogLevel = 1): any {
    switch (logLevel) {
      case 0: this._loggerService.trace(msg, value); break;
      case 1: this._loggerService.debug(msg, value); break;
      case 2: this._loggerService.info(msg, value); break;
      case 3: this._loggerService.warn(msg, value); break;
      case 4: this._loggerService.error(msg, value); break;
      case 5: this._loggerService.fatal(msg, value); break;
      default: throw Error(`El logLevel ${logLevel} invalido.`);
    }
    return value;
  }

}
