import { RidLogLevel, RidLoggerServiceConfig } from '../models/logger-service-config.model';

/** Configuracion por default para el servicio de alertas. */
export const RID_DEFAULT_LOGGER_SERVICE_CONFIG: RidLoggerServiceConfig = {
  canDebug: true,
  prefix: '',
  logLevel: RidLogLevel.TRACE,
  showColors: true,
  showDate: true,
  showMetada: true,
  showLogType: true,
  showOrigin: true,
  showStackTrace: false,
};

