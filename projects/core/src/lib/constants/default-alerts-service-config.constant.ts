import { RidAlertsServiceConfig } from '../models/alerts-service-config.model';

/** Configuracion por default para el servicio de alertas. */
export const RID_DEFAULT_ALERTS_SERVICE_CONFIG: RidAlertsServiceConfig = {
  closeDelay: 4000,
};
