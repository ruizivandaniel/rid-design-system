import { RidEncripterServiceConfig } from '../models/encripter-service-config.model';

/** Configuracion por default para el servicio de encriptacion. */
export const RID_DEFAULT_ENCRYPTER_SECRET: RidEncripterServiceConfig = {
  secret: 'JBaEr2WpzwpouakSAD0prymjWcOCxRV7RDbVRu6UE/E=',
};
