import { RidStorageServiceConfig } from '../models/storage-service-config.model';

/** Configuracion por default para el servicio de storage. */
export const RID_DEFAULT_STORAGE_SERVICE_CONFIG: RidStorageServiceConfig = {
  needEncrypt: false,
};
