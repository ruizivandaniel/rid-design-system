import { RidResponsiveBreakpoints } from '../models/responsive-breakpoints.model';

/** Breakpoints del responsive service por default. */
export const RID_DEFAULT_RESPONSIVE_BREAKPOINTS: RidResponsiveBreakpoints = {
  'MOBILE': '(max-width: 991px)',
  'NOTEBOOK': '(min-width: 992px) and (max-width: 1365px)',
  'DESKTOP': '(min-width: 1366px)',
};
