# Ruiz Ivan Daniel - Design System

Librería que agrupa todos los componentes reutilizables creados por mi a la hora de crear projectos con el fin de mantener una linea de estilos entre estos. Los componentes se basan en los estilos establecidos por un diseño en comun.

## Implementar

Para agregar la librería en un nuevo proyecto debemos realizar los siguientes pasos:

- Crear en el root del proyecto un archivo y nombrarlo como `.npmrc`.
- Dentro del archivo agregar lo siguiente `@rid:registry=[urlRegistry]`. Tambien debería poder obtenerse de la propiedad `publishConfig > registry` en el archivo `./package.json` de la librería.
- Abrir la terminal (estando en el root del proyecto) y ejecutar el comando `npm i @rid/design-system`.
- Agregar los styles generales de la libreria que se encuentran en `node_modules/@rid/design-system/styles/css/styles.min.css` dentro del `angular.json` en la propiedad: `architect > build > options > styles`.
- Agregar el modulo (general o del componente que necesitemos) al `app.module.ts`.
- En caso de necesitar los assets de la lib, agregar en el apartado `assets` del `angular.json` lo siguiente:
`{
  "glob": "**/*",
  "input": "node_modules/@rid/design-system/assets",
  "output": "assets"
}`
