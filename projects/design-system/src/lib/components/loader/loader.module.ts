import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidLoaderComponent } from './loader.component';

@NgModule({
  declarations: [
    RidLoaderComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    RidLoaderComponent,
  ],
})
export class RidLoaderModule { }
