import {
  Component, ElementRef, Input, OnInit, Renderer2, OnChanges,
  SimpleChanges,
} from '@angular/core';
import { RidLoaderSize } from './models/loader-size.model';

const LOADER_CONFIGS = {
  'xs': '15px',
  'sm': '25px',
  'md': '35px',
  'lg': '50px',
  'xl': '70px',
}

@Component({
  selector: 'rid-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class RidLoaderComponent implements OnInit, OnChanges {
  /** Tamaño del loader. */
  @Input() size: RidLoaderSize = 'sm';
  /** True si se desea que el loader sea modo dark (color blanco). */
  @Input() isDark: boolean = false;

  constructor(private _el: ElementRef,
    private _renderer: Renderer2) { }

  ngOnInit(): void {
    this._checkClass();
    this._checkSize();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['isDark']
      && !changes['isDark'].isFirstChange()
      && changes['isDark'].currentValue !== changes['isDark'].previousValue) {
      this._checkClass();
    }
    if (changes['size']
      && !changes['size'].isFirstChange()
      && changes['size'].currentValue !== changes['size'].previousValue) {
      this._checkSize(changes['size'].previousValue);
    }
  }

  private _checkClass(): void {
    if (this.isDark) {
      this._renderer.addClass(this._el.nativeElement.childNodes[0], 'loader-dark');
    } else {
      this._renderer.removeClass(this._el.nativeElement.childNodes[0], 'loader-dark');
    }
  }

  private _checkSize(prevSize?: RidLoaderSize): void {
    this._renderer.setStyle(this._el.nativeElement.childNodes[0], 'width', LOADER_CONFIGS[this.size]);
    this._renderer.setStyle(this._el.nativeElement.childNodes[0], 'height', LOADER_CONFIGS[this.size]);
    if (prevSize) {
      this._renderer.removeClass(this._el.nativeElement.childNodes[0], `loader-${prevSize}`);
    }
    this._renderer.addClass(this._el.nativeElement.childNodes[0], `loader-${this.size}`);
  }

}
