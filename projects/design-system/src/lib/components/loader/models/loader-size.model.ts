export type RidLoaderSize = 'xs' | 'sm' | 'md' | 'lg' | 'xl';
