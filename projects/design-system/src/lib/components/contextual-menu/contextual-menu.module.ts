import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidContextualMenuComponent } from './contextual-menu.component';
import { RidSvgIconsModule } from '../svg-icons/svg-icons.module';

@NgModule({
  declarations: [
    RidContextualMenuComponent,
  ],
  imports: [
    CommonModule,
    RidSvgIconsModule,
  ],
  exports: [
    RidContextualMenuComponent,
  ],
})
export class RidContextualMenuModule { }
