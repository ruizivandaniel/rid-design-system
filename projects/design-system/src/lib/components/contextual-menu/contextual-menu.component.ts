import { Component, OnDestroy, OnInit } from '@angular/core';
import { RidContextualMenuOption, RidContextualMenuService } from '@rid/core';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'rid-contextual-menu',
  templateUrl: 'contextual-menu.component.html',
})
export class RidContextualMenuComponent implements OnInit, OnDestroy {

  protected _posTop: number = 0;
  protected _posLeft: number = 0;
  protected _currentMenu: RidContextualMenuOption[] = [];
  protected _opened: boolean = false;
  private _unsubscriber = new Subject<void>();

  get someIconExist(): boolean {
    return this._currentMenu.some((o: RidContextualMenuOption) => o.icon);
  }

  constructor(
    private _contextualMenuService: RidContextualMenuService,
  ) { }

  ngOnInit(): void {
    this._contextualMenuService.onChanges
      .pipe(takeUntil(this._unsubscriber))
      .subscribe((ctxMenuOpts: RidContextualMenuOption[]) => {
        this._currentMenu = ctxMenuOpts;
      });
  }

  ngOnDestroy(): void {
    this._unsubscriber.next();
    this._unsubscriber.complete();
  }

  showContextMenu(e: any): void {
    e.preventDefault();
    if (!this._opened) {
      this._opened = true;
    }
    this._posTop = e.layerY || e.offsetY;
    this._posLeft = e.layerX || e.offsetX;
  }

  closeContextMenu(e: any): void {
    e.preventDefault();
    if (this._opened) {
      this._opened = false;
    }
  }

}
