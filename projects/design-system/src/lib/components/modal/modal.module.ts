import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidModalComponent } from './modal.component';
import { RidSvgIconsModule } from './../svg-icons/svg-icons.module';
import { RidButtonModule } from './../button/button.module';

@NgModule({
  declarations: [
    RidModalComponent,
  ],
  imports: [
    CommonModule,
    RidSvgIconsModule,
    RidButtonModule,
  ],
  exports: [
    RidModalComponent,
  ],
})
export class RidModalModule { }
