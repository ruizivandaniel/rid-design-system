import { Component, OnInit, OnDestroy } from '@angular/core';
import { RidModalConfig, RidModalService, RidResponsiveService } from '@rid/core';
import { Subject, Subscription, filter, fromEvent, takeUntil } from 'rxjs';

const RID_MODAL_SIZES: { [key: string]: number } = {
  'xs': 420,
  'sm': 500,
  'md': 700,
  'lg': 900,
  'xl': 1100,
};

/**
 * Componente que representa un modal con el template
 * que se le pase (y una configuracion adicional). En
 * caso de necesitar usarlo, ver el servicio de
 * RidModalService.
 */
@Component({
  selector: 'rid-modal',
  templateUrl: 'modal.component.html',
})
export class RidModalComponent implements OnInit, OnDestroy {
  config!: RidModalConfig;

  private _showModal = false;
  private _onCloseSubscription!: Subscription;
  private _escKeyPressSubscription!: Subscription;
  private _modalWidth: number = 0;
  private _modalMaxHeight: number = 0;
  private _unsubscriber = new Subject<void>();

  get isResponsive(): boolean {
    return this._responsiveService.is(['MOBILE', 'NOTEBOOK']);
  }

  get showModal(): boolean {
    return this._showModal;
  }

  get modalWidth(): number {
    return this._modalWidth;
  }

  get modalMaxHeight(): number {
    return this._modalMaxHeight;
  }

  constructor(
    private _modalService: RidModalService,
    private _responsiveService: RidResponsiveService,
  ) { }

  ngOnInit(): void {
    this._modalService.onOpen
      .pipe(takeUntil(this._unsubscriber))
      .subscribe((config: RidModalConfig) => {
        this._setConfig(config);
        this._modalWidth = RID_MODAL_SIZES[this.config.size ?? 'md'];
        this._modalMaxHeight = this.config.type === 'modal' ? window.innerHeight - 20 : window.innerHeight;
        this._showModal = true;
        if (this.config.closeOnPressEscKey) {
          this._listenEscKey();
        } else {
          this._unlistenEscKey();
        }
        /** Me subscribo en caso de recibir un close de fuera. */
        this._listenToOnClose();
      });
  }

  ngOnDestroy(): void {
    this._unsubscriber.next();
    this._unsubscriber.complete();
  }

  private _unlistenToOnClose(): void {
    if (this._onCloseSubscription) {
      this._onCloseSubscription.unsubscribe();
    }
  }
  private _listenToOnClose(): void {
    this._unlistenToOnClose();
    this._onCloseSubscription = this._modalService.onClose
      .pipe(takeUntil(this._unsubscriber))
      .subscribe(_ => {
        this._showModal = false;
        this._onCloseSubscription.unsubscribe();
        this._unlistenEscKey();
      });
  }

  private _listenEscKey(): void {
    this._unlistenEscKey();
    this._escKeyPressSubscription = fromEvent(window, 'keydown')
      .pipe(
        takeUntil(this._unsubscriber),
        filter((e: any) => e.code === 'Escape')
      )
      .subscribe((_: any) => {
        this.close();
        this._unlistenEscKey();
      });
  }

  private _unlistenEscKey(): void {
    if (this._escKeyPressSubscription) {
      this._escKeyPressSubscription.unsubscribe();
    }
  }

  private _setConfig(config: RidModalConfig) {
    this.config = {
      type: 'modal',
      showBackground: true,
      backgroundColor: 'dark',
      backgroundOpacity: 25,
      closeOnBackDropClick: false,
      closeOnPressEscKey: false,
      size: 'md',
      context: { RidmodalClose: this.close },
      actions: [],
      ...config
    };
    if (!this.config.context.RidmodalClose) {
      this.config.context['RidmodalClose'] = this.close;
    }
  }

  close = (args?: any): void => {
    this._showModal = false;
    this._onCloseSubscription.unsubscribe();
    this._unlistenEscKey();
    this._modalService.close(args);
  };
}
