import {
  Component, OnInit, OnDestroy, Input, Output, EventEmitter,
  ViewChild, ElementRef, AfterViewInit, Renderer2
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Subject, fromEvent } from 'rxjs';
import { takeUntil, skip } from 'rxjs/operators';

/**
 * Este componente es interno del RidPinCodeComponent que utiliza
 * para representar cada uno de los inputs que va a mostrar.
 */
@Component({
  selector: 'rid-pin-code-item',
  templateUrl: 'pin-code-item.component.html',
  styleUrls: ['./pin-code-item.component.scss']
})
export class RidPinCodeItemComponent implements OnInit, AfterViewInit, OnDestroy {

  /** Posicion del item respecto al array del pin-code component. */
  @Input() index: number = -1;
  /** Limite minimo y maximo del input. */
  @Input() length: number = 1;
  /** true si solo se desea poder ingresar numeros. */
  @Input() onlyNumbers: boolean = false;
  @Output() moveTo = new EventEmitter<number>();
  @Output() blur = new EventEmitter<void>();

  @ViewChild('input') private _input!: ElementRef<any>;

  control!: FormControl;

  private _unsubscriber = new Subject<void>();

  get currentValue(): string | number {
    return this.control.value;
  }

  constructor(private _renderer: Renderer2) { }

  ngOnInit(): void {
    this.control = new FormControl('', [
      Validators.required,
      Validators.maxLength(this.length),
      Validators.minLength(this.length),
    ]);
    this.control.valueChanges
      .pipe(
        skip(1),
        takeUntil(this._unsubscriber),
      )
      .subscribe((_: string) => {
        if (this.control.valid) {
          this.moveTo.emit(this.index + 1);
        }
      });
  }

  ngAfterViewInit(): void {
    fromEvent(this._input.nativeElement, 'keydown')
      .pipe(takeUntil(this._unsubscriber))
      .subscribe((event: any) => {
        if (event.key === 'Backspace' && this.control.value === '') {
          this.moveTo.emit(this.index - 1);
        }
        event.stopPropagation();
        const charCode = (event.which) ? event.which : event.keyCode;
        if ([189, 187, 109, 107, 69, 45, 43].includes(charCode)) {
          event.preventDefault();
        }
      });
    if (this.onlyNumbers) {
      this._renderer.setAttribute(this._input.nativeElement, 'min', '0');
      this._renderer.setAttribute(this._input.nativeElement, 'max', Array(this.length).fill(9).join(''));
    }
  }

  ngOnDestroy(): void {
    this._unsubscriber.next();
    this._unsubscriber.complete();
  }

  focus(): void {
    if (this._input) {
      this._input.nativeElement.focus();
    }
  }

  onBlur(): void {
    this.blur.emit();
  }

  setDisabled(isDisabled: boolean): void {
    isDisabled ? this.control.disable() : this.control.enable();
    this.control.setValue('', { emitEvent: false });
  }

}
