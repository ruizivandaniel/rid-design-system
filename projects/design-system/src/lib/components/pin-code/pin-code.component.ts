import {
  AfterViewInit, ChangeDetectorRef, Component, forwardRef,
  Input, QueryList, ViewChildren, OnChanges, SimpleChanges
} from '@angular/core';
import {
  ControlValueAccessor, NG_VALUE_ACCESSOR, Validators
} from '@angular/forms';
import { RidPinCodeItemComponent } from './components/pin-code-item.component';

/**
 * Componente que representa un conjunto de inputs
 * al estilo "codigo de verificacion".
 */
@Component({
  selector: 'rid-pin-code',
  templateUrl: 'pin-code.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RidPinCodeComponent),
      multi: true,
    }
  ]
})
export class RidPinCodeComponent implements AfterViewInit, OnChanges, ControlValueAccessor {

  /** Cantidad de inputs que se generaran por separado. */
  @Input() inputsLength: number = 4;
  /**
   * Maximo de elementos por input. Si el valor pasado es
   * de tipo number, todos los inputs tendran ese valor.
   * Miestras que si es de tipo array, cada elemento tendra
   * el valor que corresponde a su posicion en el array de
   * inputs (si no existe, por defecto tendra 1).
   */
  @Input() maxLengthPerPin: number | number[] = 1;
  /** true si solo se desea ingresar numeros. */
  @Input() onlyNumbers: boolean = true;

  @ViewChildren(RidPinCodeItemComponent)
  private _pinCodeItemsComponentes!: QueryList<RidPinCodeItemComponent>;

  private _onChangesFn!: (value: string) => {};
  private _onTouched!: () => {};
  private _firstChangeValue: string | number = '';
  private _firstChange: boolean = true;
  private _pinsDisabled: boolean = false;

  constructor(private _changeDetectorRef: ChangeDetectorRef) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['onlyNumbers']
      && !changes['onlyNumbers'].isFirstChange() && changes['onlyNumbers'].currentValue) {
      if (isNaN(Number(this._getValue()))) {
        this._updateInputsValue('');
      }
    }
  }

  ngAfterViewInit(): void {
    if (this._firstChangeValue !== '') {
      this._updateInputsValue(this._firstChangeValue);
      this._changeDetectorRef.detectChanges();
    }
  }

  getInputMaxLength(index: number): number {
    return typeof this.maxLengthPerPin === 'number' ?
      this.maxLengthPerPin :
      this.maxLengthPerPin[index] || 1;
  }

  moveTo(index: number): void {
    if (index !== this.inputsLength) {
      const aux = this._pinCodeItemsComponentes.toArray()[index];
      if (aux) {
        aux.focus();
      }
    }
    this._onChange();
  }

  onItemBlur(): void {
    if (this._onTouched) {
      this._onTouched();
    }
  }

  private _onChange(): void {
    if (this._onChangesFn) {
      this._onChangesFn(this._getValue());
    }
  }

  private _getValue(): string {
    const aux = this._pinCodeItemsComponentes.toArray();
    return aux ? aux.map(a => a.currentValue).join('') : '';
  }

  private _updateInputsValue(newValue: string | number): void {
    const items = this._pinCodeItemsComponentes ? this._pinCodeItemsComponentes.toArray() : [];
    if (this.onlyNumbers && isNaN(Number(newValue))) {
      items.forEach((item: RidPinCodeItemComponent, index: number) => {
        item.control.setValue('', { emitEvent: false });
      });
      throw Error('El valor ingresado no es numerico.');
    }
    let from = 0;
    items.forEach((item: RidPinCodeItemComponent, index: number) => {
      let to = from + this.getInputMaxLength(index);
      let aux = newValue.toString().substring(from, to);
      if (aux) {
        item.control.setValue(aux, { emitEvent: false });
      } else {
        item.control.setValue('', { emitEvent: false });
      }
      from = to;
    });
  }

  /** IMPLEMENTACION CONTROL VALUE ACCESOR */
  writeValue(value: string | number): void {
    if (this._pinsDisabled) {
      return;
    }
    if (this._firstChange) {
      this._firstChange = false;
      this._firstChangeValue = value;
    } else {
      this._updateInputsValue(value);
      this._onChange();
    }
  }
  registerOnChange(fn: any): void {
    this._onChangesFn = fn;
  }
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    this._pinsDisabled = isDisabled;
    if (this._pinCodeItemsComponentes) {
      this._pinCodeItemsComponentes.forEach(a => a.setDisabled(isDisabled));
    }
    this._onChange();
  }

}
