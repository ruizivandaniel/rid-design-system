import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RidPinCodeItemComponent } from './components/pin-code-item.component';
import { RidPinCodeComponent } from './pin-code.component';

@NgModule({
  declarations: [
    RidPinCodeComponent,
    RidPinCodeItemComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
  exports: [
    RidPinCodeComponent,
    RidPinCodeItemComponent,
  ]
})
export class RidPinCodeModule { }
