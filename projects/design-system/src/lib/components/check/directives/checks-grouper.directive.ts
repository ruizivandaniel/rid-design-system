import { ContentChildren, Directive, EventEmitter, Input, Output, QueryList } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { RidCheckType } from '../models/check-type.model';
import { RidChecksGroupEvent, RidCheckStatus } from '../models/checks-group-event.model';
import { RidCheckComponent } from '../check.component';

@Directive({
  selector: 'ridChecksGrouper, [ridChecksGrouper]',
  exportAs: 'ridChecksGrouper'
})
export class RidChecksGrouperDirective {
  /** Tipo de checks. */
  @Input() type: RidCheckType = 'checkbox';
  /** Evento que se emite al modificarse alguno de los checks. */
  @Output() onChange = new EventEmitter<RidChecksGroupEvent>();

  @ContentChildren(RidCheckComponent, {
    descendants: true,
    emitDistinctChangesOnly: true,
    read: RidCheckComponent
  }) private _checkboxes!: QueryList<RidCheckComponent>;

  get currentStatus(): RidCheckStatus[] {
    return this._getCurrentStatus();
  }

  private _unsubscriber = new Subject<void>();

  ngAfterContentInit(): void {
    this._setCheckboxes(this._checkboxes.toArray());
    this._checkboxes.changes
      .subscribe((changes: QueryList<RidCheckComponent>) => {
        this._setCheckboxes(changes.toArray());
      })
  }

  ngOnDestroy(): void {
    this._unsubscriber.next();
    this._unsubscriber.complete();
  }

  private _setCheckboxes(checkboxes: RidCheckComponent[]): void {
    checkboxes.forEach((checkbox: RidCheckComponent) => {
      checkbox.setType = this.type;
      checkbox.onChecked
        .pipe(takeUntil(this._unsubscriber))
        .subscribe((checked: boolean) => {
          this._emitCheckboxesStatus();
        });
    })
  }

  private _emitCheckboxesStatus(): void {
    this.onChange.emit(this._getCurrentStatus());
  }

  private _getCurrentStatus(): RidCheckStatus[] {
    return this._checkboxes.toArray()
      .map((checkbox: RidCheckComponent) => {
        return {
          checked: checkbox.checked,
          value: checkbox.value,
          id: checkbox.id,
          label: checkbox.label
        } as RidCheckStatus;
      });
  }
}
