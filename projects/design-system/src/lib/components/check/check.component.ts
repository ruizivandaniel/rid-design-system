import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { v4 } from 'uuid';

@Component({
  selector: 'rid-check',
  templateUrl: 'check.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RidCheckComponent),
      multi: true,
    }
  ]
})
export class RidCheckComponent implements OnInit, OnChanges, ControlValueAccessor {

  /** Identificador del check. */
  @Input() id: string = '';
  /** Texto a mostrar en el check. */
  @Input() label: string = '';
  /** Texto descriptivo a mostrar en el check. */
  @Input() description: string = '';
  /** Contexto del checkbox. */
  @Input() value!: any;
  /** Valor del check. */
  @Input() default: boolean = false;
  /** True si se desea deshabilitar el check. */
  @Input() isDisabled: boolean = false;
  /** Emite evento cuando se chequea o deschequea. */
  @Output() onChecked = new EventEmitter<boolean>();

  protected _type: 'radio' | 'checkbox' = 'checkbox';
  protected _name: string = '';
  protected _disabled: boolean = false;
  private _checked: boolean = false;
  private _onChangeFn!: (checked: boolean) => void;
  private _onTouchedFn!: () => void;

  get checked(): boolean {
    return this._checked;
  }

  set setName(newName: string) {
    this._name = newName;
  }

  set setType(newName: string) {
    this._name = newName;
  }

  ngOnInit(): void {
    if (!this.id) {
      this.id = v4();
    }
    if (this.default) {
      this._checked = this.default;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['isDisabled']) {
      this._disabled = changes['isDisabled'].currentValue;
    }
  }

  onChange(event: any): void {
    this.updateState(event.target.checked);
  }

  updateState(newValue: boolean, omitEvent: boolean = false): void {
    this._checked = newValue;
    if (!omitEvent) {
      this._onChangeFn && this._onChangeFn(this._checked);
      this.onChecked.emit(this._checked);
    }
  }

  onBlur(): void {
    this._onTouchedFn && this._onTouchedFn();
  }

  /** Implementacion del control value accesor */
  writeValue(checked: boolean): void {
    if (this._checked === checked) {
      return;
    }
    if (checked === undefined || checked === null) {
      this._checked = this.default || false;
      return;
    }
    this.updateState(checked);
  }
  registerOnChange(fn: any): void {
    this._onChangeFn = fn;
  }
  registerOnTouched(fn: any): void {
    this._onTouchedFn = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    this._disabled = isDisabled;
  }

}
