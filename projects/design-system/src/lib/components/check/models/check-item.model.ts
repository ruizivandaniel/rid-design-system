export interface RidCheckItem {
  /** Texto a mostrar para el item. */
  label: string;
  /** Valor del item que se emite al clickear. */
  value: any;
}
