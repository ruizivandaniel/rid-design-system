export interface RidCheckStatus {
  /** Identificador del checkbox. */
  id: string;
  /** Label del checkbox. */
  label: string;
  /** Valor opcional del checkbox. */
  value?: any;
  /** True si esta seleccionado el checkbox. */
  checked: boolean;
};

export type RidChecksGroupEvent = RidCheckStatus[];
