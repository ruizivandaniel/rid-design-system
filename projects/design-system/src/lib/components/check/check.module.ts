import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidCheckComponent } from './check.component';
import { FormsModule } from '@angular/forms';
import { RidChecksGrouperDirective } from './directives/checks-grouper.directive';

@NgModule({
  declarations: [
    RidCheckComponent,
    RidChecksGrouperDirective,
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    RidCheckComponent,
    RidChecksGrouperDirective,
  ],
})
export class RidCheckModule { }
