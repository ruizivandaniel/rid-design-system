import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidDynamicTableComponent } from './dynamic-table.component';
import { RidOrderByDirective } from './directives/dynamic-table-order-by.directive';
import { RidOrderingTableDirective } from './directives/dynamic-table-ordering.directive';
import { RidSvgIconsModule } from './../svg-icons/svg-icons.module';
import { RidBannerModule } from './../banner/banner.module';
import { RidPaginationModule } from '../pagination/pagination.module';
import { RidButtonModule } from '../button/button.module';
import { RidScrollableDirective } from '@rid/core';

@NgModule({
  declarations: [
    RidOrderByDirective,
    RidOrderingTableDirective,
    RidDynamicTableComponent,
  ],
  imports: [
    CommonModule,
    RidSvgIconsModule,
    RidBannerModule,
    RidPaginationModule,
    RidButtonModule,
    RidScrollableDirective,
  ],
  exports: [
    RidDynamicTableComponent,
  ],
})
export class RidDynamicTableModule { }
