import { Component, ElementRef, Input, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject, fromEvent, takeUntil } from 'rxjs';
import { RidOrderType } from '../models/dynamic-table-ordering-event.model';

/**
 * Componente que se expone como una directiva (necesario
 * para poder manejar el ngcontent y agregarle los sort
 * arrows). Este componente es solo valido para la directiva
 * "RidOrderingTableDirective".
 */
@Component({
  selector: 'ridOrderBy, [ridOrderBy]',
  exportAs: 'ridOrderBy',
  template: `
    <div class="d-flex cursor-pointer justify-content-start align-items-center">
        <ng-content></ng-content>
        <div *ngIf="ridOrderBy" class="d-flex flex-column justify-content-center ps-1">
          <div *ngIf="ordering === 'asc' || ordering === ''" [style.minHeight.px]="8">
            <rid-svg class="text-muted" [url]="'assets/caret-up-fill.svg'"
              [width]="8" [height]="8">
            </rid-svg>
          </div>
          <div *ngIf="ordering === 'desc' || ordering === ''" [style.minHeight.px]="8">
            <rid-svg class="text-muted" [url]="'assets/caret-down-fill.svg'"
              [width]="8" [height]="8">
            </rid-svg>
          </div>
        </div>
    </div>
  `
})
export class RidOrderByDirective implements OnInit, OnDestroy {

  /**
   * Nombre de la propiedad que deberá informar al clickear
   * sobre el elemento que se la implementa
   * (ej: 'name', 'data.name').
   */
  @Input('ridOrderBy') ridOrderBy!: string;

  ordering: RidOrderType = '';

  private _onClick = new Subject<string>();
  private _unsubscriber = new Subject<void>();

  get onClicked(): Observable<string> {
    return this._onClick.asObservable();
  }

  constructor(private _elementRef: ElementRef<any>) { }

  ngOnInit(): void {
    if (this.ridOrderBy) {
      fromEvent(this._elementRef.nativeElement, 'click')
        .pipe(takeUntil(this._unsubscriber))
        .subscribe(() => {
          this._onClick.next(this.ridOrderBy);
        });
    }
  }

  ngOnDestroy(): void {
    this._unsubscriber.next();
    this._unsubscriber.complete();
  }

}
