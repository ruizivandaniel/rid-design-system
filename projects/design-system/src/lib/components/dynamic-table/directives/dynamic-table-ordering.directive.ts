import {
  Directive, QueryList, Output, Input,
  EventEmitter, ContentChildren, AfterContentInit
} from '@angular/core';
import { RidOrderByDirective } from './dynamic-table-order-by.directive';
import { Subject, takeUntil } from 'rxjs';
import { RidOrderType, RidTableOrderer, RidTableOrdering } from './../models/dynamic-table-ordering-event.model';

@Directive({
  selector: 'ridOrderingTable, [ridOrderingTable]',
  exportAs: 'ridOrderingTable',
})
export class RidOrderingTableDirective implements AfterContentInit {

  /**
   * Funcion que se ejecuta al clickear sobre una directiva "OrderBy".
   * Recibre como parametro la funcion ya armada para realizar el sort.
   */
  @Input('ridOrderingTable')
  orderingFn!: (orderer: RidTableOrderer) => void;

  @ContentChildren(RidOrderByDirective, { descendants: true })
  private _orderByItems!: QueryList<RidOrderByDirective>;

  private _currentOrdering: RidTableOrdering = { order: '', prop: '' };
  private _unsubscriber = new Subject<void>();

  ngAfterContentInit(): void {
    this._orderByItems.toArray()
      .forEach((item: RidOrderByDirective) => {
        item.onClicked
          .pipe(takeUntil(this._unsubscriber))
          .subscribe((newProp: string) => {
            if (this._currentOrdering.prop !== newProp) {
              this._currentOrdering = {
                prop: newProp,
                order: 'desc'
              };
            } else {
              if (this._currentOrdering.order !== '') {
                if (this._currentOrdering.order === 'desc') {
                  this._currentOrdering.order = 'asc';
                } else if (this._currentOrdering.order === 'asc') {
                  this._currentOrdering.order = '';
                }
              } else {
                this._currentOrdering.order = 'desc';
              }
            }
            item.ordering = this._currentOrdering.order;
            this.orderingFn(this._doSortByFn(this._currentOrdering.prop, this._currentOrdering.order));
          });
      });
  }

  /**
   * Retorna una funcion util para utilizar en la funcion js nativa sort().
   *
   * @param prop propiedad del elemento a evaluar.
   * @param orderType tipo de ordenamiento ascendente o descendente.
   */
  private _doSortByFn(prop: string, orderType: RidOrderType): (a: any, b: any) => number {
    /**
     * Esta funcion busca la propiedad (anidada o no) del objeto.
     * Si la propiedad esta anidada se debe indicar por puntos las distintas
     * props hasta llegar a la necesaria. Ej: "cotizacion.ticker.id"
     * retornara el valor de "id".
     */
    const getProp = (obj: any, prop: string) => {
      let r: any;
      prop.split('.')
        .forEach((a, i) => {
          r = !i ? obj[a] : r[a];
        });
      return r;
    };
    return (a: any, b: any) => {
      if (orderType === '' || getProp(a, prop) === getProp(b, prop)) {
        return 0;
      }
      const result = getProp(a, prop) > getProp(b, prop) ? 1 : -1;
      return orderType === 'desc' ? -1 * result : result;
    };
  }

}
