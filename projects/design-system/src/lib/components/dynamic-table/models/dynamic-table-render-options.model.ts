export interface RidDynamicTableRenderOptions {
  /** Sobrescribe el intervalo de tiempo entre cada renderizacion de los items. */
  renderInterval?: number;
  /** Sobrescribe la cantidad de elementos a renderizar por pasada. */
  itemsPerRender?: number;
}
