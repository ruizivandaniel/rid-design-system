export type RidTableOrderer = (a: any, b: any) => number;

export type RidOrderType = 'asc' | 'desc' | '';

export interface RidTableOrdering {
  prop: string;
  order: RidOrderType;
}
