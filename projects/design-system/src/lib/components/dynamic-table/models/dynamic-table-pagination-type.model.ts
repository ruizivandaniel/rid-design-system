/** Posibles tipos de pagination para cargar mas data en la tabla. */
export type RidDynamicTablePaginationType = 'none' | 'scroll' | 'pagination' | 'button';
