/** Configuracion del placeholder en caso de no haber data en la tabla. */
export interface RidDynamicTablePlaceholderConfig {
  /** Url de la imagen del placeholder. */
  iconUrl?: string;
  /** Mensaje del placeholder. */
  message?: string;
  /** Descripcion del placeholder. */
  description?: string;
}
