export interface RidDynamicTableHeader {
  /** Texto que mostrará la columna. */
  label: string;
  /** Propiedad con la que filtrar el array  */
  sortBy?: string;
}
