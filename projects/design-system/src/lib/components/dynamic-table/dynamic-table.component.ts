import {
  Component, SimpleChanges, ChangeDetectorRef, ViewContainerRef,
  TemplateRef, Input, ContentChild, ViewChild, ElementRef, OnDestroy,
  OnChanges, AfterViewInit, OnInit,
} from '@angular/core';
import { RidDynamicRenderer, RidScrollEvent, RidScrollService } from '@rid/core';
import { RidTableOrderer } from './models/dynamic-table-ordering-event.model';
import { RidDynamicTableHeader } from './models/dynamic-table-header.model';
import { RidDynamicTableRenderOptions } from './models/dynamic-table-render-options.model';
import { RidDynamicTablePlaceholderConfig } from './models/dynamic-table-placeholder-config.model';
import { RidDynamicTablePaginationType } from './models/dynamic-table-pagination-type.model';
import { Subscription } from 'rxjs';
import { v4 } from 'uuid';


@Component({
  selector: 'rid-dynamic-table',
  templateUrl: 'dynamic-table.component.html',
})
export class RidDynamicTableComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  /**
   * Identificador unico de la tabla. Se implementa para el
   * renderizado dinamico por scroll.
   */
  @Input() id!: string;
  /** Listado de nombres de las columnas, con su sortBy. */
  @Input() headers!: RidDynamicTableHeader[];
  /** Array de datos con los que se crearan las filas. */
  @Input() dataRows!: any[];
  /** Mensaje que se muestra en caso de no haber data. */
  @Input() placeholderConfig: RidDynamicTablePlaceholderConfig = { message: 'No data.' };
  /** Opciones de renderizado. */
  @Input() renderOptions!: RidDynamicTableRenderOptions;
  /** Configuracion adicional de la tabla. */
  @Input() paginationType: RidDynamicTablePaginationType = 'none';
  /** TemplateRef que se utilizara para el renderizado de cada row. */
  @ContentChild('ridTemplateRow', { read: TemplateRef })
  private _itemTemplate!: TemplateRef<any>;

  @ViewChild('ridTableContainer', { read: ViewContainerRef, static: false })
  private _tableContainer!: ViewContainerRef;
  @ViewChild('mainContainer', { read: ElementRef<any>, static: false })
  private _mainContainer!: ElementRef<any>;

  totalPages: number = 0;
  mainContainerMaxHeight: number = -1;

  private _dynamicRenderer!: RidDynamicRenderer;
  private _scrollSubscription!: Subscription;

  constructor(private _changeDetectorRef: ChangeDetectorRef,
    private _scrollService: RidScrollService) { }

  ngOnInit(): void {
    if (!this.id) {
      this.id = v4();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes['renderOptions'] &&
      !changes['renderOptions'].isFirstChange() &&
      changes['renderOptions'].currentValue !==
      changes['renderOptions'].previousValue
    ) {
      this._dynamicRenderer.restart(changes['renderOptions'].currentValue);
      this._dynamicRenderer.render();
    }
    if (
      changes['dataRows'] &&
      !changes['dataRows'].isFirstChange() &&
      changes['dataRows'].currentValue !== changes['dataRows'].previousValue
    ) {
      this._checkTotalPages();
      this._dynamicRenderer.restart({
        itemsData: changes['dataRows'].currentValue,
      });
      this._dynamicRenderer.render();
    }
    if (
      changes['tableHeaders'] &&
      !changes['tableHeaders'].isFirstChange() &&
      changes['tableHeaders'].currentValue !==
      changes['tableHeaders'].previousValue
    ) {
      this._dynamicRenderer.restart();
      this._dynamicRenderer.render();
    }
    if (
      changes['paginationType'] &&
      !changes['paginationType'].isFirstChange() &&
      changes['paginationType'].currentValue !==
      changes['paginationType'].previousValue
    ) {
      this._dynamicRenderer.restart();
      this._dynamicRenderer.render();
      if (this.paginationType === 'scroll') {
        this._setMainContainerMaxHeight();
        this._subscribeToScrollBottom();
      } else {
        this._unsetMainContainerMaxHeight();
        this._unsubscribeToScrollBottom();
      }
    }
  }

  ngAfterViewInit(): void {
    if (this.paginationType === 'scroll') {
      this._setMainContainerMaxHeight();
      this._subscribeToScrollBottom();
    }
    this._dynamicRenderer = new RidDynamicRenderer({
      itemsData: this.dataRows,
      template: this._itemTemplate,
      viewContainerRef: this._tableContainer,
      changesDetectorRef: this._changeDetectorRef,
      options: this.renderOptions,
    });
    this._checkTotalPages();
    this._dynamicRenderer.render();
  }

  ngOnDestroy(): void {
    this._unsubscribeToScrollBottom();
  }

  orderChange = (orderer: RidTableOrderer): void => {
    this._dynamicRenderer.restart({
      itemsData: this.dataRows.slice().sort(orderer),
    });
    this._dynamicRenderer.render();
  }

  render(sliceFrom?: number): void {
    this._dynamicRenderer.render(sliceFrom);
  }

  onPageChanged(page: number): void {
    this.render((page - 1) * (this._dynamicRenderer.config.itemsPerRender ?? 20));
  }

  showLoadMoreBtn(): boolean {
    return this._dynamicRenderer && this._dynamicRenderer.canRender;
  }

  private _setMainContainerMaxHeight(): void {
    this.mainContainerMaxHeight = this._mainContainer.nativeElement.offsetHeight - 20;
  }

  private _unsetMainContainerMaxHeight(): void {
    this.mainContainerMaxHeight = -1;
  }

  private _subscribeToScrollBottom(): void {
    this._scrollSubscription = this._scrollService
      .scrolledToBottom(this.id)
      .subscribe((event: RidScrollEvent) => {
        this._dynamicRenderer.render();
        if (!this._dynamicRenderer.canRender) {
          this._scrollSubscription.unsubscribe();
        }
      });
  }

  private _unsubscribeToScrollBottom(): void {
    if (this._scrollSubscription) {
      this._scrollSubscription.unsubscribe();
    }
  }

  private _checkTotalPages(): void {
    this.totalPages = Math.ceil(this.dataRows.length / (this._dynamicRenderer.config?.itemsPerRender ?? 20));
  }
}
