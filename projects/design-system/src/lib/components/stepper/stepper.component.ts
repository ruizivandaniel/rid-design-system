import {
  Component, ContentChildren, TemplateRef, AfterContentInit, QueryList,
  OnDestroy, Input, Output, EventEmitter,
} from '@angular/core';
import { RidLoader } from '@rid/core';
import { Subject, takeUntil } from 'rxjs';
import { RidStepDirective } from './directives/step.directive';
import { StepperCloseReason } from './models/stepper-close-reason.model';

@Component({
  selector: 'rid-stepper',
  exportAs: 'ridStepper',
  templateUrl: 'stepper.component.html',
})
export class RidStepperComponent implements AfterContentInit, OnDestroy {
  /** True si se desea visualizar la cruz que emite el 'onClose'. */
  @Input() showCloseBtn: boolean = true;
  /** True si se desea visualizar las lineas que indican en que step estas. */
  @Input() showStepsStatus: boolean = true;
  /** True si se desea visualizar los titulos de cada step. */
  @Input() showStepsTitle: boolean = true;
  /** True si se desea visualizar las descripciones de cada step. */
  @Input() showStepsDescription: boolean = true;
  /** True si se desea visualizar los botones next y prev. */
  @Input() showFooterButtons: boolean = true;
  /** Evento que se emite al clickear en la cruz. */
  @Output() finalized = new EventEmitter<StepperCloseReason>();

  // TODO: Manejar los errores de los observables automaticamente. Si falla,
  // enviar al step error mostrando un mensaje generico y que tenga un boton
  // que permita volver al step uno (o al anterior).

  @ContentChildren(TemplateRef<any>, {
    read: RidStepDirective,
    descendants: false,
    emitDistinctChangesOnly: true
  })
  private _stepsList!: QueryList<RidStepDirective>;

  private _steps: RidStepDirective[] = [];
  private _currentStep!: RidStepDirective;
  private _loader = new RidLoader();
  private _unsubscriber = new Subject<void>();

  get isLoading(): boolean {
    return this._loader.isLoading();
  }

  get currentStep(): RidStepDirective {
    return this._currentStep;
  }

  get steps(): RidStepDirective[] {
    return this._steps;
  }

  ngAfterContentInit(): void {
    this._steps = this._stepsList.toArray();
    this._stepsList.changes
      .pipe(takeUntil(this._unsubscriber))
      .subscribe((changes: QueryList<RidStepDirective>) => {
        this._steps = changes.toArray();
      });
    this._steps = this._steps.map((s: RidStepDirective, index: number) => {
      if (!s.id) {
        s.id = `${new Date().getTime()}_${index}`;
      }
      return s;
    });
    const filtered = this._steps.filter((s: RidStepDirective) => s.showStepStatus);
    if (filtered.length > 1) {
      filtered[filtered.length - 1].isLast = true;
      filtered[0].isFirst = true;
    } else if (filtered.length === 1) {
      filtered[0].isLast = true;
      filtered[0].isFirst = true;
    }
    this.goToStep(filtered[0]);
  }

  ngOnDestroy(): void {
    this._unsubscriber.next();
    this._unsubscriber.complete();
  }

  goToStep(step: RidStepDirective): void {
    this._currentStep = step;
    console.log(this._currentStep);

  }

  goToStepById(id: string): void {
    const finded: RidStepDirective | undefined = this._steps
      .find((step: RidStepDirective) => step.id === id);
    if (finded) {
      this.goToStep(finded);
    }
  }

  goToStepByTitle(title: string): void {
    const finded: RidStepDirective | undefined = this._steps
      .find((step: RidStepDirective) => step.title === title);
    if (finded) {
      this.goToStep(finded);
    }
  }

  nextStep(): void {
    const stepInstance = this._currentStep;
    if (!stepInstance.nextStepHandler) {
      if (!stepInstance.isLast && !stepInstance.omitStepChange) {
        const auxIndex = this._steps.findIndex(s => stepInstance === s);
        if (auxIndex !== -1) {
          this.goToStep(this._steps[auxIndex + 1]);
        }
      } else if (stepInstance.isLast) {
        this.finalized.emit('finalized');
      }
      return;
    }
    this._loader.load(stepInstance.nextStepHandler())
      .pipe(takeUntil(this._unsubscriber))
      .subscribe(() => {
        if (!stepInstance.isLast && !stepInstance.omitStepChange) {
          const auxIndex = this._steps.findIndex(s => stepInstance === s);
          if (auxIndex !== -1) {
            this.goToStep(this._steps[auxIndex + 1]);
          }
        } else if (stepInstance.isLast) {
          this.finalized.emit('finalized');
        }
        this._unsubscriber.next();
      });
  }

  prevStep(): void {
    const stepInstance = this._currentStep;
    if (!stepInstance.prevStepHandler) {
      if (!stepInstance.isFirst && !stepInstance.omitStepChange) {
        const auxIndex = this._steps.findIndex(s => stepInstance === s);
        if (auxIndex !== -1) {
          this.goToStep(this._steps[auxIndex - 1]);
        }
      }
      return;
    }
    this._loader.load(stepInstance.prevStepHandler())
      .pipe(takeUntil(this._unsubscriber))
      .subscribe(() => {
        if (!stepInstance.isFirst && !stepInstance.omitStepChange) {
          const auxIndex = this._steps.findIndex(s => stepInstance === s);
          if (auxIndex !== -1) {
            this.goToStep(this._steps[auxIndex - 1]);
          }
        }
        this._unsubscriber.next();
      });
  }

  doClose(): void {
    this.finalized.emit('icon');
  }

}
