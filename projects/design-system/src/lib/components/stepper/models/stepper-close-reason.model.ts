export type StepperCloseReason = 'icon' | 'error' | 'finalized';
