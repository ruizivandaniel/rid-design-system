import { Directive, Input, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';

@Directive({
  selector: 'ridStep, [ridStep]',
  exportAs: 'ridStep',
})
export class RidStepDirective {
  /** Identificador del paso. */
  @Input() id: string = '';
  /** Titulo del paso. */
  @Input() title: string = ' ';
  /** Descripcion del paso. */
  @Input() description: string = ' ';
  /** Fuerza el texto a mostrar en el boton de paso anterior*/
  @Input() prevStepLabel!: string;
  /** Fuerza el texto a mostrar en el boton de paso siguiente*/
  @Input() nextStepLabel!: string;
  /** Funcion handler al ir al paso anterior. */
  @Input() prevStepHandler!: () => Observable<any>;
  /** Funcion handler al ir al paso siguiente. */
  @Input() nextStepHandler!: () => Observable<any>;
  /** true si no se desea mostrar el boton de paso anterior. */
  @Input() hideGoBack: boolean = false; // TODO: Implementar.
  /** True si se desea omitir el cambio de step (para poder hacerlo de manera manual). */
  @Input() omitStepChange: boolean = false;
  /** True si se desea omitir la vista en el status de steps en el stepper component. */
  @Input() showStepStatus: boolean = true;

  // TODO: @Input() order: string = ''; ????? deberia?

  isLast: boolean = false;
  isFirst: boolean = false;

  get template(): TemplateRef<any> {
    return this._elementRef;
  }

  constructor(private _elementRef: TemplateRef<any>) { }

}
