import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidStepperComponent } from './stepper.component';
import { RidStepDirective } from './directives/step.directive';
import { RidButtonModule } from '../button/button.module';
import { RidLoaderModule } from '../loader/loader.module';
import { RidSvgIconsModule } from '../svg-icons/svg-icons.module';

@NgModule({
  declarations: [
    RidStepperComponent,
    RidStepDirective,
  ],
  imports: [
    CommonModule,
    RidButtonModule,
    RidLoaderModule,
    RidSvgIconsModule,
  ],
  exports: [
    RidStepperComponent,
    RidStepDirective,
  ],
})
export class RidStepperModule { }
