import { Component, Input } from '@angular/core';
import { RidFloatingMenuItem } from './models/floating-menu-item.model';

@Component({
  selector: 'rid-floating-menu',
  templateUrl: 'floating-menu.component.html',
  styleUrls: ['./floating-menu.component.scss']
})
export class RidFloatingMenuComponent {
  /** Elementos a mostrar por el menu. */
  @Input() items: RidFloatingMenuItem[] = [];
  /** Fuerza el espacio en pixeles que tendra por encima el menu.  */
  @Input() offsetTop: number = 150;
  /** Limita la cantidad maxima de elementos hasta ocultarlos. */
  @Input() maxVisibleItems: number = 6;
  /** Hace que se cierre el menu al clickear fuera del componente */
  @Input() closeOnBackDropClick: boolean = false;

  private _opened: boolean = false;
  private _showAllElements: boolean = false;

  get isOpen(): boolean {
    return this._opened;
  }

  get showAllElements(): boolean {
    return this._showAllElements;
  }

  toggle(): void {
    this._opened = !this._opened;
    this._showAllElements = false;
  }

  toggleElements(): void {
    this._showAllElements = !this._showAllElements;
  }

  open(): void {
    if (!this._opened) {
      this._opened = true;
    }
  }

  close = (): void => {
    if (this._opened) {
      this._opened = false;
    }
  };

  doItemAction(item: RidFloatingMenuItem): void {
    if (item.action && typeof item.action === 'function') {
      item.action();
      this.close();
    }
  }
}
