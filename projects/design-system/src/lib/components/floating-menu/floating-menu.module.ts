import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidFloatingMenuComponent } from './floating-menu.component';
import { RidSvgIconsModule } from '../svg-icons/svg-icons.module';
import { RidClickedOutsideDirective } from '@rid/core';

@NgModule({
  declarations: [RidFloatingMenuComponent],
  imports: [
    CommonModule,
    RidClickedOutsideDirective,
    RidSvgIconsModule,
  ],
  exports: [RidFloatingMenuComponent],
})
export class RidFloatingMenuModule { }
