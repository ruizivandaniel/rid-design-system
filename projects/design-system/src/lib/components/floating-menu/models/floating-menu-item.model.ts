export interface RidFloatingMenuItem {
  /** Url del icono del item. */
  iconUrl: string;
  /** Texto que se mostrará en el tooltip del item. */
  label?: string;
  /** Accion a realizar cuando se clickea el item. */
  action: () => void;
}
