/** Item del switch */
export interface RidSwitchItem {
  /** Identificador del item. */
  id?: string;
  /** Texto que mostrará la opcion. */
  label?: string;
  /**
   * Contexto a utilizar en el switch. Si no se
   * pasa el value, se tomará como valor el label.
   */
  value?: any;
  /** Icono del switch. */
  iconUrl?: string;
}
