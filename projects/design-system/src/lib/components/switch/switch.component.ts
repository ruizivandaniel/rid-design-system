import { Component, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { RidSwitchItem } from './models/switch-item.model';
import { v4 } from 'uuid';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'rid-switch',
  templateUrl: 'switch.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RidSwitchComponent),
      multi: true,
    }
  ]
})
export class RidSwitchComponent implements OnInit, OnChanges, ControlValueAccessor {
  /** Items a mostrar en el switch. */
  @Input() items: RidSwitchItem[] = [];
  /** Item o index por defecto al inicializar el componente. */
  @Input() default: RidSwitchItem | number = 0;
  /** True si se desea deshabilitar el switch. */
  @Input() isDisabled: boolean = false;
  /** Evento que informa que el switch cambio. */
  @Output() change = new EventEmitter<any>();

  private _currentValue!: RidSwitchItem;
  private _onChangeFn!: (checked: boolean) => void;
  private _onTouchedFn!: () => void;

  get currentValue(): RidSwitchItem {
    return this._currentValue;
  }

  ngOnInit(): void {
    this._refreshItems();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['items'] && !changes['items'].isFirstChange() &&
      changes['items'].currentValue !== changes['items'].previousValue) {
      this._refreshItems();
    }
  }

  switchToItem(item: RidSwitchItem): void {
    this._currentValue = item;
    this.change.emit(item.value ?? item.label);
    this._onChangeFn && this._onChangeFn(item.value ?? item.label);
    this._onTouchedFn && this._onTouchedFn();
  }

  private _refreshItems(): void {
    this.items = this.items.map((item: RidSwitchItem) => {
      return {
        ...item,
        id: item.id || v4(),
      };
    });
    if (typeof this.default === 'number') {
      this._currentValue = this.items[this.default] ?? this.items[0];
    } else if (this.default) {
      const finded = this.items.find((i: RidSwitchItem) =>
        i.id === (this.default as RidSwitchItem).id ||
        i.label === (this.default as RidSwitchItem).label
      );
      if (finded) {
        this._currentValue = finded;
      }
    }
  }

  /** Implementacion de control value accesor. */
  writeValue(item: RidSwitchItem): void {
    const finded = this.items.find((i: RidSwitchItem) => i.id === item.id);
    if (finded) {
      this.switchToItem(finded);
    }
  }
  registerOnChange(fn: any): void {
    this._onChangeFn = fn;
  }
  registerOnTouched(fn: any): void {
    this._onTouchedFn = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

}
