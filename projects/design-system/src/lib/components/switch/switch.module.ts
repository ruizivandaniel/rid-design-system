import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidSwitchComponent } from './switch.component';
import { RidButtonModule } from '../button/button.module';
import { RidSvgIconsModule } from '../svg-icons/svg-icons.module';

@NgModule({
  declarations: [
    RidSwitchComponent,
  ],
  imports: [
    CommonModule,
    RidButtonModule,
    RidSvgIconsModule,
  ],
  exports: [
    RidSwitchComponent,
  ],
})
export class RidSwitchModule { }
