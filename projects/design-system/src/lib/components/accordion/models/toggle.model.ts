export interface RidAccordionToggleEvent {
  index: number;
  activeIndexes: number[];
  isMultiple: boolean;
  opened: boolean;
  closed: boolean;
}
