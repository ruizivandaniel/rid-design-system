import { Component, Input, ContentChild, TemplateRef, ChangeDetectionStrategy } from '@angular/core';
import { RidAccordionItemContentDirective } from '../directives/accordion-item-content.directive';
import { RidAccordionItemToggleDirective } from '../directives/accordion-item-toggle.directive';

@Component({
  selector: 'rid-accordion-item',
  template: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RidAccordionItemComponent {
  @Input() title: string = '';

  @ContentChild(TemplateRef, { read: RidAccordionItemToggleDirective, descendants: false })
  private _toggle!: RidAccordionItemToggleDirective;

  @ContentChild(TemplateRef, { read: RidAccordionItemContentDirective, descendants: false })
  private _content!: RidAccordionItemContentDirective;

  get contentTemp(): TemplateRef<any> {
    return this._content?.template;
  }

  get toggleTemp(): TemplateRef<any> {
    return this._toggle?.template;
  }

}
