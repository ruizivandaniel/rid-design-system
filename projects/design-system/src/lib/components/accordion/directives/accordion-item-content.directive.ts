import { Directive, TemplateRef } from '@angular/core';

@Directive({ selector: '[ridAccordionItemContent]' })
export class RidAccordionItemContentDirective {
  get template(): TemplateRef<any> {
    return this._template;
  }
  constructor(private _template: TemplateRef<any>) { }
}
