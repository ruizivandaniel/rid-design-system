import { Directive, TemplateRef } from '@angular/core';

@Directive({ selector: '[ridAccordionItemToggle]' })
export class RidAccordionItemToggleDirective {
  get template(): TemplateRef<any> {
    return this._template;
  }
  constructor(private _template: TemplateRef<any>) { }
}
