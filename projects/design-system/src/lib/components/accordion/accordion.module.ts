import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidSvgIconsModule } from '../svg-icons/svg-icons.module';
import { RidAccordionComponent } from './accordion.component';
import { RidAccordionItemToggleDirective } from './directives/accordion-item-toggle.directive';
import { RidAccordionItemContentDirective } from './directives/accordion-item-content.directive';
import { RidAccordionItemComponent } from './components/accordion-item.component';

@NgModule({
  declarations: [
    RidAccordionComponent,
    RidAccordionItemComponent,
    RidAccordionItemContentDirective,
    RidAccordionItemToggleDirective,
  ],
  imports: [
    CommonModule,
    RidSvgIconsModule,
  ],
  exports: [
    RidAccordionComponent,
    RidAccordionItemComponent,
    RidAccordionItemContentDirective,
    RidAccordionItemToggleDirective,
  ],
})
export class RidAccordionModule { }
