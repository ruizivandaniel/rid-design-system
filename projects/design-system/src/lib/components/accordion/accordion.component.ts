import {
  Component, ContentChildren, QueryList, Output,
  Input, EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { RidAccordionItemComponent } from './components/accordion-item.component';
import { RidAccordionToggleEvent } from './models/toggle.model';

@Component({
  selector: 'rid-accordion',
  templateUrl: 'accordion.component.html',
})
export class RidAccordionComponent implements OnChanges {

  /** True si se desea seleccionar mas de un item */
  @Input() multiple: boolean = false;
  /** True si se desea hacer mas compacto los items */
  @Input() compact: boolean = false;
  /** Evento que emite la informacion de el o los items activos actualmente. */
  @Output() onToggle = new EventEmitter<RidAccordionToggleEvent>();

  @ContentChildren(RidAccordionItemComponent, { descendants: false })
  private _itemsTemps!: QueryList<RidAccordionItemComponent>;

  private _currents: number[] = [];

  get currents(): number[] {
    return this._currents;
  }

  get items(): RidAccordionItemComponent[] {
    return this._itemsTemps ? this._itemsTemps.toArray() : [];
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['multiple']
      && !changes['multiple'].isFirstChange()
      && changes['multiple'].currentValue !== changes['multiple'].previousValue) {
      this._closeAll();
    }
  }

  toggle(itemIndex: number): void {
    const index = this._currents.indexOf(itemIndex);
    if (index !== -1) {
      this._close(index);
    } else {
      this._open(itemIndex);
    }
  }

  private _closeAll(): void {
    this._currents = [];
  }

  private _close(index: number): void {
    this._currents.splice(index, 1);
    this.onToggle.emit({
      closed: true,
      opened: false,
      activeIndexes: this._currents,
      index,
      isMultiple: this.multiple,
    });
  }

  private _open(newIndex: number): void {
    if (!this.multiple) {
      this._currents = [];
    }
    this._currents.push(newIndex);
    this.onToggle.emit({
      closed: false,
      opened: true,
      activeIndexes: this._currents,
      index: newIndex,
      isMultiple: this.multiple,
    });
  }

}
