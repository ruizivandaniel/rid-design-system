import {
  Component, ElementRef, Input, OnInit, ViewChild, Output, EventEmitter,
  OnChanges, SimpleChanges
} from '@angular/core';
import { DateTime, Info } from 'luxon';
import { RidResizeEvent } from '@rid/core';
import { RidCalendarData } from './models/calendar-metadata.model';
import { RidCalendarType } from './models/calendar-type.model';
import { RidValueRangeDateTimes, RidRangeDate } from './models/value-range.model';
import { OnMonthChanged } from './models/month-changed.model';

@Component({
  selector: 'rid-calendar',
  templateUrl: 'calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class RidCalendarComponent implements OnInit, OnChanges {
  /** true si se quiere seleccionar muchas fechas (cancela el range) */
  @Input() type: RidCalendarType = 'single';
  /** fecha o fechas (si es range o multiple) cargadas por default */
  @Input() default!: Date | Date[] | RidRangeDate;
  /** true si se desea ver el boton de 'hoy' */
  @Input() showToday: boolean = true;
  /** true si se desea ver el boton de 'lipiar' */
  @Input() showClear: boolean = true;
  /** Justifica la cantidad de rows del calendario */
  @Input() justifyRows: boolean = false;
  /** conjunto de fechas que no se podran seleccionar */
  @Input() invalidDates: Date[] = [];
  /** fecha minima posible para ser seleccionada */
  @Input() minDate!: Date;
  /** fecha maxima posible para ser seleccionada */
  @Input() maxDate!: Date;
  /** Deshabilita el componente. */
  @Input() disabled: boolean = false;
  /** Oculta el selector de mes y año */
  @Input() showMonthYearSelectors: boolean = true;
  /** Oculta el selector de mes y año */
  @Input() monthYearSelectorsEditable: boolean = true;
  /** Oculta los chevrones que cambian de */
  @Input() showNavigationsArrows: boolean = true;
  /** Evento que indica cambio en el valor de fecha/s */
  @Output() onDateChanged = new EventEmitter<Date | Date[] | RidRangeDate>();
  /** Evento que indica cambio del mes */
  @Output() onMonthChanged = new EventEmitter<OnMonthChanged>();
  /** Evento que indica cambio del año */
  @Output() onYearChanged = new EventEmitter<number>();
  /** Evento que indica click en el boton hoy */
  @Output() onTodayClick = new EventEmitter<void>();
  /** Evento que indica click en el boton limpiar */
  @Output() onClearClick = new EventEmitter<void>();

  @ViewChild('CalendarContainer', { read: ElementRef })
  private _calendarContainer!: ElementRef<any>;

  valueSingle: DateTime | undefined = undefined;
  valueRange: RidValueRangeDateTimes = { from: undefined, to: undefined };
  valueMultiple: DateTime[] = [];
  onResize = (onresize: RidResizeEvent): void => { }

  private _data!: RidCalendarData;
  private _minDate!: DateTime;
  private _maxDate!: DateTime;

  get data(): RidCalendarData {
    return this._data;
  }

  get value(): Date | Date[] | RidRangeDate | undefined {
    switch (this.type) {
      case 'single':
        return this.valueSingle ? (this.valueSingle?.toJSDate() as Date) : undefined;
      case 'multiple':
        return (this.valueMultiple && this.valueMultiple.length) ?
          (this.valueMultiple.map((d) => d.toJSDate()) as Date[]) :
          [];
      case 'range':
        return {
          from: this.valueRange.from ? this.valueRange.from.toJSDate() : undefined,
          to: this.valueRange.to ? this.valueRange.to.toJSDate() : undefined,
        };
      default:
        throw Error(`Tipo ${this.type} no reconocido.`);
    }
  }

  get minYear(): number {
    return this._minDate ? this._minDate.year : 1500;
  }

  get maxYear(): number {
    return this._maxDate ? this._maxDate.year : 9999;
  }

  ngOnInit(): void {
    this._checkDefault();
    this._initMetadata();
    this._setMinMaxDates();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['minDate'] && !changes['minDate'].isFirstChange()
      && changes['minDate'].currentValue !== changes['minDate'].previousValue) {
      if (this.minDate instanceof Date) {
        this._setMinMaxDates();
      } else {
        this.minDate = new Date(this.minDate);
        if (this.minDate instanceof Date) {
          this._setMinMaxDates();
        }
      }
      this.clear();
    }
    if (changes['maxDate'] && !changes['maxDate'].isFirstChange()
      && changes['maxDate'].currentValue !== changes['maxDate'].previousValue) {
      if (this.maxDate instanceof Date) {
        this._setMinMaxDates();
      } else {
        this.maxDate = new Date(this.maxDate);
        if (this.maxDate instanceof Date) {
          this._setMinMaxDates();
        }
      }
      this.clear();
    }
    if (changes['type'] && !changes['type'].isFirstChange()
      && changes['type'].currentValue !== changes['type'].previousValue) {
      this.clear(true);
    }
    if (changes['disabled'] && !changes['disabled'].isFirstChange()) {
      this.clear(true);
    }
  }

  /**
   * TODO: Cambiar esta funcion por una variable que
   * TODO: se actualize en caso de cambiar de tamaño
   * TODO: el contenedor. Hay un ejemplo en el carrousel.
   */
  colWeight(): number {
    if (this._calendarContainer) {
      return (this._calendarContainer.nativeElement.offsetWidth / 7) - 0.1;
    }
    return 0;
  }

  disable(): void {
    this.disabled = true;
  }

  enable(): void {
    this.disabled = false;
  }

  prevMonth(): void {
    this._updateCalendarDate({ month: Number(this.data.calendarDate.month - 1) });
  }

  nextMonth(): void {
    this._updateCalendarDate({ month: Number(this.data.calendarDate.month + 1) });
  }

  updateCalendarDateYear(): void {
    this.data.calendarDate.year = Number(this.data.calendarDate.year);
    this._updateCalendarDate({ year: this.data.calendarDate.year });
  }

  updateCalendarDateMonth(): void {
    this.data.calendarDate.month = Number(this.data.calendarDate.month);
    this._updateCalendarDate({ month: this.data.calendarDate.month });
  }

  selectDate(newDay: number): void {
    const newDate: DateTime = this._getDatetimeByDay(newDay);
    switch (this.type) {
      case 'single':
        if (this.valueSingle
          && this.valueSingle.equals(newDate)) {
          this.valueSingle = undefined;
        } else {
          this.valueSingle = newDate;
        }
        this.onDateChanged.emit(this.value);
        break;
      case 'multiple':
        const index: number = this.valueMultiple
          .findIndex((v: DateTime) => v.equals(newDate));
        if (index !== -1) {
          this.valueMultiple.splice(index, 1);
        } else {
          this.valueMultiple.push(newDate);
        }
        this.onDateChanged.emit(this.value);
        break;
      case 'range':
        if (this.valueRange.from === undefined) {
          this.valueRange.from = newDate;
        } else if (this.valueRange.to === undefined) {
          if (!this.valueRange.from.equals(newDate)) {
            if (this.valueRange.from > newDate) {
              this.valueRange.from = newDate;
            } else {
              this.valueRange.to = newDate;
              this.onDateChanged.emit(this.value);
            }
          }
        } else {
          if (!this.valueRange.to.equals(newDate)) {
            if (newDate > this.valueRange.from) {
              this.valueRange.to = newDate;
              this.onDateChanged.emit(this.value);
            } else {
              this.valueRange.from = newDate;
              this.valueRange.to = undefined;
            }
          }
        }
        break;
    }
  }

  goToToday(): void {
    this._updateCalendarDate({
      month: this.data.today.month,
      year: this.data.today.year
    });
    this.onTodayClick.emit();
  }

  clear(omitEvent: boolean = false): void {
    switch (this.type) {
      case 'single':
        this.valueSingle = undefined;
        break;
      case 'multiple':
        this.valueMultiple = [];
        break;
      case 'range':
        this.valueRange = { from: undefined, to: undefined };
        break;
    }
    if (!omitEvent) {
      this.onClearClick.emit();
      this.onDateChanged.emit(this.value);
    }
  }

  isValidDate(day: number): boolean {
    const auxDate = this._getDatetimeByDay(day);
    if ((this._minDate && auxDate <= this._minDate) || (this._maxDate && auxDate >= this._maxDate)) {
      return false;
    }
    return this.invalidDates
      .find(date => this._clearTime(DateTime.fromJSDate(date)).equals(auxDate)) === undefined;
  }

  isSelectedDate(day: number): string {
    const auxDate = this._getDatetimeByDay(day);
    switch (this.type) {
      case 'single':
        return this.valueSingle !== undefined && this.valueSingle.equals(auxDate)
          ? 'rid-calendar-selected-date' : '';
      case 'multiple':
        return this.valueMultiple.length > 0
          && this.valueMultiple.find(d => d.equals(auxDate)) !== undefined
          ? 'rid-calendar-selected-date' : '';
      case 'range':
        const from = this.valueRange.from;
        const to = this.valueRange.to;
        if (from !== undefined && from.equals(auxDate)) {
          return to === undefined ? 'rid-calendar-selected-date' : 'rid-calendar-selected-date-first';
        } else if (to !== undefined && to.equals(auxDate)) {
          return 'rid-calendar-selected-date-last';
        }
        return '';
      default:
        throw Error(`Tipo ${this.type} no reconocido.`);
    }
  }

  isInRangeDate(day: number): boolean {
    const from = this.valueRange.from;
    const to = this.valueRange.to;
    const auxDate = this._getDatetimeByDay(day);
    return from !== undefined && to !== undefined && (auxDate > from && auxDate < to);
  }

  private _getDatetimeByDay(day: number): DateTime {
    const y = this.data.calendarDate.year;
    const m = this.data.calendarDate.month < 10 ? `0${this.data.calendarDate.month}` : String(this.data.calendarDate.month);
    const d = day < 10 ? `0${day}` : String(day);
    return this._clearTime(DateTime.fromSQL(`${y}-${m}-${d}`));
  }

  private _initMetadata(): void {
    const current = this._clearTime(DateTime.now().set({ day: 1 }));
    this._data = {
      calendarDate: {
        month: current.month,
        monthText: current.monthShort,
        year: current.year,
        datetime: current.set({ day: 1 }),
        monthNumberOfDays: current.daysInMonth,
        monthPostDays: Math.abs(current.weekday + current.daysInMonth - 42),
        monthPreDays: current.weekday - 1,
      },
      today: this._clearTime(DateTime.now()),
      meta: {
        weekDaysText: Info.weekdays().map(d => d.substring(0, 3)),
        yearMonthsText: Info.months('short'),
      }
    };
  }

  private _updateCalendarDate(set: { month?: number, year?: number }): void {
    const newDate = this.data.calendarDate.datetime.set(set);
    this.data.calendarDate = {
      datetime: newDate,
      month: newDate.month,
      monthText: newDate.monthShort,
      year: newDate.year,
      monthNumberOfDays: newDate.daysInMonth,
      monthPostDays: Math.abs(newDate.weekday + newDate.daysInMonth - 42),
      monthPreDays: newDate.weekday - 1,
    };
    if (set.month) {
      this.onMonthChanged.emit({
        number: newDate.month,
        textLong: newDate.monthLong,
        textShort: newDate.monthShort,
      });
    } else if (set.year) {
      this.onYearChanged.emit(set.year);
    }
  }

  private _checkDefault(): void {
    switch (this.type) {
      case 'single':
        if (this.default) {
          this.valueSingle = this._clearTime(DateTime.fromJSDate(this.default as Date));
        }
        break;
      case 'multiple':
        if (this.default && Array.isArray(this.default)) {
          this.valueMultiple = (this.default as Date[]).map(d => this._clearTime(DateTime.fromJSDate(d)));
        }
        break;
      case 'range':
        if (this.default as RidRangeDate) {
          const from = (this.default as RidRangeDate).from;
          const to = (this.default as RidRangeDate).to;
          this.valueRange = {
            from: from ? DateTime.fromJSDate(from) : undefined,
            to: to ? DateTime.fromJSDate(to) : undefined,
          }
        }
        break;
    }
  }

  private _clearTime(date: DateTime): DateTime {
    return date.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
  }

  private _setMinMaxDates(): void {
    if (this.minDate !== undefined && this.minDate !== null && this.minDate instanceof Date) {
      this._minDate = DateTime.fromJSDate(this.minDate);
    } else {
      this._minDate = DateTime.fromJSDate(new Date('1500-01-01'));
    }
    if (this.maxDate !== undefined && this.maxDate !== null && this.maxDate instanceof Date) {
      this._maxDate = DateTime.fromJSDate(this.maxDate);
    } else {
      this._minDate = DateTime.fromJSDate(new Date('9999-31-12'));
    }
  }

}
