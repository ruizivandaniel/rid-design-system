import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RidCoreModule } from '@rid/core';
import { RidSvgIconsModule } from '../svg-icons/svg-icons.module';
import { RidCalendarComponent } from './calendar.component';
import { RidButtonModule } from '../button/button.module';

@NgModule({
  declarations: [
    RidCalendarComponent,
  ],
  imports: [
    CommonModule,
    RidSvgIconsModule,
    RidCoreModule,
    FormsModule,
    RidButtonModule,
  ],
  exports: [
    RidCalendarComponent,
  ],
})
export class RidCalendarModule { }
