/** Posibles tipos de calendario. */
export type RidCalendarType = 'multiple' | 'range' | 'single';
