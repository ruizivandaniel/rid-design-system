import { DateTime } from 'luxon';

export interface RidCalendarCalendarDate {
  /** Fecha del calendario */
  datetime: DateTime;
  /** Numero del mes actual */
  month: number;
  /** Texto del mes actual */
  monthText: string;
  /** Numero del año actual */
  year: number;
  /** Cantidad de dias previos */
  monthPreDays: number;
  /** Cantidad de dias del mes */
  monthNumberOfDays: number;
  /** Cantidad de dias de resto para llegar a los 42 espacios */
  monthPostDays: number;
}

export interface RidCalendarMetadata {
  /** Textos de los dias de la semana */
  weekDaysText: string[];
  /** Textos de los meses del año */
  yearMonthsText: string[];
}

export interface RidCalendarData {
  /** Info del dia actual */
  today: DateTime;
  /** Info necesaria para trabajar con el calendario */
  meta: RidCalendarMetadata;
  /** Info del mes y año posicionado del calendario */
  calendarDate: RidCalendarCalendarDate;
}
