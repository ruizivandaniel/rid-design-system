import { DateTime } from 'luxon';

export interface RidRangeDate {
  from: Date | undefined;
  to: Date | undefined;
}

export interface RidValueRangeDateTimes {
  from: DateTime | undefined;
  to: DateTime | undefined;
}
