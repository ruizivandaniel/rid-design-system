export interface OnMonthChanged {
  /** numero del 1 al 12 del mes */
  number: number;
  /** texto del mes recortado */
  textShort: string;
  /** texto del mes completo */
  textLong: string;
}
