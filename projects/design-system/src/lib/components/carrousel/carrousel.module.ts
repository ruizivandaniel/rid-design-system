import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidCarrouselComponent } from './carrousel.component';
import { RidCarrouselSlideDirective } from './directives/carrousel-slide.directive';
import { RidSvgIconsModule } from './../svg-icons/svg-icons.module';
import { RidCarrouselSlideErrorDirective } from './directives/carrousel-slide-error.directive';
import { RidCarrouselSlideDefaultDirective } from './directives/carrousel-slide-default.directive';
import { RidResizeListenerDirective } from '@rid/core';

@NgModule({
  declarations: [
    RidCarrouselComponent,
    RidCarrouselSlideDirective,
    RidCarrouselSlideErrorDirective,
    RidCarrouselSlideDefaultDirective,
  ],
  imports: [
    CommonModule,
    RidSvgIconsModule,
    RidResizeListenerDirective,
  ],
  exports: [
    RidCarrouselComponent,
    RidCarrouselSlideDirective,
    RidCarrouselSlideErrorDirective,
    RidCarrouselSlideDefaultDirective,
  ],
})
export class RidCarrouselModule { }
