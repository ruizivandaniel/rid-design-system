import { Directive, TemplateRef, Input, OnInit } from '@angular/core';
import { v4 } from 'uuid';

@Directive({ selector: 'ridSlide, [ridSlide]' })
export class RidCarrouselSlideDirective implements OnInit {
  /** Identificador del slide. */
  @Input() id!: string;
  /** Data que se necesite pasar al ser seleccionado. */
  @Input() context!: any;

  get template(): TemplateRef<any> {
    return this._templateRef;
  }

  constructor(private _templateRef: TemplateRef<any>) { }

  ngOnInit(): void {
    if (!this.id) {
      this.id = v4();
    }
  }

}
