import { Directive } from '@angular/core';
import { RidCarrouselSlideDirective } from './carrousel-slide.directive';

@Directive({ selector: 'ridErrorSlide, [ridErrorSlide]' })
export class RidCarrouselSlideErrorDirective extends RidCarrouselSlideDirective { }
