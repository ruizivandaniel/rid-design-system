import { Directive } from '@angular/core';
import { RidCarrouselSlideDirective } from './carrousel-slide.directive';

@Directive({ selector: 'ridDefaultSlide, [ridDefaultSlide]' })
export class RidCarrouselSlideDefaultDirective extends RidCarrouselSlideDirective { }
