export interface RidSliceSelectedEvent {
  /** Identificador del slice. */
  id: string;
  /** Inidice del array de slices. */
  index: number;
  /** Contexto que se le haya agregado al tab. */
  context: any;
}
