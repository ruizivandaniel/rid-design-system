export interface RidSlideProps {
  /** True si se esta moviendo. */
  dragging: boolean;
  /** Posicion inicial del mouse. */
  initMousePos: number;
  /** Posicion del elemento. */
  scrollLeft: number;
}
