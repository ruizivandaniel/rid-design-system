import {
  Component, ContentChildren, ContentChild, TemplateRef, QueryList, ChangeDetectorRef,
  OnDestroy, ViewChild, Input, ElementRef, OnChanges, SimpleChanges, OnInit, Output,
  EventEmitter, AfterViewInit,
} from '@angular/core';
import { RidCarrouselSlideDirective } from './directives/carrousel-slide.directive';
import { Subject, interval, fromEvent, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { RidCarrouselSlideErrorDirective } from './directives/carrousel-slide-error.directive';
import { RidCarrouselSlideDefaultDirective } from './directives/carrousel-slide-default.directive';
import { RidSlideProps } from './models/slider-props.model';
import { RidSliceSelectedEvent } from './models/slice-selected.model';
import { RidResizeEvent, checkMobileTabletBrowser } from '@rid/core';

@Component({
  selector: 'rid-carrousel',
  templateUrl: 'carrousel.component.html',
  styleUrls: ['./carrousel.component.scss']
})
export class RidCarrouselComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit {

  /** Cantidad de slides que se mostraran al mismo tiempo */
  @Input() slidesPerView: number = 1;
  /** True si se desea ver la navegacion con chevrones */
  @Input() showNavigationsArrows: boolean = true;
  /** True si se desea ver la navegacion con los puntos debajo */
  @Input() showNavigationsSteps: boolean = false;
  /** True si se desea volver al inicio a llegar al limite de slides */
  @Input() cyclic: boolean = false;
  /** True si se desea mostrar el slide de error (ridErrorSlide) */
  @Input() showError: boolean = false;
  /** True si se desea deslizar los slides del carrousel de manera automatica */
  @Input() autoplay: boolean = false;
  /** Tiempo en milisegundos en que se desliza automaticamente */
  @Input() autoplayDelay: number = 3000;
  /** True si se desea frenar el autoplay cuando se hace mouse hover */
  @Input() stopOnHover: boolean = true;
  /** True si se desea poder scrollear los slides con el mouse. */
  @Input() draggable: boolean = true;
  /** True si se desea que se autoscrollee en caso de hacer click en el slice. */
  @Input() scrollOnSelect: boolean = true;
  /** Informa que Slide se selecciono. */
  @Output() sliceChange = new EventEmitter<RidSliceSelectedEvent>();

  @ContentChildren(TemplateRef, { read: RidCarrouselSlideDirective, descendants: false })
  private _slides!: QueryList<RidCarrouselSlideDirective>;
  @ContentChild(TemplateRef, { read: RidCarrouselSlideErrorDirective, descendants: false, static: true })
  errorSlide!: RidCarrouselSlideErrorDirective;
  @ContentChild(TemplateRef, { read: RidCarrouselSlideDefaultDirective, descendants: false, static: true })
  defaultSlide!: RidCarrouselSlideDefaultDirective;
  @ViewChild('carrouselContainer', { static: true })
  private _carrouselContainer!: ElementRef<any>;
  @ViewChild('slidesContainer', { static: true })
  private _slidesContainer!: ElementRef<any>;

  checkMinHeight = (_?: RidResizeEvent): void => {
    this._containerMinHeight = this._slidesContainer ?
      this._slidesContainer.nativeElement.offsetHeight
      : 0;
    this._cdr.detectChanges();
  }

  private _containerMinHeight = 0;
  private _currentIndex: number = 0;
  private _unsubscriber = new Subject<void>();
  private _autopalyUnsubscriber!: Subject<void>;
  private _onHover: boolean = false;
  private _slideProps: RidSlideProps = {
    dragging: false,
    initMousePos: 0,
    scrollLeft: 0,
  };
  private _mouseDownSubscription!: Subscription;
  private _onMouseMoveSubscription!: Subscription;
  private _onMouseUpSubscription!: Subscription;

  get containerMinHeight(): number {
    return this._containerMinHeight;
  }

  get currentSlides(): RidCarrouselSlideDirective[] {
    return this._slides ? this._slides.toArray() : [];
  };

  get currentIndex(): number {
    return this._currentIndex;
  }

  get slideWidth(): number {
    return this._carrouselContainer ?
      this._carrouselContainer.nativeElement.offsetWidth / this.slidesPerView
      : 0;
  }

  get showRightNavigationArrow(): boolean {
    return this.showNavigationsArrows
      && (this._hasNext() || this.cyclic)
      && this._slides.length > 0
      && !this.showError;
  }

  get showLeftNavigationArrow(): boolean {
    return this.showNavigationsArrows
      && (this._hasPrev() || this.cyclic)
      && this._slides.length > 0
      && !this.showError;
  }

  get totalSlides(): number {
    return this._slides ? this._slides.toArray().length : 0;
  }

  constructor(private _cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    if (this.draggable) {
      this._setDraggableEvents();
    }
  }

  ngAfterViewInit(): void {
    if (this.autoplay) {
      this._configAutoplay();
    }
    this.checkMinHeight();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['cyclic']
      && !changes['cyclic'].isFirstChange()
      && changes['cyclic'].previousValue !== changes['cyclic'].currentValue
      && changes['cyclic'].currentValue === true) {
      if (this.autoplay) {
        this._configAutoplay();
      }
    }
    if (changes['showError']
      && !changes['showError'].isFirstChange()
      && changes['showError'].previousValue !== changes['showError'].currentValue) {
      if (this.autoplay) {
        this.showError ? this._unsubscribeAutoplay() : this._configAutoplay();
      }
    }
    if (changes['draggable'] && !changes['draggable'].isFirstChange()) {
      if (changes['draggable'].currentValue) {
        this._setDraggableEvents();
      } else {
        this._unsetDraggableEvents();
      }
    }
    if (changes['autoplay']
      && !changes['autoplay'].isFirstChange()
      && changes['autoplay'].previousValue !== changes['autoplay'].currentValue) {
      this.autoplay ? this._configAutoplay() : this._unsubscribeAutoplay();
    }
    if (changes['autoplayDelay']
      && !changes['autoplayDelay'].isFirstChange()
      && changes['autoplayDelay'].previousValue !== changes['autoplayDelay'].currentValue) {
      if (this.autoplay) {
        this._configAutoplay();
      }
    }
  }

  ngOnDestroy(): void {
    this._unsubscriber.next();
    this._unsubscriber.complete();
    this._unsubscribeAutoplay();
  }

  goToIndex(index: number): void {
    if (this._exists(index)) {
      let scrollTo = this._currentIndex * this.slideWidth;
      this._currentIndex = index;
      if (this.currentSlides.length - index >= this.slidesPerView) {
        scrollTo = index * this.slideWidth;
      } else if (this.currentSlides.length - this.slidesPerView > 0) {
        scrollTo = (this.currentSlides.length - this.slidesPerView) * this.slideWidth;
      }
      this._slidesContainer.nativeElement.scroll(scrollTo, 0);
    }
  }

  goToId(id: string): void {
    const findedIndex = this.currentSlides.findIndex((slide: RidCarrouselSlideDirective) => {
      slide.id === id;
    });
    if (findedIndex !== -1) {
      this.goToIndex(findedIndex);
    }
  }

  prev(): void {
    if (this._hasPrev()) {
      this.goToIndex(this._currentIndex - 1);
      this._informSelected();
    } else if (this.cyclic) {
      this.goToIndex(this.currentSlides.length - 1);
      this._informSelected();
    }
  }

  next(): void {
    if (this._hasNext()) {
      this.goToIndex(this._currentIndex + 1);
    } else if (this.cyclic) {
      this.goToIndex(0);
    }
  }

  private _informSelected(): void {
    const slide = this.currentSlides[this._currentIndex];
    this.sliceChange.emit({
      context: slide.context,
      id: slide.id,
      index: this._currentIndex,
    });
  }

  private _setDraggableEvents(): void {
    if (this._mouseDownSubscription) {
      this._mouseDownSubscription.unsubscribe();
    }
    this._mouseDownSubscription =
      fromEvent(this._carrouselContainer.nativeElement, 'mousedown')
        // fromEvent(this._carrouselContainer.nativeElement, checkMobileTabletBrowser() ? 'touchstart' : 'mousedown')
        .pipe(takeUntil(this._unsubscriber))
        .subscribe(this._onMouseDownSlideHandler);
  }

  private _unsetDraggableEvents(): void {
    if (this._mouseDownSubscription) {
      this._mouseDownSubscription.unsubscribe();
    }
    if (this._onMouseMoveSubscription) {
      this._onMouseMoveSubscription.unsubscribe();
    }
    if (this._onMouseUpSubscription) {
      this._onMouseUpSubscription.unsubscribe();
    }
  }

  private _configAutoplay(): void {
    if (this._autopalyUnsubscriber) {
      this._unsubscribeAutoplay();
    }
    this._autopalyUnsubscriber = new Subject<void>();
    if (this.stopOnHover) {
      fromEvent(this._carrouselContainer.nativeElement, 'mouseenter')
        .pipe(takeUntil(this._autopalyUnsubscriber))
        .subscribe(() => {
          this._onHover = true;
        });
      fromEvent(this._carrouselContainer.nativeElement, 'mouseleave')
        .pipe(takeUntil(this._autopalyUnsubscriber))
        .subscribe(() => {
          this._onHover = false;
        });
    }
    interval(this.autoplayDelay)
      .pipe(takeUntil(this._autopalyUnsubscriber))
      .subscribe(() => {
        if ((this.stopOnHover && this._onHover) || (!this._slides.length)) {
          return;
        }
        this.next();
        if (!this.cyclic && !this._hasNext()) {
          this._unsubscribeAutoplay();
        }
      });
  }

  private _unsubscribeAutoplay(): void {
    this._autopalyUnsubscriber.next();
    this._autopalyUnsubscriber.complete();
  }

  private _hasPrev(): boolean {
    const prev = this.currentSlides[this._currentIndex - 1];
    return prev !== undefined && prev !== null;
  }

  private _hasNext(): boolean {
    const next = this.currentSlides[this._currentIndex + 1];
    return next !== undefined && next !== null;
  }

  private _exists(index: number): boolean {
    const next = this.currentSlides[index];
    return next !== undefined && next !== null;
  }

  private _onMouseDownSlideHandler = (e: any): void => {
    this._slideProps = {
      dragging: true,
      initMousePos: e.x,
      scrollLeft: this._slidesContainer.nativeElement.scrollLeft,
    };
    const containerElement = this._carrouselContainer.nativeElement;
    this._onMouseMoveSubscription =
      fromEvent(containerElement, 'mousemove')
        // fromEvent(containerElement, checkMobileTabletBrowser() ? 'touchmove' : 'mousemove')
        .pipe(takeUntil(this._unsubscriber))
        .subscribe(this._onMouseMoveSlideHandler);
    this._onMouseUpSubscription =
      fromEvent(containerElement, 'mouseup')
        // fromEvent(containerElement, checkMobileTabletBrowser() ? 'touchend' : 'mouseup')
        .pipe(takeUntil(this._unsubscriber))
        .subscribe(this._onMouseUpSlideHandler);
  }

  private _onMouseMoveSlideHandler = (e: any): void => {
    if (this._slideProps.dragging) {
      const dx = e.clientX - this._slideProps.initMousePos;
      const scrollLeftTo = this._slideProps.scrollLeft - dx;
      // TODO: No funciona en browser mobile. Chequear cada una
      // TODO: de las props de aca arriba
      this._slidesContainer.nativeElement.scroll(scrollLeftTo, 0);
    }
  }

  private _onMouseUpSlideHandler = (e: any): void => {
    this._slideProps = {
      dragging: false,
      initMousePos: 0,
      scrollLeft: 0,
    };
    if (this._onMouseMoveSubscription) {
      this._onMouseMoveSubscription.unsubscribe();
    }
    if (this._onMouseUpSubscription) {
      this._onMouseUpSubscription.unsubscribe();
    }
  }

}
