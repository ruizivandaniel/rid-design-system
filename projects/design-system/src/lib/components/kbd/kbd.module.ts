import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidKbdComponent } from './kbd.component';
import { RidSvgIconsModule } from './../svg-icons/svg-icons.module';

@NgModule({
  declarations: [
    RidKbdComponent,
  ],
  imports: [
    CommonModule,
    RidSvgIconsModule,
  ],
  exports: [
    RidKbdComponent,
  ],
})
export class RidKbdModule { }
