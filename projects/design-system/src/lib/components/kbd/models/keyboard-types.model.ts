export enum RidKbdKey {
  COMMAND = "command",      // ⌘
  SHIFT = "shift",          // ⇧
  CTRL = "ctrl",            // ⌃
  OPTION = "option",        // ⌥
  ENTER = "enter",          // ↵
  DELETE = "delete",        // ⌫
  ESCAPE = "escape",        // ⎋
  TAB = "tab",              // ⇥
  CAPSLOCK = "capslock",    // ⇪
  UP = "up",                // ↑
  RIGHT = "right",          // →
  DOWN = "down",            // ↓
  LEFT = "left",            // ←
  PAGEUP = "pageup",        // ⇞
  PAGEDOWN = "pagedown",    // ⇟
  HOME = "home",            // ↖
  END = "end",              // ↘
  HELP = "help",            // ?
  SPACE = "space",          // ␣
};
