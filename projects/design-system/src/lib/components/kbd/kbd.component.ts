import { Component, Input } from '@angular/core';
import { RidKbdKey } from './models/keyboard-types.model';

@Component({
  selector: 'rid-kbd',
  templateUrl: 'kbd.component.html',
})
export class RidKbdComponent {
  /** Listado de teclas especiales. */
  @Input() keys: RidKbdKey[] = [];
  /** True si se muestra el modo dark. */
  @Input() darkMode: boolean = false;

  getKeyUrl(key: RidKbdKey): string {
    switch (key) {
      case RidKbdKey.COMMAND: return 'assets/command.svg';
      case RidKbdKey.SHIFT: return 'assets/shift.svg';
      case RidKbdKey.CTRL: return 'assets/arrow-up-left-circle.svg';
      case RidKbdKey.OPTION: return 'assets/option.svg';
      case RidKbdKey.ENTER: return 'assets/arrow-return-left.svg';
      case RidKbdKey.DELETE: return 'assets/backspace.svg';
      case RidKbdKey.ESCAPE: return 'assets/escape.svg';
      case RidKbdKey.TAB: return 'assets/indent.svg';
      case RidKbdKey.CAPSLOCK: return 'assets/capslock.svg';
      case RidKbdKey.UP: return 'assets/arrow-up.svg';
      case RidKbdKey.RIGHT: return 'assets/arrow-right.svg';
      case RidKbdKey.DOWN: return 'assets/arrow-down.svg';
      case RidKbdKey.LEFT: return 'assets/arrow-left.svg';
      case RidKbdKey.PAGEUP: return 'assets/chevron-bar-up.svg';
      case RidKbdKey.PAGEDOWN: return 'assets/chevron-bar-down.svg';
      case RidKbdKey.HOME: return 'assets/house.svg';
      case RidKbdKey.END: return 'assets/arrow-down-right-circle.svg';
      case RidKbdKey.HELP: return 'assets/question-circle.svg';
      case RidKbdKey.SPACE: return 'assets/.svg';
      default: return '';
    }
  }

}
