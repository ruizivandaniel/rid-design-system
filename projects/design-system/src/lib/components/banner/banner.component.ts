import { Component, Input } from '@angular/core';

@Component({
  selector: 'rid-banner',
  templateUrl: 'banner.component.html',
})
export class RidBannerComponent {
  /** Texto principal del banner */
  @Input() message: string = '';
  /** Texto secundario del banner */
  @Input() description: string = '';
  /** Imagen del banner */
  @Input() imageUrl: string = '';
  /** Forzar el width de la imagen. */
  @Input() forceImageWidth: number = 120;
  /** Forzar el height de la imagen. */
  @Input() forceImageHeight: number = 120;
}
