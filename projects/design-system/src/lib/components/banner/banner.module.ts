import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidBannerComponent } from './banner.component';
import { RidSvgIconsModule } from './../svg-icons/svg-icons.module';

@NgModule({
  declarations: [RidBannerComponent],
  imports: [
    CommonModule,
    RidSvgIconsModule,
  ],
  exports: [RidBannerComponent],
})
export class RidBannerModule { }
