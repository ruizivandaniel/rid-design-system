import {
  Component, Input, Output, EventEmitter, OnChanges, SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'rid-pagination',
  templateUrl: 'pagination.component.html',
})
export class RidPaginationComponent implements OnChanges {
  /** Limite de paginas a mostrar. */
  @Input() totalPages: number = 1;
  /** Deshabilitar input para editar pagina actual. */
  @Input() disablePageInput: boolean = false;
  /** Emite cuando cambia la pagina. */
  @Output() pageChanged = new EventEmitter<number>();

  currentPage: number = 1;
  private _previousPage: number = 1;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['totalPages'] && !changes['totalPages'].isFirstChange() &&
      changes['totalPages'].currentValue !== changes['totalPages'].previousValue) {
      this.goToPage(1);
    }
  }

  prev(): void {
    const auxPrev = this.currentPage - 1;
    if (this._hasPrev(auxPrev)) {
      this.goToPage(auxPrev);
    }
  }

  next(): void {
    const auxNext = this.currentPage + 1;
    if (this._hasNext(auxNext)) {
      this.goToPage(auxNext);
    }
  }

  goToPage(pageNumber: number): void {
    this.currentPage = pageNumber;
    this._previousPage = pageNumber;
    this.pageChanged.emit(this.currentPage);
  }

  showPrevBtn(): boolean {
    return this._hasPrev(this.currentPage - 1);
  }

  showNextBtn(): boolean {
    return this._hasNext(this.currentPage + 1);
  }

  checkValue(): void {
    if (!this._hasPrev(this.currentPage) || !this._hasNext(this.currentPage)) {
      this.goToPage(this._previousPage);
    } else if (this._previousPage !== this.currentPage) {
      this.goToPage(this.currentPage);
    }
  }

  private _hasPrev(pageNumber: number): boolean {
    return pageNumber > 0;
  }

  private _hasNext(pageNumber: number): boolean {
    return pageNumber <= this.totalPages;
  }

}
