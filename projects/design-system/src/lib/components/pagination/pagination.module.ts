import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidPaginationComponent } from './pagination.component';
import { RidButtonModule } from '../button/button.module';
import { RidSvgIconsModule } from '../svg-icons/svg-icons.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [RidPaginationComponent],
  imports: [
    CommonModule,
    RidButtonModule,
    RidSvgIconsModule,
    FormsModule,
  ],
  exports: [RidPaginationComponent],
})
export class RidPaginationModule { }
