import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { RidSvgIconComponent } from './svg-icons.component';

@NgModule({
  declarations: [RidSvgIconComponent],
  imports: [AngularSvgIconModule.forRoot(), HttpClientModule],
  exports: [RidSvgIconComponent, AngularSvgIconModule],
})
export class RidSvgIconsModule { }
