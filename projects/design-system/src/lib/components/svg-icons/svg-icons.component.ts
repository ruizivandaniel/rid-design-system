import { Component, Input } from '@angular/core';

@Component({
  selector: 'rid-svg',
  templateUrl: 'svg-icons.component.html',
})
export class RidSvgIconComponent {
  /** Url del asset */
  @Input() url: string = 'assets/patch-question-fill.svg';
  /** Ancho del asset en px */
  @Input() width: number = 16;
  /** Alto del asset en px */
  @Input() height: number = 16;
  /** Clases para el componente */
  @Input() svgClass: string = '';
}
