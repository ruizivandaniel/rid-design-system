import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidLabelComponent } from './label.component';

@NgModule({
  declarations: [
    RidLabelComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    RidLabelComponent,
  ],
})
export class RidLabelModule { }
