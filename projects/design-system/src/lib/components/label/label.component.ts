import { Component, Input } from '@angular/core';

@Component({
  selector: 'rid-label',
  templateUrl: 'label.component.html',
})
export class RidLabelComponent {
  /** Texto que mostrará el label. */
  @Input() text: string = '';
  /** Texto secundario que mostrará el label. */
  @Input() subText: string = '';
  /** Forzado de clases para el label. */
  @Input() forceClass: string = '';
  /** Deshabilitar el label. */
  @Input() isDisabled: boolean = false;
}
