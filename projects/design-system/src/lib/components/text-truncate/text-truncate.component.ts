import { Component, Input } from '@angular/core';

/**
 * @description Componente encargado de recortar el largo del texto en base al limite
 * pasado por parametro (20 por defecto). Si el texto es recortado, se
 * atacha al componente un tooltip con el texto original, que se mostrara
 * si el usuario posa el mouse sobre el componente.
 * @param text Texto a recortar.
 * @param limit Limite de caracteres hasta realizar el corte.
 * @param cancelTruncate True si se desea denegar el recorte.
 * @param disableTooltip True si se desea deshabilitar el tooltip.
 * @param tooltipMaxWidth Largo maximo del tooltip.
 * @param reverse True si se desea recortar de izquierda a derecha.
 *
 * @usageNotes
 * ```html
 * <!-- Por defecto recorta en 20: -->
 * <div [ridTruncate]="'Este es mi texto a recortar"></div>
 * <!-- Forzar limite: -->
 * <div [ridTruncate]="'Este es mi texto a recortar" [limit]="30"></div>
 * <!-- Cancelar el recorte: -->
 * <div [ridTruncate]="'Este es mi texto a recortar" [cancelTruncate]="mustCancel"></div>
 * <!-- Recortar al revez: -->
 * <div [ridTruncate]="'Este es mi texto a recortar" [reverse]="true"></div>
 * <!-- Recortar sin mostrar el tooltip: -->
 * <div [ridTruncate]="'Este es mi texto a recortar" [disableTooltip]="true"></div>
 * ```
 */
@Component({
  selector: 'ridTruncate, [ridTruncate]',
  exportAs: 'ridTruncate',

  templateUrl: 'text-truncate.component.html',
})
export class RidTextTruncateComponent {
  /** Texto a recortar. */
  @Input('ridTruncate') text!: string;
  /** Limite de caracteres hasta realizar el corte. */
  @Input() limit: number = 20;
  /** True si se desea denegar el recorte. */
  @Input() cancelTruncate: boolean = false;
  /** True si se desea deshabilitar el tooltip. */
  @Input() disableTooltip: boolean = false;
  /** Largo maximo del tooltip. */
  @Input() tooltipMaxWidth: number = 350;
  /** True si se desea recortar de izquierda a derecha. */
  @Input() reverse: boolean = false;

  get truncated(): string {
    if (!this.cancelTruncate && this.text.length > this.limit) {
      if (this.reverse) {
        const from = this.text.length - this.limit;
        const to = this.text.length;
        return `...${this.text.substring(from, to)}`;
      } else {
        return `${this.text.substring(0, this.limit)}...`;
      }
    }
    return '';
  }

}
