import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidTooltipModule } from '../../directives/tooltip/tooltip.module';
import { RidTextTruncateComponent } from './text-truncate.component';

@NgModule({
  declarations: [
    RidTextTruncateComponent,
  ],
  imports: [
    CommonModule,
    RidTooltipModule,
  ],
  exports: [
    RidTextTruncateComponent,
  ],
})
export class RidTextTruncateModule { }
