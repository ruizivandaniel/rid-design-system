import { Directive, TemplateRef } from '@angular/core';

@Directive({ selector: '[ridCardBody]' })
export class RidCardBodyDirective {

  get template(): TemplateRef<any> {
    return this._template;
  }

  constructor(private _template: TemplateRef<any>) { }

}
