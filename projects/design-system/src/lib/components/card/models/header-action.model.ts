export interface RidCardHeaderAction {
  /** texto de la accion */
  label: string;
  /** icono de la accion */
  icon: string;
  /** funcion de la accion */
  action: () => void;
}
