import { RidButtonType } from './../../button/models/button-type.models';

export interface RidCardFooterAction {
  /** texto del boton */
  label: string;
  /** icono del boton */
  icon?: string;
  /** tipo del boton */
  type?: RidButtonType;
  /** funcion del boton */
  action: () => void;
  /** True si se desea desabilitar el boton. */
  isDisabled?: boolean;
  /** True si se desea mostrar el spinner en el boton. */
  isLoading?: boolean;
}
