import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidCardComponent } from './card.component';
import { RidCardBodyDirective } from './directives/card-body.directive';
import { RidSvgIconsModule } from './../svg-icons/svg-icons.module';
import { RidButtonModule } from './../button/button.module';
import { RidDropdownModule } from '../dropdown/dropdown.module';

@NgModule({
  declarations: [
    RidCardComponent,
    RidCardBodyDirective,
  ],
  imports: [
    CommonModule,
    RidSvgIconsModule,
    RidButtonModule,
    RidDropdownModule,
  ],
  exports: [
    RidCardComponent,
    RidCardBodyDirective,
  ],
})
export class RidCardModule { }
