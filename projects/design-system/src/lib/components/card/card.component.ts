import { Component, Input, ContentChild, TemplateRef, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { RidCardBodyDirective } from './directives/card-body.directive';
import { RidCardHeaderAction } from './models/header-action.model';
import { RidCardFooterAction } from './models/footer-action.model';
import { RidResponsiveService } from '@rid/core';
import { RidDropdownItem } from '../dropdown/models/dropdown-item.model';

@Component({
  selector: 'rid-card',
  templateUrl: 'card.component.html',
})
export class RidCardComponent implements OnInit, OnChanges {
  /** Titulo de la card */
  @Input() title: string = '';
  /** Icono del titulo de la card */
  @Input() titleIcon: string = '';
  /** Subtitulo de la card */
  @Input() subTitle: string = '';
  /** Conjunto de acciones del header */
  @Input() headerActions: RidCardHeaderAction[] = [];
  /** Conjunto de acciones del footer */
  @Input() footerActions: RidCardFooterAction[] = [];

  @ContentChild(TemplateRef, { read: RidCardBodyDirective, descendants: false })
  private _cardBodyDirective!: RidCardBodyDirective;

  headerActionsResponsive: RidDropdownItem[] = [];

  get bodyTemplate(): TemplateRef<any> | undefined {
    return this._cardBodyDirective?.template ?? undefined;
  }

  get isResponsive(): boolean {
    return this._responsiveService.is(['MOBILE', 'NOOTEBOOK']);
  }

  constructor(private _responsiveService: RidResponsiveService) { }

  ngOnInit(): void {
    this._setHeaderActionsResponsive();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['headerActions']
      && !changes['headerActions'].isFirstChange()
      && changes['headerActions'].currentValue !== changes['headerActions'].previousValue) {
      this._setHeaderActionsResponsive();
    }
  }

  doAction(action: () => void): void {
    if (typeof action === 'function') {
      action();
    }
  }

  private _setHeaderActionsResponsive(): void {
    this.headerActionsResponsive = this.headerActions.map((h: RidCardHeaderAction) => {
      return {
        action: h.action,
        label: h.label,
        iconUrl: h.icon,
        type: 'primary',
      } as RidDropdownItem;
    });
  }
}
