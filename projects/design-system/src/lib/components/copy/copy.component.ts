import { Component, Input, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { Subject, Subscription, from, takeUntil, timer } from 'rxjs';
import { RidCopyStatus } from './models/copy-status.model';
import { RidTooltipDirective } from '../../directives/tooltip/tooltip.directive';

@Component({
  selector: 'ridCopy, [ridCopy]',
  exportAs: 'ridCopy',
  templateUrl: 'copy.component.html',
})
export class RidCopyComponent {

  /** Texto a copiar. */
  @Input('ridCopy') content: string = '';
  /** Template para mostrar en el tooltip. */
  @Input() forceTemplate!: TemplateRef<any>;
  /**
   * Evento que emite cuando se copia el elemento.
   * Devolverá false en caso de fallar.
   */
  @Output() copied = new EventEmitter<boolean>();

  @ViewChild(RidTooltipDirective)
  private _tooltipInstance!: RidTooltipDirective;

  private _status: RidCopyStatus = 'waiting';
  private _unsubscriber = new Subject<void>();
  private _timerSub!: Subscription;

  get isCoping(): boolean {
    return this._status === 'coping';
  }

  get isCopied(): boolean {
    return this._status === 'copied';
  }

  doCopy(): void {
    this._status = 'coping';
    from(navigator.clipboard.writeText(this.content))
      .pipe(takeUntil(this._unsubscriber))
      .subscribe({
        next: () => {
          this._status = 'copied';
          this._showTooltip();
          this.copied.emit(true);
          if (this._timerSub) {
            this._timerSub.unsubscribe();
          }
          this._timerSub = timer(2000)
            .pipe(takeUntil(this._unsubscriber))
            .subscribe(() => {
              this._timerSub.unsubscribe();
              this._status = 'waiting';
              this._unsubscriber.next();
            });
        },
        error: (_: any) => {
          this._status = 'error';
          this.copied.emit(false);
          this._unsubscriber.next();
        },
      });
  }

  private _showTooltip(): void {
    if (this.forceTemplate) {
      this._tooltipInstance.template = this.forceTemplate;
    }
    this._tooltipInstance.open();
    this._tooltipInstance.close(2000);
  }

}
