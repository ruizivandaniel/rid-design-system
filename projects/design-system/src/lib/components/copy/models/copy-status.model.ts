/** Estado actual del copiado de texto. */
export type RidCopyStatus = 'coping' | 'waiting' | 'copied' | 'error';
