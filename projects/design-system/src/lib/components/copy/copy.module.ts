import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidCopyComponent } from './copy.component';
import { RidSvgIconsModule } from '../svg-icons/svg-icons.module';
import { RidTooltipModule } from '../../directives/tooltip/tooltip.module';

@NgModule({
  declarations: [
    RidCopyComponent,
  ],
  imports: [
    CommonModule,
    RidSvgIconsModule,
    RidTooltipModule,
  ],
  exports: [
    RidCopyComponent,
  ],
})
export class RidCopyModule { }
