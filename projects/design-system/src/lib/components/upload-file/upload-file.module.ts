import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidUploadFileComponent } from './upload-file.component';
import { RidButtonModule } from '../button/button.module';
import { RidSvgIconsModule } from '../svg-icons/svg-icons.module';
import { RidDragDropDirective } from '@rid/core';

@NgModule({
  declarations: [
    RidUploadFileComponent,
  ],
  imports: [
    CommonModule,
    RidButtonModule,
    RidSvgIconsModule,
    RidDragDropDirective,
  ],
  exports: [
    RidUploadFileComponent,
  ],
})
export class RidUploadFileModule { }
