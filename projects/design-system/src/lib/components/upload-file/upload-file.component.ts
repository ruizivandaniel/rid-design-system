import { Component, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'rid-upload-file',
  templateUrl: 'upload-file.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useClass: forwardRef(() => RidUploadFileComponent),
      multi: true,
    }
  ]
})
export class RidUploadFileComponent implements ControlValueAccessor {

  /** Texto a mostrar en el selector de archivos. */
  @Input() label: string = 'Haz click o arrastra para subir un archivo';
  /** True si se desea seleccionar mas de un archivo. */
  @Input() isMultiple: boolean = true;
  /** Evento que se emite cuando se selecciona un archivo o se limpian los valores. */
  @Output() uploaded = new EventEmitter<File[]>();

  selectedFilesLabel = '';
  draggingHover: boolean = false;

  get disabled(): boolean {
    return this._disabled;
  }

  private _disabled = false;
  private _onTouchFn!: any;
  private _onChangeFn!: any;

  setFile(fileList: FileList | null): void {
    if (!fileList || !fileList.length) {
      this.cleanFiles();
      return;
    }
    this.draggingHover = false;
    const files: File[] = Array.from(fileList || []);
    this.selectedFilesLabel = files.map((f: File) => f.name).join(' | ');
    this.uploaded.emit(files);
    // FIXME: Ver por que las funciones quedan como undefined...
    this._onChangeFn && this._onChangeFn(files);
  }

  cleanFiles(): void {
    this.selectedFilesLabel = '';
    this.uploaded.emit([]);
    this._onChangeFn && this._onChangeFn([]);
  }

  onBlur(): void {
    this._onTouchFn && this._onTouchFn();
  }

  /** Implementacion del control value accesor */
  writeValue(fileList: FileList): void {
    this.setFile(fileList);
  }

  registerOnChange(fn: any): void {
    this._onChangeFn = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouchFn = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this._disabled = isDisabled;
    if (this.disabled) {
      this.selectedFilesLabel = '';
      this.uploaded.emit([]);
    }
  }

}
