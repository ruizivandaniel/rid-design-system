import {
  Component, Input, ContentChild, AfterContentInit, Renderer2, ElementRef, OnChanges, SimpleChanges
} from '@angular/core';

@Component({
  selector: 'rid-input',
  templateUrl: 'input.component.html',
})
export class RidInputComponent implements AfterContentInit, OnChanges {
  /** Texto que se mostrara sobre el input. */
  @Input() label: string = '';
  /** Texto que se mostrara debajo el input. */
  @Input() description: string = '';
  /** Texto que se mostrara debajo el input. Modificará el input a estado error. */
  @Input() errorMsg: string = '';
  /** Url del icono que se mostrara a la izquierda del input. */
  @Input() leftIconUrl: string = '';
  /** Accion del icono que se mostrara a la izquierda del input. */
  @Input() leftIconAction!: () => void;
  /** Url del icono que se mostrara a la derecha del input. */
  @Input() rightIconUrl: string = '';
  /** Accion del icono que se mostrara a la derecha del input. */
  @Input() rightIconAction!: () => void;
  /**
   * True si se desea mostrar un contador del length del input.
   * Es necesario que el input tenga seteado su maxLength.
   */
  @Input() showLengthCounter: boolean = false;

  @ContentChild('ridInput', {}) private _input!: any;

  get inputLengthStatus(): string {
    if (!this._input || !this._input.nativeElement) {
      return '';
    }
    const input = this._input.nativeElement;
    return `${input.value.length} / ${input.maxLength}`;
  }

  get inputHeight(): number {
    if (!this._input || !this._input.nativeElement) {
      return 0;
    }
    return this._input.nativeElement.offsetHeight;
  }

  constructor(private _renderer: Renderer2) { }

  ngAfterContentInit(): void {
    this._setInputStyles();
    this._setPaddings();
    this._checkForErrors();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['leftIconUrl']
      && !changes['leftIconUrl'].isFirstChange()
      && changes['leftIconUrl'].currentValue !== changes['leftIconUrl'].previousValue) {
      this._setPaddings();
    }
    if (changes['rightIconUrl']
      && !changes['rightIconUrl'].isFirstChange()
      && changes['rightIconUrl'].currentValue !== changes['rightIconUrl'].previousValue) {
      this._setPaddings();
    }
    if (changes['errorMsg']
      && !changes['errorMsg'].isFirstChange()
      && changes['errorMsg'].currentValue !== changes['errorMsg'].previousValue) {
      this._checkForErrors();
    }
  }

  private _setInputStyles(): void {
    this._renderer.addClass(this._input.nativeElement, 'form-control');
    this._renderer.addClass(this._input.nativeElement, 'form-control-sm');
    this._renderer.addClass(this._input.nativeElement, 'rounded-3');
    this._renderer.addClass(this._input.nativeElement, 'shadow-sm');
  }

  private _checkForErrors(): void {
    this.errorMsg ?
      this._renderer.addClass(this._input.nativeElement, 'border-danger') :
      this._renderer.removeClass(this._input.nativeElement, 'border-danger');
  }

  private _setPaddings(): void {
    if (this.leftIconUrl) {
      this._renderer.addClass(this._input.nativeElement, 'ps-4');
    }
    if (this.rightIconUrl) {
      this._renderer.addClass(this._input.nativeElement, 'pe-4');
    }
  }
}
