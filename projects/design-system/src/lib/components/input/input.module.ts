import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidInputComponent } from './input.component';
import { RidSvgIconsModule } from '../svg-icons/svg-icons.module';

@NgModule({
  declarations: [
    RidInputComponent,
  ],
  imports: [
    CommonModule,
    RidSvgIconsModule,
  ],
  exports: [
    RidInputComponent,
  ],
})
export class RidInputModule { }
