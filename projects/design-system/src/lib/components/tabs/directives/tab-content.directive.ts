import { Directive, Input, TemplateRef } from '@angular/core';
import { RidTabItemBadge } from '../models/tab-badge.model';

@Directive({ selector: 'ridTab, [ridTab]' })
export class RidTabDirective {

  /** Identificador del tab. */
  @Input() id: string = '';
  /** Label que mostrará el tabs component. */
  @Input('ridTab') label: string = '';
  /** Badges que mostrará sobre el tab en el tabs component. */
  @Input() badges: RidTabItemBadge[] = [];

  get template(): TemplateRef<any> {
    return this._template;
  }

  constructor(private _template: TemplateRef<any>) { }

  ngOnInit(): void {
    if (!this.id) {
      this.id = this.label;
    }
  }

}
