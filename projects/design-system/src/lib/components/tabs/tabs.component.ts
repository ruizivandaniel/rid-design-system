import { Component, ContentChildren, QueryList, AfterContentInit, TemplateRef } from '@angular/core';
import { RidTabDirective } from './directives/tab-content.directive';

@Component({
  selector: 'rid-tabs',
  templateUrl: 'tabs.component.html',
})
export class RidTabsComponent implements AfterContentInit {

  @ContentChildren(TemplateRef, { read: RidTabDirective, descendants: false, emitDistinctChangesOnly: true })
  private _items!: QueryList<RidTabDirective>;

  private _activeIndex: number = 0;

  get items(): RidTabDirective[] {
    return this._items ? this._items.toArray() : [];
  }

  get activeIndex(): number {
    return this._activeIndex;
  }

  get currentTemp(): TemplateRef<any> {
    return this.items[this.activeIndex].template;
  }

  ngAfterContentInit(): void {
    if (this.items.length) {
      this.clear();
    }
  }

  selectTab(index: number): void {
    if (this.items[index]) {
      this._activeIndex = index;
    } else {
      throw Error(`No se encontró index para el index ${index}`);
    }
  }

  selectTabByLabel(label: string): void {
    const index = this.items.findIndex(i => i.label === label);
    if (index !== -1) {
      this._activeIndex = index;
    } else {
      throw Error(`No se encontró index para el label ${label}`);
    }
  }

  selectTabById(id: string): void {
    const index = this.items.findIndex(i => i.id === id);
    if (index !== -1) {
      this._activeIndex = index;
    } else {
      throw Error(`No se encontró index para el id ${id}`);
    }
  }

  clear(): void {
    this.selectTab(0);
  }

}
