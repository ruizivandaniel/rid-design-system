import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidTabDirective } from './directives/tab-content.directive';
import { RidTabsComponent } from './tabs.component';

@NgModule({
  declarations: [
    RidTabsComponent,
    RidTabDirective,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    RidTabsComponent,
    RidTabDirective,
  ],
})
export class RidTabsModule { }
