import { AfterContentInit, ContentChildren, Directive, QueryList } from '@angular/core';
import { RidButtonComponent } from '../button.component';

@Directive({
  selector: 'ridButtonGroup, [ridButtonGroup]',
  exportAs: 'ridButtonGroup',
})
export class RidButtonGroupDirective implements AfterContentInit {
  @ContentChildren(RidButtonComponent, { emitDistinctChangesOnly: true, read: RidButtonComponent })
  private _buttons!: QueryList<RidButtonComponent>;

  get buttons(): RidButtonComponent[] {
    return this._buttons ? this._buttons.toArray() : [];
  }

  ngAfterContentInit(): void {
    this.buttons.forEach((button: RidButtonComponent, index: number) => {
      if (index === 0) {
        button.forceClass += ' rid-btn-group-first';
      } else if (index === this.buttons.length - 1) {
        button.forceClass += ' rid-btn-group-last';
      } else {
        button.forceClass += ' rid-btn-group';
      }
    });

  }

}
