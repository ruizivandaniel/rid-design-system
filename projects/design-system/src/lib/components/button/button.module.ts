import { NgModule } from '@angular/core';
import { RidButtonComponent } from './button.component';
import { RidLoaderModule } from './../loader/loader.module';
import { CommonModule } from '@angular/common';
import { RidButtonGroupDirective } from './directives/buttons-group.directive';

@NgModule({
  declarations: [RidButtonComponent, RidButtonGroupDirective],
  imports: [RidLoaderModule, CommonModule],
  exports: [RidButtonComponent, RidButtonGroupDirective],
})
export class RidButtonModule { }
