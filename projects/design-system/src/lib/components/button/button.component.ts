import { Component, OnInit, ElementRef, Renderer2, Input, OnChanges, SimpleChanges } from '@angular/core';
import { RidButtonType } from './models/button-type.models';

@Component({
  selector: 'ridButton, [ridButton]',
  exportAs: 'ridButton',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class RidButtonComponent implements OnInit, OnChanges {

  /** Tipo del boton. */
  @Input() btnType: RidButtonType = 'primary';
  /** True si se desea mostrar el loader. */
  @Input() isLoading: boolean = false;
  /** True si se desea hacer compacto. */
  @Input() isCompact: boolean = false;
  /** Clases para el boton. */
  @Input() forceClass: string = '';

  private _btnClasses: string = '';

  get btnClasses(): string {
    return this._btnClasses;
  }

  constructor(
    private _el: ElementRef,
    private _renderer: Renderer2,
  ) { }

  ngOnInit(): void {
    this._setBtnBaseClasses();
    this._setBtnCompactClasses();
    this._setBtnBgClasses(this.btnType);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['btnType']
      && !changes['btnType'].isFirstChange()
      && changes['btnType'].currentValue !== changes['btnType'].previousValue) {
      this._setBtnBgClasses(changes['btnType'].currentValue, changes['btnType'].previousValue);
    }
    if (changes['isCompact']
      && !changes['isCompact'].isFirstChange()
      && changes['isCompact'].currentValue !== changes['isCompact'].previousValue) {
      this._setBtnCompactClasses();
    }
    if (changes['isLoading']
      && !changes['isLoading'].isFirstChange()
      && changes['isLoading'].currentValue !== changes['isLoading'].previousValue) {
      if (this._el.nativeElement.disabled !== changes['isLoading'].currentValue) {
        this._renderer.setProperty(this._el.nativeElement, 'disabled', changes['isLoading'].currentValue);
      }
    }
  }

  private _setBtnBaseClasses(): void {
    this._renderer.addClass(this._el.nativeElement, 'btn');
    this._renderer.addClass(this._el.nativeElement, 'bg-transparent');
    this._renderer.addClass(this._el.nativeElement, 'p-0');
    this._renderer.addClass(this._el.nativeElement, 'border-0');
    this._renderer.addClass(this._el.nativeElement, 'rounded-3');
    this._renderer.addClass(this._el.nativeElement, 'position-relative');
  }

  private _setBtnCompactClasses(): void {
    if (this.isCompact) {
      this._btnClasses = this._btnClasses.replace(' text-size-5', '');
      this._btnClasses += ' text-size-2';
    } else {
      this._btnClasses = this._btnClasses.replace(' text-size-2', '');
      this._btnClasses += ' text-size-5';
    }
  }

  private _setBtnBgClasses(current: RidButtonType, previous?: RidButtonType): void {
    let newClass, oldClass = '';
    if (previous) {
      if (previous === 'primary') {
        oldClass = 'btn-dark';
      } else if (previous === 'secondary') {
        oldClass = 'btn-light';
      } else {
        oldClass = `btn-${previous}`;
      }
    }
    if (current === 'primary') {
      newClass = 'btn-dark';
    } else if (current === 'secondary') {
      newClass = 'btn-light';
    } else {
      newClass = `btn-${current}`;
    }
    this._btnClasses = this._btnClasses.replace(oldClass, '');
    this._btnClasses += ' ' + newClass;
  }

}
