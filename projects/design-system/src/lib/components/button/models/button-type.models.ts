/** Posibles tipos de botones */
export type RidButtonType = 'primary' | 'secondary' | 'danger' | 'warning' | 'info' | 'success';
