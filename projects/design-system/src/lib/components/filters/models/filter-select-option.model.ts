export interface RidFilterSelectOption {
  /** Texto que se mostrará como la opcion del selector. */
  text: string;
  /** Valor que se devolverá al seleccionar la opcion del selector. */
  value: any;
  /** En caso de ser selector multiple, se utilizará esta opcion. */
  selected?: boolean;
}
