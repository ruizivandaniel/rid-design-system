export interface RidFilterRangeDateConfigValue {
  /** Label del campo. */
  label?: string;
  /** Description del campo. */
  description?: string;
}

export interface RidFilterRangeDateConfig {
  /** Configuracion para la fecha desde. */
  from?: RidFilterRangeDateConfigValue;
  /** Configuracion para la fecha hasta. */
  to?: RidFilterRangeDateConfigValue;
  /** Fecha minima a seleccionar. */
  minDate?: Date;
  /** Fecha maxima a seleccionar. */
  maxDate?: Date;
}

export interface RidFilterRangeDateType {
  /** Valor de la fecha desde. */
  from: Date;
  /** Valor de la fecha hasta. */
  to: Date;
}
