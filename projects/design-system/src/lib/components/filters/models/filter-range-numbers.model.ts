export interface RidFilterRangeNumberField {
  /** Label del campo. */
  label?: string;
  /** Description del campo. */
  description?: string;
}

export interface RidFilterRangeNumberConfig {
  /** Configuracion para el input number desde. */
  from?: RidFilterRangeNumberField;
  /** Configuracion para el input number hasta. */
  to?: RidFilterRangeNumberField;
  /** Valor minimo a seleccionar. */
  min?: number;
  /** Valor maximo a seleccionar. */
  max?: number;
}

export interface RidFilterRangeNumberType {
  /** Valor del rango desde. */
  from: number;
  /** Valor del rango hasta. */
  to: number;
}
