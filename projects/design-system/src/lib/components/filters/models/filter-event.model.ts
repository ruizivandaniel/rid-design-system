export interface RidFilterStatus<T> {
  /** Identificador unico del filtro. */
  id: string;
  /** Valor actual del filtro. */
  currentValue: T | null;
  /** Valor previo del filtro. */
  previousValue: T | null;
  /** True si es la primera vez que se edita el valor del filtro. */
  isFirstChange: boolean;
}
