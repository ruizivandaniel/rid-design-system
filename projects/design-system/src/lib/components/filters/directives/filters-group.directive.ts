import { AfterViewInit, ContentChild, ContentChildren, Directive, Input, OnDestroy, QueryList } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { RidFilterBaseDirective } from './filter-base.directive';
import { RidFilterStatus } from '../models/filter-event.model';
import { RidFiltersResetterComponent } from '../components/filters-resetter/filters-resetter.component';

@Directive({
  selector: 'ridFilterListener, [ridFilterListener]',
  exportAs: 'ridFilterListener',
})
export class RidFilterListenerDirective implements AfterViewInit, OnDestroy {

  /** Handler que se ejecutará al modificarse los filtros. */
  @Input('ridFilterListener') onChangesHandler!: (currentStatus: RidFilterStatus<any>[]) => void;

  @ContentChildren(RidFilterBaseDirective, {
    read: RidFilterBaseDirective,
    descendants: false,
    emitDistinctChangesOnly: true
  })
  private _filters!: QueryList<RidFilterBaseDirective<any>>;
  @ContentChild(RidFiltersResetterComponent, {
    read: RidFiltersResetterComponent,
    descendants: false,
    static: false,
  })
  private _resetter!: RidFiltersResetterComponent;

  private _resetterUnsubscriber = new Subject<void>();
  private _filtersUnsubscriber = new Subject<void>();

  ngAfterViewInit(): void {
    this._subscribeToFilters(this._filters.toArray());
    this._filters.changes
      .pipe(takeUntil(this._filtersUnsubscriber))
      .subscribe((newFilters: QueryList<RidFilterBaseDirective<any>>) => {
        this._filtersUnsubscriber.next();
        this._subscribeToFilters(newFilters.toArray());
      });
    if (this._resetter) {
      this._subscribeToResetter();
    }
  }

  ngOnDestroy(): void {
    this._resetterUnsubscriber.next();
    this._resetterUnsubscriber.complete();
    this._filtersUnsubscriber.next();
    this._filtersUnsubscriber.complete();
  }

  /** Ejecuta el reset de todos los filtros actuales. */
  resetFilters(): void {
    this._filters.forEach((f: RidFilterBaseDirective<any>) => f.reset(true));
    this.checkCurrentStatus();
  }

  /**
   * Agrupa el status de todos los filtros y ejecuta el handler
   * pasando como parametro dichos status.
   */
  checkCurrentStatus(): void {
    const currentStatus: RidFilterStatus<any>[] = this._filters
      .map((f: RidFilterBaseDirective<any>) => f.status);
    this.onChangesHandler(currentStatus);
  }

  /**
   * Realiza la suscripcion a cambios por parte de cada filtro
   * y ejecuta el handler pasado por input con el status actual
   * de cada uno de los filtros.
   * @param filters filtros a suscribirse.
   */
  private _subscribeToFilters(filters: RidFilterBaseDirective<any>[]): void {
    filters
      .forEach((filter: RidFilterBaseDirective<any>) => {
        filter.onChange
          .pipe(takeUntil(this._filtersUnsubscriber))
          .subscribe((_: RidFilterStatus<any>) => this.checkCurrentStatus());
      });
  }
  /**
   * Realiza la suscripcion al evento del resetter (en caso de
   * existir) y ejecuta el reset de todos los filtros encontrados.
   */
  private _subscribeToResetter(): void {
    this._resetter.onFilter
      .pipe(takeUntil(this._resetterUnsubscriber))
      .subscribe(() => this.resetFilters());
  }

}
