import { Directive, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { RidFilterStatus } from '../models/filter-event.model';
import { v4 } from 'uuid';

/**
 * Clase general que debe ser implementada por aquellos
 * componentes que quieren proveer un filtro. Para mas
 * informacion ver como ejemplo "filter-input.component".
 */
@Directive({
  selector: 'ridFilterBase',
  exportAs: 'ridFilterBase',
})
export class RidFilterBaseDirective<T> implements OnInit, OnChanges {
  /** Identificador del filtro. */
  @Input() id!: string;
  /** Label del filtro. */
  @Input() label: string = '';
  /** Valor por defecto del filtro. */
  @Input() default!: T;
  /** True si el filtro esta deshabilitado. */
  @Input() isDisabled: boolean = false;
  /** Informa cuando se modifica el valor del filtro. */
  @Output() onChange = new EventEmitter<RidFilterStatus<T>>();
  /** True si el menu esta expandido o no. */
  @Output() onExpand = new EventEmitter<boolean>();

  /** True si el filtro esta deshabilitado. */
  protected _disabled: boolean = false;
  /** True si el menu del filtro esta expandido. */
  protected _isExpanded: boolean = false;
  /** True si es el primer cambio de valor del filtro. */
  protected _firstChange: boolean = false;
  /** Status actual del filtro. */
  protected _status: RidFilterStatus<T> = {
    id: '',
    currentValue: null,
    previousValue: null,
    isFirstChange: true,
  };

  /** Obtener el status del filtro. */
  get status(): RidFilterStatus<T> {
    return this._status;
  }

  /** Obtener el valor actual del filtro. */
  get currentValue(): T | null {
    return this._status.currentValue;
  }

  /** Obtener el valor previo del filtro. */
  get previousValue(): T | null {
    return this._status.previousValue;
  }

  /** True si el filtro se encuentra deshabilitado. */
  get disabled(): boolean {
    return this._disabled;
  }

  /** True si el menu del filtro debe estar expandido. */
  get isExpanded(): boolean {
    return this._isExpanded;
  }

  /** Iniciador del filtro, en caso de override, ejecutar el 'super.ngOnInit'. */
  ngOnInit(): void {
    this._status.id = this.id || v4();
    if (this.default) {
      this.setValue(this.default);
    }
  }

  /** Listener de cambios del filtro, en caso de override, ejecutar el 'super.ngOnChanges'. */
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['isDisabled']
      && !changes['isDisabled'].isFirstChange()
      && changes['isDisabled'].currentValue !== changes['isDisabled'].previousValue) {
      this._disabled = changes['isDisabled'].currentValue;
    }
  }

  /** Realiza el reseteo de valores del filtro. */
  reset(omitEvent: boolean = false): void {
    this._status.previousValue = this._status.currentValue;
    this._status.currentValue = this.default || null;
    !omitEvent && this.onChange.emit(this._status);
  }

  /** Cambia el valor actual de menu expandido. */
  toggle(): void {
    this._isExpanded = !this._isExpanded;
    this.onExpand.emit(this._isExpanded);
  }

  /** Setea al menu expandido en true. */
  expand(): void {
    if (!this.isExpanded) {
      this._isExpanded = true;
      this.onExpand.emit(this._isExpanded);
    }
  }

  /** Setea al menu expandido en false. */
  contract(): void {
    if (this.isExpanded) {
      this._isExpanded = false;
      this.onExpand.emit(this._isExpanded);
    }
  }

  /** Actualiza el status actual del filtro. */
  setValue(newValue: T): void {
    if (this._status.currentValue) {
      this._status.previousValue = this._status.currentValue;
    }
    if (this._status.isFirstChange && this._status.previousValue !== null) {
      this._status.isFirstChange = false;
    }
    this._status.currentValue = newValue;
    this.onChange.emit(this._status);
  }

}
