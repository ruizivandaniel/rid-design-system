import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RidInputModule } from '../input/input.module';
import { RidButtonModule } from '../button/button.module';
import { RidDropdownModule } from '../dropdown/dropdown.module';
import { RidCheckModule } from '../check/check.module';
import { RidLabelModule } from '../label/label.module';
import { RidTextTruncateModule } from '../text-truncate/text-truncate.module';
import { RidFilterBaseDirective } from './directives/filter-base.directive';
import { RidFilterListenerDirective } from './directives/filters-group.directive';
import { RidFilterInputComponent } from './components/filter-input/filter-input.component';
import { RidFilterSelectComponent } from './components/filter-select/filter-select.component';
import { RidFiltersResetterComponent } from './components/filters-resetter/filters-resetter.component';
import { RidFilterRangeDateComponent } from './components/filter-range-date/filter-range-date.component';
import { RidFilterRangeNumbersComponent } from './components/filter-range-numbers/filter-range-numbers.component';

@NgModule({
  declarations: [
    RidFilterBaseDirective,
    RidFilterListenerDirective,
    RidFilterInputComponent,
    RidFilterSelectComponent,
    RidFiltersResetterComponent,
    RidFilterRangeDateComponent,
    RidFilterRangeNumbersComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RidInputModule,
    RidButtonModule,
    RidDropdownModule,
    RidCheckModule,
    RidTextTruncateModule,
    RidLabelModule,
  ],
  exports: [
    RidFilterBaseDirective,
    RidFilterListenerDirective,
    RidFilterInputComponent,
    RidFilterSelectComponent,
    RidFiltersResetterComponent,
    RidFilterRangeDateComponent,
    RidFilterRangeNumbersComponent,
  ],
})
export class RidFiltersModule { }
