import { Component, Input } from '@angular/core';
import { RidFilterBaseDirective } from './../../directives/filter-base.directive';
import { DropdownToggledEvent } from './../../../dropdown/models/dropdown-toggled-event.model';

@Component({
  selector: 'rid-filter-input',
  templateUrl: './filter-input.component.html',
  providers: [
    {
      provide: RidFilterBaseDirective,
      useExisting: RidFilterInputComponent,
    }
  ]
})
export class RidFilterInputComponent extends RidFilterBaseDirective<string> {

  /** Descripcion que se mostrará debajo del input. */
  @Input() description!: string;
  /** Placeholder del input. */
  @Input() placeHolder!: string;

  onToggledDropdown(toggled: DropdownToggledEvent): void {
    toggled.opened ? this.expand() : this.contract();
  }

}
