
import { Component, Input } from '@angular/core';
import { RidFilterBaseDirective } from './../../directives/filter-base.directive';
import { DropdownToggledEvent } from './../../../dropdown/models/dropdown-toggled-event.model';
import { RidFilterRangeNumberConfig, RidFilterRangeNumberType } from '../../models/filter-range-numbers.model';

@Component({
  selector: 'rid-filter-range-numbers',
  templateUrl: './filter-range-numbers.component.html',
  providers: [
    {
      provide: RidFilterBaseDirective,
      useExisting: RidFilterRangeNumbersComponent,
    }
  ]
})
export class RidFilterRangeNumbersComponent extends RidFilterBaseDirective<RidFilterRangeNumberType> {


  /** Descripcion que se mostrará debajo del input. */
  @Input() description!: string;
  /** Placeholder del input. */
  @Input() placeHolder: string = 'Seleccionar rango de fechas...';
  /** Configuracion del filter range. */
  @Input() config!: RidFilterRangeNumberConfig;

  protected _to!: number;
  protected _from!: number;
  protected _min!: number;
  protected _max!: number;

  override ngOnInit(): void {
    super.ngOnInit();
    if (this.default) {
      this._to = this.default.to;
      this._from = this.default.from;
    }
    if (this.config.min) {
      this._min = this.config.min;
    }
    if (this.config.max) {
      this._max = this.config.max;
    }
  }

  onToggledDropdown(toggled: DropdownToggledEvent): void {
    toggled.opened ? this.expand() : this.contract();
  }

  onMinChange(): void {
    if (this.config.min && this._from < this.config.min) {
      this._from = this.config.min;
    } else if (this._to && this._from > this._to) {
      this._from = this._to;
    } else if (this.config.max && this._from > this.config.max) {
      this._from = this.config.max;
    }
  }

  onMaxChange(): void {
    if (this.config.max && this._to > this.config.max) {
      this._to = this.config.max;
    } else if (this._from && this._to < this._from) {
      this._to = this._from;
    } else if (this.config.min && this._to < this.config.min) {
      this._to = this.config.min;
    }
  }


  onCancel(): void {
    if (this.currentValue?.to) {
      this._to = this.currentValue.to;
    }
    if (this.currentValue?.from) {
      this._from = this.currentValue.from;
    }
  }

  onConfirm(): void {
    this.setValue({
      from: this._from,
      to: this._to,
    });
  }

}
