
import { Component, Input, OnInit } from '@angular/core';
import { RidFilterBaseDirective } from './../../directives/filter-base.directive';
import { RidFilterRangeDateConfig, RidFilterRangeDateType } from '../../models/filter-range-date.model';
import { DropdownToggledEvent } from './../../../dropdown/models/dropdown-toggled-event.model';
import { DateTime } from 'luxon';

@Component({
  selector: 'rid-filter-range-date',
  templateUrl: './filter-range-date.component.html',
  providers: [
    {
      provide: RidFilterBaseDirective,
      useExisting: RidFilterRangeDateComponent,
    }
  ]
})
export class RidFilterRangeDateComponent extends RidFilterBaseDirective<RidFilterRangeDateType> implements OnInit {

  /** Descripcion que se mostrará debajo del input. */
  @Input() description!: string;
  /** Placeholder del input. */
  @Input() placeHolder: string = 'Seleccionar rango de fechas...';
  /** Configuracion del filter range. */
  @Input() config!: RidFilterRangeDateConfig;

  protected _to!: string;
  protected _from!: string;
  protected _minDate!: string;
  protected _maxDate!: string;

  override ngOnInit(): void {
    super.ngOnInit();
    if (this.default) {
      this._to = DateTime.fromJSDate(this.default.to)
        .toFormat('yyyy-MM-dd');
      this._from = DateTime.fromJSDate(this.default.from)
        .toFormat('yyyy-MM-dd');
    }
    if (this.config.minDate) {
      this._minDate = DateTime.fromJSDate(this.config.minDate)
        .toFormat('yyyy-MM-dd');
    }
    if (this.config.maxDate) {
      this._maxDate = DateTime.fromJSDate(this.config.maxDate)
        .toFormat('yyyy-MM-dd');
    }
  }

  onToggledDropdown(toggled: DropdownToggledEvent): void {
    toggled.opened ? this.expand() : this.contract();
  }

  onCancel(): void {
    if (this.currentValue?.to) {
      this._to = DateTime.fromJSDate(this.currentValue.to)
        .toFormat('yyyy-MM-dd');
    }
    if (this.currentValue?.from) {
      this._from = DateTime.fromJSDate(this.currentValue.from)
        .toFormat('yyyy-MM-dd');
    }
  }

  onConfirm(): void {
    this.setValue({
      from: DateTime.fromFormat(this._from, 'yyyy-MM-dd').toJSDate(),
      to: DateTime.fromFormat(this._to, 'yyyy-MM-dd').toJSDate(),
    });
  }

}
