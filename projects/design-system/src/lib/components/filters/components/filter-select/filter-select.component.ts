import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { RidFilterSelectOption } from '../../models/filter-select-option.model';
import { RidFilterBaseDirective } from '../../directives/filter-base.directive';
import { DropdownToggledEvent } from './../../../dropdown/models/dropdown-toggled-event.model';
import { RidCheckComponent } from './../../../check/check.component';

@Component({
  selector: 'rid-filter-select',
  templateUrl: './filter-select.component.html',
  providers: [
    {
      provide: RidFilterBaseDirective,
      useExisting: RidFilterSelectComponent,
    }
  ]
})
export class RidFilterSelectComponent extends RidFilterBaseDirective<any> implements OnInit {
  /** Placeholder del selector. */
  @Input() placeHolder: string = 'Seleccionar opcion...';
  /** Opciones para mostrar en el selector. */
  @Input() options: RidFilterSelectOption[] = [];
  /** True si se desea poder seleccionar mas de una opcion. */
  @Input() isMultiple: boolean = false;
  /** (isMultiple true) Ocultar opcion para seleccionar todas las opciones. */
  @Input() showOptionAll: boolean = true;
  /** (isMultiple true) Texto que mostrará la opcion para seleccionar todas las opciones. */
  @Input() optionAllLabel: string = 'Seleccionar todas';

  @ViewChild('checkAll', { read: RidCheckComponent, static: false }) private _checkAllRef!: RidCheckComponent;

  private _selectedOption!: RidFilterSelectOption | RidFilterSelectOption[] | null;
  private _originalOpts: RidFilterSelectOption[] = [];

  get selectedText(): string {
    if (Array.isArray(this._selectedOption)) {
      const aux = this._selectedOption.length;
      return aux ? `${aux} seleccionados` : this.placeHolder;
    }
    return this._selectedOption?.text ?? this.placeHolder;
  }

  override ngOnInit(): void {
    super.ngOnInit();
    if (this.isMultiple) {
      this.options.map(o => ({ ...o, selected: o.selected ?? false }));
      this._originalOpts = structuredClone(this.options);
      const filtered = this._originalOpts.filter(o => o.selected);
      this.default = filtered ? filtered.map(o => o.value) : null;
      if (this.options.some(o => o.selected)) {
        this.updateMultipleOptions();
      }
    }
  }

  onToggledDropdown(toggled: DropdownToggledEvent): void {
    toggled.opened ? this.expand() : this.contract();
  }

  /**
   * Actualiza el valor actual en base a la opcion
   * seleccionada e informa del cambio.
   */
  selectOption(option: RidFilterSelectOption): void {
    if (!this._selectedOption
      || option.text !== (this._selectedOption as RidFilterSelectOption).text) {
      this.setValue(option.value);
      this._selectedOption = option as RidFilterSelectOption;
    }
  }

  /**
   * Selecciona o deselecciona todas las opciones.
   */
  toggleAll(checked: boolean): void {
    this.options.map(o => o.selected = checked);
  }

  /**
   * (Opcion isMultiple true) Actualiza el valor actual en base a las opciones
   * seleccionadas e informa de los cambios.
   */
  updateMultipleOptions(): void {
    const selectedOptions = this.options.filter(o => o.selected);
    if ((this.currentValue?.join(',') ?? '') === selectedOptions.map(o => o.value).join(',')) {
      return;
    }
    this.setValue(selectedOptions.map(o => o.value));
    this._selectedOption = selectedOptions as RidFilterSelectOption[];
  }

  checkToggleAll(): void {
    if (!this._checkAllRef) {
      return;
    }
    this._checkAllRef.updateState(
      this.options.every(o => o.selected === true),
      true
    );
  }

  override reset(omitEvent: boolean = false): void {
    if (this.isMultiple) {
      this.options = structuredClone(this._originalOpts);
    }
    this._selectedOption = this.default || null;
    super.reset(omitEvent);
  }

}

