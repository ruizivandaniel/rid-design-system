import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'rid-filters-resetter',
  templateUrl: './filters-resetter.component.html',
})
export class RidFiltersResetterComponent {

  /** Texto que mostrará el boton. */
  @Input() label = 'Limpiar filtros';
  /** True si se desea deshabilitar. */
  @Input() disabled: boolean = false;
  /** Evento que informa de una peticion de filtrado. */
  @Output() onFilter = new EventEmitter<void>();

}

