import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidChipsComponent } from './chips.component';
import { RidSvgIconsModule } from '../svg-icons/svg-icons.module';

@NgModule({
  declarations: [RidChipsComponent],
  imports: [
    CommonModule,
    RidSvgIconsModule,
  ],
  exports: [RidChipsComponent],
})
export class RidChipsModule { }
