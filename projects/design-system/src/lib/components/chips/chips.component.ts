import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { RidChip } from './models/chip.model';
import { v4 } from 'uuid';

@Component({
  selector: 'rid-chips',
  templateUrl: 'chips.component.html',
})
export class RidChipsComponent implements OnInit, OnChanges {
  /** Elementos a mostrar por el componente. */
  @Input() chips: RidChip[] = [];
  /** Two Way Binding de Input chips. */
  @Output() chipsChange = new EventEmitter<RidChip[]>();

  ngOnInit(): void {
    this._checkChips();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['chips'] && !changes['chips'].isFirstChange()) {
      this._checkChips();
    }
  }

  removeChip(chip: RidChip): void {
    const index = this.chips.findIndex((i) => i.id === chip.id);
    if (index !== -1) {
      this.chips.splice(index, 1);
      this.chipsChange.emit(this.chips);
    }
  }

  private _checkChips(): void {
    this.chips = this.chips.map((i: RidChip) => {
      return {
        id: v4(),
        type: 'primary',
        removable: false,
        ...i,
      } as RidChip;
    });
    this.chipsChange.emit(this.chips);
  }

}
