import { RidChipType } from './chip-type.model';

export interface RidChip {
  /** Texto a mostrar por el chip. */
  label: string;
  /** Icono a mostrar en el chip. */
  iconUrl?: string;
  /** Identificador del chip (por default se toma el label) */
  id?: string;
  /** Tipo de chip (define el estilo) */
  type?: RidChipType;
  /** True para que se visualice la cruz en el chip. */
  removable?: boolean;
  /** Metadata que se necesite pasar (por ejemplo cuando se remueve) */
  context?: any;
}
