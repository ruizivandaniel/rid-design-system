export type RidChipType = 'primary' | 'secondary' | 'info' | 'danger' | 'success' | 'warning';
