import { Component, Input, OnChanges, OnInit, SimpleChanges, OnDestroy } from '@angular/core';
import { RidAlertType, RidAlertsService, RidAlertEvent } from '@rid/core';
import { RidAlertClosableType } from './models/alert-closable-types.model';
import { Subscription, timer } from 'rxjs';
import { RidAlert } from './models/alert.model';

@Component({
  selector: 'rid-alert',
  templateUrl: 'alert.component.html',
})
export class RidAlertComponent implements OnInit, OnChanges, OnDestroy {
  /** Tipo de la alerta (en caso de no utilizar el servicio). */
  @Input() type: RidAlertType = 'danger';
  /** Titulo de la alerta (en caso de no utilizar el servicio). */
  @Input() title: string = '';
  /** Mensaje de la alerta (en caso de no utilizar el servicio). */
  @Input() message: string = '';
  /** True si se desea mostrar el icono del alerta. */
  @Input() showIcon: boolean = true;
  /** True si se desea mostrar la hora de la notificacion. */
  @Input() showTime: boolean = false;
  /** Formas de poder eliminar el alerta. */
  @Input() closable: RidAlertClosableType = 'none';
  /** True si se desea escuchar cambios del servicio de alertas. */
  @Input() listening: boolean = true;

  private _currentAlert: RidAlert | null = null;
  private _timerSubs!: Subscription;
  private _listeningSub!: Subscription;

  get currentAlert(): RidAlert | null {
    return this._currentAlert;
  }

  constructor(private _alertsService: RidAlertsService) { }

  ngOnInit(): void {
    if (this.listening) {
      this._subscribeToNewAlerts();
    } else {
      this._currentAlert = {
        type: this.type,
        iconUrl: `assets/${this._getAlertTypeIcon(this.type)}.svg`,
        title: this.title,
        message: this.message,
        time: this._setTime(),
      };
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['listening']
      && !changes['listening'].isFirstChange()
      && changes['listening'].previousValue !== changes['listening'].currentValue) {
      this.listening ? this._subscribeToNewAlerts() : this._unsubscribeToNewAlerts();
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeToNewAlerts();
  }

  clearCurrentAlert(): void {
    this._currentAlert = null;
  }

  private _setTime(): string {
    const x = new Date();
    const h = x.getHours();
    const m = x.getMinutes();
    const hh = h >= 10 ? h : `0${h}`;
    const mm = m >= 10 ? m : `0${m}`;
    return `${hh}:${mm}`;
  }

  private _unsubscribeToNewAlerts(): void {
    this.clearCurrentAlert();
    if (this._listeningSub) {
      this._listeningSub.unsubscribe();
    }
  }

  private _subscribeToNewAlerts(): void {
    this._unsubscribeToNewAlerts();
    this._listeningSub = this._alertsService.onNotification
      .subscribe((newAlert: RidAlertEvent) => {
        if (this._timerSubs) {
          this._timerSubs.unsubscribe();
        }
        this._setAlert(newAlert);
        if (newAlert.closeDelay > 0) {
          this._timerSubs = timer(newAlert.closeDelay)
            .subscribe(() => {
              this.clearCurrentAlert();
            });
        }
      });
  }

  private _setAlert(newAlert: RidAlertEvent): void {
    this._currentAlert = {
      type: newAlert.type,
      iconUrl: `assets/${this._getAlertTypeIcon(newAlert.type)}.svg`,
      message: newAlert.message || '',
      title: newAlert.title,
      time: this._setTime(),
    };
  }

  private _getAlertTypeIcon(type: RidAlertType): string {
    switch (type) {
      case 'success':
        return 'check-circle';
      case 'warning':
        return 'exclamation-circle';
      case 'danger':
        return 'x-circle';
      case 'info':
        return 'question-circle';
      default:
        return 'x-circle';
    }
  }

}
