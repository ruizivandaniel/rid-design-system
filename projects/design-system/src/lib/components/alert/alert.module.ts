import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidSvgIconsModule } from './../svg-icons/svg-icons.module';
import { RidAlertComponent } from './alert.component';

@NgModule({
  declarations: [RidAlertComponent],
  imports: [
    CommonModule,
    RidSvgIconsModule,
  ],
  exports: [RidAlertComponent],
})
export class RidAlertModule { }
