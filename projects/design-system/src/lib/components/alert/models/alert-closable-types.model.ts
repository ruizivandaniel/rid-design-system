/** Posibles tipos de cierres de una alerta. */
export type RidAlertClosableType = 'none' | 'times' | 'click';
