import { RidAlertType } from '@rid/core';

export interface RidAlert {
  /** Titulo de la alerta. */
  title: string;
  /** Mensaje de la alerta. */
  message: string;
  /** Horario de la alerta. */
  time: string;
  /** Clase de la alerta. */
  type: RidAlertType;
  /** Url del icono de la alerta. */
  iconUrl: string;
}
