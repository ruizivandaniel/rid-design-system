import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RidClickedOutsideDirective } from '@rid/core';
import { RidDropdownComponent } from './dropdown.component';
import { RidDropdownFooterDirective } from './directives/dropdown-footer.directive';
import { RidDropdownHeaderDirective } from './directives/dropdown-header.directive';
import { RidDropdownToggleDirective } from './directives/dropdown-toggle.directive';
import { RidSvgIconsModule } from './../svg-icons/svg-icons.module';

@NgModule({
  declarations: [
    RidDropdownComponent,
    RidDropdownFooterDirective,
    RidDropdownHeaderDirective,
    RidDropdownToggleDirective,
  ],
  imports: [
    CommonModule,
    RidClickedOutsideDirective,
    RidSvgIconsModule,
  ],
  exports: [
    RidDropdownComponent,
    RidDropdownFooterDirective,
    RidDropdownHeaderDirective,
    RidDropdownToggleDirective,
  ],
})
export class RidDropdownModule { }
