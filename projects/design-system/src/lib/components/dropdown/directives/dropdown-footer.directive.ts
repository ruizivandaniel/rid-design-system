import { Directive, TemplateRef } from '@angular/core';

@Directive({ selector: 'ridDropdownFooter, [ridDropdownFooter]' })
export class RidDropdownFooterDirective {
  get template(): TemplateRef<any> {
    return this._templateRef;
  }
  constructor(private _templateRef: TemplateRef<any>) { }
}
