import { Directive, TemplateRef } from '@angular/core';

@Directive({ selector: 'ridDropdownHeader, [ridDropdownHeader]' })
export class RidDropdownHeaderDirective {
  get template(): TemplateRef<any> {
    return this._templateRef;
  }
  constructor(private _templateRef: TemplateRef<any>) { }
}
