import { Directive, TemplateRef } from '@angular/core';

@Directive({ selector: 'ridDropdownToggle, [ridDropdownToggle]' })
export class RidDropdownToggleDirective {
  get template(): TemplateRef<any> {
    return this._templateRef;
  }
  constructor(private _templateRef: TemplateRef<any>) { }
}
