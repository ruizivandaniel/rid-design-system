/** Posibles tipos de labels */
export type RidDropdownItemType = 'primary' | 'secondary' | 'danger' | 'warning' | 'info' | 'success';
