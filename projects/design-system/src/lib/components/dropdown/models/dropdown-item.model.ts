import { RidDropdownItemType } from './dropdown-item-type.model';

export interface RidDropdownItem {
  /** Label del dropdown. */
  label: string;
  /** Url del icono del dropdown. */
  iconUrl?: string;
  /** Accion del dropdown. */
  action: () => void;
  /** Tipo del dropdown. */
  type?: RidDropdownItemType;
}
