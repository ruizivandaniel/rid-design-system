export interface DropdownToggledEvent {
  /** Estado actual del dropdown.  */
  opened: boolean;
  /** Informacion adicional que se envia al cerrar el dropdown. */
  context?: any;
}
