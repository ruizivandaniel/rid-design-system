import {
  Component, ContentChild, ElementRef, EventEmitter, Input, Output,
  Renderer2, TemplateRef, ViewChild, OnChanges, SimpleChanges,
  AfterViewInit,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';
import { Subject, takeUntil, timer } from 'rxjs';
import { RidDropdownFooterDirective } from './directives/dropdown-footer.directive';
import { RidDropdownHeaderDirective } from './directives/dropdown-header.directive';
import { RidDropdownToggleDirective } from './directives/dropdown-toggle.directive';
import { RidDropdownAlignment } from './models/dropdown-alignment.model';
import { RidDropdownItem } from './models/dropdown-item.model';
import { DropdownToggledEvent } from './models/dropdown-toggled-event.model';

@Component({
  selector: 'rid-dropdown',
  templateUrl: 'dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class RidDropdownComponent implements AfterViewInit, OnChanges, OnDestroy {
  /** Muestra/oculta el chevron que indica si el dropdown esta abierto o no. */
  @Input() showChevron: boolean = true;
  /** Hace que el chevron se vea blanco, en vez de negro. */
  @Input() chevronLight: boolean = false;
  /** Items a mostrar en el body del dropdown. */
  @Input() items: RidDropdownItem[] = [];
  /** Ancho maximo permitido (ej: 500 o '500px') */
  @Input() maxWidth!: number;
  /** Ancho minimo permitido (ej: 500 o '500px') */
  @Input() minWidth!: number;
  /** Alineacion desde donde se mostrará el menu. */
  @Input() alignment: RidDropdownAlignment = 'left';
  /** Alinea verticalmente el chevron. */
  @Input() alignChevron: 'start' | 'center' | 'end' = 'center';
  /** Evento que se emite al abrir o cerrar el dropdown. */
  @Output() toggled = new EventEmitter<DropdownToggledEvent>();

  @ContentChild(TemplateRef, { read: RidDropdownFooterDirective, descendants: false })
  footerTemp!: RidDropdownFooterDirective;
  @ContentChild(TemplateRef, { read: RidDropdownHeaderDirective, descendants: false })
  headerTemp!: RidDropdownHeaderDirective;
  @ContentChild(TemplateRef, { read: RidDropdownToggleDirective, descendants: false })
  toggleTemp!: RidDropdownToggleDirective;

  @ViewChild('dropdownToggle')
  private _dropdownToggle!: ElementRef<any>;
  @ViewChild('dropdownMenu')
  private _dropdownMenu!: ElementRef<any>;

  private _opened: boolean = false;
  private _menuMinWidth: string = 'max-content';
  private _menuMaxWidth: string = 'max-content';
  private _unsubscriber = new Subject<void>();

  get menuMinWidth(): string {
    return this._menuMinWidth;
  }

  get menuMaxWidth(): string {
    return this._menuMaxWidth;
  }

  get isOpen(): boolean {
    return this._opened;
  }

  constructor(
    private _renderer: Renderer2,
    private _cdr: ChangeDetectorRef,
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['alignment']
      && !changes['alignment'].isFirstChange()
      && changes['alignment'].currentValue !== changes['alignment'].previousValue) {
      this._setAlignment();
    }
    if (changes['maxWidth']
      && !changes['maxWidth'].isFirstChange()
      && changes['maxWidth'].currentValue !== changes['maxWidth'].previousValue) {
      this._checkMenuMaxWidth();
      this._cdr.detectChanges();
    }
    if (changes['minWidth']
      && !changes['minWidth'].isFirstChange()
      && changes['minWidth'].currentValue !== changes['minWidth'].previousValue) {
      this._checkMenuMinWidth();
      this._cdr.detectChanges();
    }
  }

  ngAfterViewInit(): void {
    this._checkMenuMinWidth();
    this._checkMenuMaxWidth();
    this._cdr.detectChanges();
  }

  ngOnDestroy(): void {
    this._unsubscriber.next();
    this._unsubscriber.complete();
  }

  toggle(): void {
    this._opened ? this.close() : this.open();
  }

  open(ctx?: any): void {
    if (!this._opened) {
      const sub = timer(1)
        .pipe(takeUntil(this._unsubscriber))
        .subscribe(() => {
          this._setAlignment();
          this._opened = true;
          this.toggled.emit({ opened: true, context: ctx });
          sub.unsubscribe();
        });
    }
  }

  close = (ctx?: any): void => {
    if (this._opened) {
      this._opened = false;
      this.toggled.emit({ opened: false, context: ctx });
    }
  };

  doAction(item: RidDropdownItem): void {
    if (item.action) {
      item.action();
      this.close();
    }
  }

  private _checkMenuMinWidth(): void {
    if (!isNaN(Number(this.minWidth))) {
      this._menuMinWidth = `${this.minWidth}px`;
    }
  }

  private _checkMenuMaxWidth(): void {
    if (!isNaN(Number(this.maxWidth))) {
      this._menuMaxWidth = `${this.maxWidth}px`;
    }
  }

  private _setAlignment(): void {
    let newAlign = 0;
    if (this.alignment !== 'left') {
      const menuOffWidth = this._dropdownMenu.nativeElement.clientWidth;
      const toggleOffWidth = this._dropdownToggle.nativeElement.clientWidth;
      if (menuOffWidth && toggleOffWidth) {
        if (this.alignment === 'center') {
          newAlign =
            toggleOffWidth < menuOffWidth
              ? -(menuOffWidth / 2 - toggleOffWidth / 2)
              : toggleOffWidth / 2 - menuOffWidth / 2;
        } else if (this.alignment === 'right') {
          newAlign =
            toggleOffWidth < menuOffWidth
              ? toggleOffWidth - menuOffWidth
              : -(menuOffWidth - toggleOffWidth);
        }
      }
    }
    this._renderer.setStyle(
      this._dropdownMenu.nativeElement,
      'left',
      `${newAlign}px`
    );
    this._renderer.setStyle(
      this._dropdownMenu.nativeElement,
      'top',
      `${this._dropdownToggle.nativeElement.clientHeight + 5}px`
    );
  }
}
