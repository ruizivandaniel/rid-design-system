import { NgModule } from '@angular/core';

// yo:modules
import { RidLabelModule } from './components/label/label.module';
import { RidKbdModule } from './components/kbd/kbd.module';
import { RidFiltersModule } from './components/filters/filters.module';
import { RidCopyModule } from './components/copy/copy.module';
import { RidContextualMenuModule } from './components/contextual-menu/contextual-menu.module';
import { RidCheckModule } from './components/check/check.module';
import { RidFloatingMenuModule } from './components/floating-menu/floating-menu.module';
import { RidStepperModule } from './components/stepper/stepper.module';
import { RidUploadFileModule } from './components/upload-file/upload-file.module';
import { RidInputModule } from './components/input/input.module';
import { RidSwitchModule } from './components/switch/switch.module';
import { RidChipsModule } from './components/chips/chips.module';
import { RidPaginationModule } from './components/pagination/pagination.module';
import { RidDynamicTableModule } from './components/dynamic-table/dynamic-table.module';
import { RidDropdownModule } from './components/dropdown/dropdown.module';
import { RidBannerModule } from './components/banner/banner.module';
import { RidModalModule } from './components/modal/modal.module';
import { RidAlertModule } from './components/alert/alert.module';
import { RidLoaderModule } from './components/loader/loader.module';
import { RidButtonModule } from './components/button/button.module';
import { RidCardModule } from './components/card/card.module';
import { RidTabsModule } from './components/tabs/tabs.module';
import { RidAccordionModule } from './components/accordion/accordion.module';
import { RidCalendarModule } from './components/calendar/calendar.module';
import { RidCarrouselModule } from './components/carrousel/carrousel.module';
import { RidTextTruncateModule } from './components/text-truncate/text-truncate.module';
import { RidPinCodeModule } from './components/pin-code/pin-code.module';
import { RidLoggedLayoutModule } from './layouts/layouts.module';
import { RidSvgIconsModule } from './components/svg-icons/svg-icons.module';
import { RidTooltipModule } from './directives/tooltip/tooltip.module';

@NgModule({
  exports: [
    // yo:exports
		RidLabelModule,
    RidKbdModule,
    RidFiltersModule,
    RidCopyModule,
    RidContextualMenuModule,
    RidCheckModule,
    RidFloatingMenuModule,
    RidSwitchModule,
    RidChipsModule,
    RidPaginationModule,
    RidDynamicTableModule,
    RidStepperModule,
    RidUploadFileModule,
    RidInputModule,
    RidDropdownModule,
    RidBannerModule,
    RidModalModule,
    RidAlertModule,
    RidLoaderModule,
    RidButtonModule,
    RidCardModule,
    RidTabsModule,
    RidAccordionModule,
    RidCalendarModule,
    RidCarrouselModule,
    RidTextTruncateModule,
    RidPinCodeModule,
    RidLoggedLayoutModule,
    RidSvgIconsModule,
    RidTooltipModule,
  ]
})
export class RidDesignSystemModule { }
