import {
  Directive, ElementRef, OnInit, ViewContainerRef, Input,
  TemplateRef, OnDestroy, OnChanges, SimpleChanges
} from '@angular/core';
import { Overlay, OverlayRef, ConnectedPosition } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import { fromEvent, timer, Subscription } from 'rxjs';
import { RidTooltipPosition } from './models/position.model';
import { RidTooltipOrientation } from './models/orientation.model';
import { RidTooltipConnectedPositions } from './models/connected-position.model';

/**
 * Directiva que recibe un template ref y lo muestra como si fuera un tooltip
 * sobre el elemento donde se la implementa.
 *
 * @param template (required) templateRef que mostrará el tooltip.
 * @param position (default: right-bottom) posicion del tooltip respecto al elemento.
 * @param orientation (default: right) Orientacion del tooltip en base a la posicion donde aparece.
 * @param openDelay (default: 0) tiempo en milisegundos que tardará en abrir el tooltip.
 * @param closeDelay (default: 0) tiempo en milisegundos que tardará en cerrar el tooltip.
 * @param disabled (default: false) true si no se quiere mostrar el tooltip.
 * @param manualHandling (default: false) true si se necesita manejar el tooltip de manera manual.
 *
 * ```html
 * Template de ejemplo:
 * <ng-template #myTemp><p>Mi template</p></ng-template>
 *
 * Uso basico:
 * <p [ridTooltip]="myTemp">Texto ramdom</p>
 *
 * Uso completo:
 * <p [ridTooltip]="myTemp" [position]="'bottom'" [orientation]="'center'" [openDelay]="1000" [closeDelay]="5000">Texto ramdom</p>
 *
 * Acceder a las funciones "open", "close" y "isOpen" obteniendo la referencia de la directiva:
 * <p #appTooltip=ridTooltip [ridTooltip]="myTemp" [manualHandling]="true">Este va a usar el ridTooltip</p>
 * ```
 */
@Directive({
  selector: 'ridTooltip, [ridTooltip]',
  exportAs: 'ridTooltip'
})
export class RidTooltipDirective implements OnInit, OnChanges, OnDestroy {

  @Input('ridTooltip') template!: TemplateRef<any>;
  @Input() position: RidTooltipPosition = 'bottom';
  @Input() orientation: RidTooltipOrientation = 'right';
  @Input() openDelay = 0;
  @Input() closeDelay = 0;
  @Input() disabled = false;
  @Input() manualHandling = false;

  subsEnter!: Subscription;
  subsLeave!: Subscription;
  subsEnterTimer!: Subscription;
  subsLeaveTimer!: Subscription;
  overlayRef!: OverlayRef;
  cursorStillStay: boolean = false;

  get isOpen(): boolean {
    return this.overlayRef && this.overlayRef.hasAttached();
  }

  constructor(private overlay: Overlay,
    private viewContainerRef: ViewContainerRef,
    private elementRef: ElementRef) { }

  ngOnInit(): void {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position()
        .flexibleConnectedTo(this.elementRef.nativeElement)
        .withPositions([this._getPositions()]),
    });
    if (!this.manualHandling) {
      this._setListeners();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['disabled']
      && !changes['disabled'].firstChange
      && changes['disabled'].currentValue !== changes['disabled'].previousValue) {
      this._closeTooltip();
    }
    if (changes['position']
      && !changes['position'].firstChange
      && changes['position'].currentValue !== changes['position'].previousValue) {
      this.updatePositions();
    } else if (changes['orientation']
      && !changes['orientation'].firstChange
      && changes['orientation'].currentValue !== changes['orientation'].previousValue) {
      this.updatePositions();
    }
    if (changes['manualHandling']
      && !changes['manualHandling'].firstChange
      && changes['manualHandling'].currentValue !== changes['manualHandling'].previousValue) {
      if (this.manualHandling) {
        this._unsetListeners();
      } else {
        this._setListeners();
      }
    }
  }

  ngOnDestroy(): void {
    this._unsetListeners();
    if (this.overlayRef) {
      this.overlayRef.dispose();
    }
  }

  /**
   * Realiza la apertura del tooltip manualmente.
   */
  open(on: number = 0): void {
    this._openTooltip(on);
  }

  /**
   * Realiza el cierre del tooltip manualmente, en caso de necesitar un retardo, se puede pasar
   * los milisegundos que se necesiten.
   * @param on (default: 0) Milisegundos en los que tardará en abrirse.
   */
  close(on: number = 0): void {
    this._closeTooltip(on);
  }

  private updatePositions(): void {
    if (this.overlayRef) {
      this.overlayRef.updatePositionStrategy(this.overlay
        .position()
        .flexibleConnectedTo(this.elementRef.nativeElement)
        .withPositions([this._getPositions()])
      );
    }
  }

  private _setListeners(): void {
    this._unsetListeners();
    this.subsEnter = fromEvent(this.elementRef.nativeElement, 'mouseenter')
      .subscribe(() => {
        this.cursorStillStay = true;
        this._openTooltip(this.openDelay);
      });
    this.subsLeave = fromEvent(this.elementRef.nativeElement, 'mouseleave')
      .subscribe(() => {
        this.cursorStillStay = false;
        this._closeTooltip(this.closeDelay);
      });
  }

  private _unsetListeners(): void {
    if (this.subsEnter) { this.subsEnter.unsubscribe(); }
    if (this.subsLeave) { this.subsLeave.unsubscribe(); }
  }

  private _openTooltip(delay: number = 0): void {
    if (!this.overlayRef.hasAttached() && !this.disabled) {
      if (delay !== undefined && delay > 0) {
        if (this.subsEnterTimer) {
          this.subsEnterTimer.unsubscribe();
        }
        this.subsEnterTimer = timer(delay).subscribe(() => {
          if (this.cursorStillStay || this.manualHandling) {
            this.overlayRef.attach(new TemplatePortal(this.template, this.viewContainerRef));
          }
        });
      } else {
        this.overlayRef.attach(new TemplatePortal(this.template, this.viewContainerRef));
      }
    }
  }

  private _closeTooltip(delay: number = 0): void {
    if (this.overlayRef.hasAttached()) {
      if (delay !== undefined && delay > 0) {
        if (this.subsLeaveTimer) {
          this.subsLeaveTimer.unsubscribe();
        }
        this.subsLeaveTimer = timer(delay).subscribe(() => {
          if (!this.cursorStillStay || this.manualHandling) {
            this.overlayRef.detach();
          }
        });
      } else {
        this.overlayRef.detach();
      }
    }
  }

  private _getPositions(): ConnectedPosition {
    const result: RidTooltipConnectedPositions = {};
    const overlayPosToBottom = this._getOverlayPositionToBottom();
    const overlayPosToTop = this._getOverlayPositionToTop();
    switch (this.position) {
      case 'bottom':
        result.originX = 'center';
        result.originY = 'bottom';
        result.overlayX = overlayPosToBottom.overlayX;
        result.overlayY = overlayPosToBottom.overlayY;
        break;
      case 'left-bottom':
        result.originX = 'start';
        result.originY = 'bottom';
        result.overlayX = overlayPosToBottom.overlayX;
        result.overlayY = overlayPosToBottom.overlayY;
        break;
      case 'right-bottom':
        result.originX = 'end';
        result.originY = 'bottom';
        result.overlayX = overlayPosToBottom.overlayX;
        result.overlayY = overlayPosToBottom.overlayY;
        break;
      case 'left-top':
        result.originX = 'start';
        result.originY = 'top';
        result.overlayX = overlayPosToTop.overlayX;
        result.overlayY = overlayPosToTop.overlayY;
        break;
      case 'right-top':
        result.originX = 'end';
        result.originY = 'top';
        result.overlayX = overlayPosToTop.overlayX;
        result.overlayY = overlayPosToTop.overlayY;
        break;
      case 'top':
        result.originX = 'center';
        result.originY = 'top';
        result.overlayX = overlayPosToTop.overlayX;
        result.overlayY = overlayPosToTop.overlayY;
        break;
      case 'left':
        result.originX = 'start';
        result.originY = 'center';
        result.overlayX = 'end';
        result.overlayY = 'center';
        break;
      case 'right':
        result.originX = 'end';
        result.originY = 'center';
        result.overlayX = 'start';
        result.overlayY = 'center';
        break;
    }
    return <ConnectedPosition>{
      originX: result.originX,
      originY: result.originY,
      overlayX: result.overlayX,
      overlayY: result.overlayY
    };
  }

  private _getOverlayPositionToBottom(): { overlayX: 'start' | 'center' | 'end', overlayY: 'top' | 'center' | 'bottom' } {
    switch (this.orientation) {
      case 'left': return { overlayX: 'end', overlayY: 'top' };
      case 'center': return { overlayX: 'center', overlayY: 'top' };
      case 'right': return { overlayX: 'start', overlayY: 'top' };
    }
  }

  private _getOverlayPositionToTop(): { overlayX: 'start' | 'center' | 'end', overlayY: 'top' | 'center' | 'bottom' } {
    switch (this.orientation) {
      case 'left': return { overlayX: 'end', overlayY: 'bottom' };
      case 'center': return { overlayX: 'center', overlayY: 'bottom' };
      case 'right': return { overlayX: 'start', overlayY: 'bottom' };
    }
  }

}
