export type RidTooltipArrowPosition = 'left' | 'top-left' | 'top-center' | 'top-right' | 'right' | 'bottom-right' | 'bottom-center' | 'bottom-left';
