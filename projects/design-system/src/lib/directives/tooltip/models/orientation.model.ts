export type RidTooltipOrientation = 'left' | 'right' | 'center';
