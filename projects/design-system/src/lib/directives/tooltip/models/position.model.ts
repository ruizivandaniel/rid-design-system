export type RidTooltipPosition = 'left-bottom' | 'left-top' | 'top' | 'bottom' | 'right-top' | 'right-bottom' | 'left' | 'right';
