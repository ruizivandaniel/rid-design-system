export interface RidTooltipConnectedPositions {
  originX?: 'start' | 'center' | 'end';
  originY?: 'top' | 'center' | 'bottom';
  overlayX?: 'start' | 'center' | 'end';
  overlayY?: 'top' | 'center' | 'bottom';
}
