/** Posibles tipos de estilo de los arrows del tooltip. */
export type RidTooltipStyle = 'primary' | 'secondary' | 'danger' | 'info' | 'warning' | 'success' | 'light' | 'dark' | 'muted';

/** Posibles tipos de tamaño de los arrows del tooltip. */
export type RidTooltipArrowSize = 'xs' | 'sm' | 'md' | 'lg';

export interface RidTooltipArrowConfig {
  /** Afecta al color que tendrá el arrow. */
  style?: RidTooltipStyle;
  /** Afecta al tamaño del arrow */
  size?: RidTooltipArrowSize;
  /** Afecta al "style.color" del arrow. No utilizar en
   * conjunto con la config "style". */
  color?: string;
}
