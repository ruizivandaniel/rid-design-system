import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { RidTooltipArrowPosition } from './models/tooltip-arrow-position.model';
import { RidTooltipArrowConfig } from './models/tooltip-arrow-config.model';

/**
 * Directiva que se encarga de agregar un "arrow" (El tipo triangulo o cola que
 * sobresale de los mensajes de chat o tips en una app) a un componente.
 * @input tooltipArrow (requerido) posicion en la que posiciona el arrow.
 * @input config (opcional) configuracion del tooltip: size: Tamaño del tooltip,
 * style: acentuacion del color en base a sus clases, color: style.color del
 * elemento (si se utiliza style, este no funciona).
 */
@Directive({
  selector: 'ridTooltipArrow, [ridTooltipArrow]',
  exportAs: 'ridTooltipArrow',
})
export class RidTooltipArrowDirective implements OnInit {
  @Input() config: RidTooltipArrowConfig = { size: 'md' };
  @Input('ridTooltipArrow') position: RidTooltipArrowPosition = 'top-center';

  constructor(private _el: ElementRef, private _render: Renderer2) { }

  ngOnInit(): void {
    this._render.setStyle(this._el.nativeElement, 'position', 'relative');
    const div = this._render.createElement('div');
    this._render.addClass(div, `rid-tooltip-arrow-${this.config.size}-${this.position}`);
    if (this.config.style) {
      this._render.addClass(div, this._getStyle());
    } else if (this.config.color) {
      this._render.setStyle(div, 'color', this.config.color);
    }
    this._render.appendChild(this._el.nativeElement, div);
  }

  private _getStyle(): string {
    return `rid-tooltip-arrow-${this.position.split('-')[0]}-${this.config.style}`;
  }

}
