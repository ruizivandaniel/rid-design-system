import { NgModule } from '@angular/core';
import { OverlayModule } from '@angular/cdk/overlay';

import { RidTooltipArrowDirective } from './tooltip-arrow.directive';
import { RidTooltipDirective } from './tooltip.directive';

@NgModule({
  declarations: [
    RidTooltipArrowDirective,
    RidTooltipDirective,
  ],
  imports: [
    OverlayModule,
  ],
  exports: [
    RidTooltipArrowDirective,
    RidTooltipDirective,
  ],
})
export class RidTooltipModule { }
