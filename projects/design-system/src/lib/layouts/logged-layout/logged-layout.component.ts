import { Component, OnInit } from '@angular/core';
import { RidResponsiveService } from '@rid/core';

@Component({
  selector: 'rid-logged-layout',
  templateUrl: 'logged-layout.component.html'
})
export class RidLoggedLayoutComponent implements OnInit {
  constructor(private _responsiveService: RidResponsiveService) { }
  ngOnInit() {
    console.log(this._responsiveService.is('DESKTOP'));
  }
}
