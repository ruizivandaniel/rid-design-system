import { NgModule } from '@angular/core';
import { RidCoreModule } from '@rid/core';

import { RidLoggedLayoutComponent } from './logged-layout/logged-layout.component';
import { RidUnloggedLayoutComponent } from './unlogged-layout/unlogged-layout.component';

@NgModule({
  exports: [
    RidLoggedLayoutComponent,
    RidUnloggedLayoutComponent,
  ],
  imports: [
    RidCoreModule.forRoot(),
  ],
  declarations: [
    RidLoggedLayoutComponent,
    RidUnloggedLayoutComponent,
  ],
})
export class RidLoggedLayoutModule { }
