var Generator = require('yeoman-generator');

module.exports = class extends Generator {

  constructor(args, opts) {
    super(args, opts);
    this.libPath = 'projects/design-system/src/lib';
    this.dummyAppPath = 'projects/app_dummy/src/app';
  }

  async prompting() {
    this.log('😎 Bienvenido al generador de Design System');
    this.answers = await this.prompt([
      {
        type: 'input',
        name: 'compName',
        message: 'Nombre del componente (sin prefijo)',
      },
    ]);
    this.log('Nombre elegido:', this.answers.compName)
    if (!this.answers || !this.answers.compName) {
      throw new Error('☢️ No se agrego ningun nombre.');
    }
  }

  writing() {
    this._private_createLibFiles();
    this._private_createDummyFiles();
  }

  /** Crea los archivos para la libreria */
  _private_createLibFiles() {
    this.log(`🌱 Creando archivos a la libreria...`);
    // module, componente y ts
    try {
      this.fs.copyTpl(
        this.templatePath('lib-component-html.template'),
        this.destinationPath(`${this.libPath}/components/${this.answers.compName}/${this.answers.compName}.component.html`),
        {
          compName: this.answers.compName,
        }
      );
      this.fs.copyTpl(
        this.templatePath('lib-component-ts.template'),
        this.destinationPath(`${this.libPath}/components/${this.answers.compName}/${this.answers.compName}.component.ts`),
        {
          compName: this.answers.compName,
          nameCamel: this._private_toCamelCase(this.answers.compName),
        }
      );
      this.fs.copyTpl(
        this.templatePath('lib-component-module.template'),
        this.destinationPath(`${this.libPath}/components/${this.answers.compName}/${this.answers.compName}.module.ts`),
        {
          compName: this.answers.compName,
          nameCamel: this._private_toCamelCase(this.answers.compName),
        }
      );
    } catch (e) {
      this.log('☢️ Error al crear los archivos del nuevo componente para la libreria. ERROR: ', e.message);
    }
    // Modificacion de design-system.module.ts
    try {
      const DECLARATION_CONTENT = `\n\t\tRid${this._private_toCamelCase(this.answers.compName)}Module,`;
      const IMPORT_CONTENT = `\nimport { Rid${this._private_toCamelCase(this.answers.compName)}Module } from './components/${this.answers.compName}/${this.answers.compName}.module';`;
      this._private_updateFile(
        `${this.libPath}/design-system.module.ts`,
        [
          {
            search: '// yo:modules',
            content: IMPORT_CONTENT
          },
          {
            search: '// yo:exports',
            content: DECLARATION_CONTENT
          },
        ]
      );
    } catch (e) {
      this.log('☢️ Error al modificar el design-system.module.ts. ERROR: ', e.message);
    }
    // Modificacion de public-api.ts
    try {
      const MODULE_CONTENT = `\nexport * from './lib/components/${this.answers.compName}/${this.answers.compName}.module';`;
      const COMPONENT_CONTENT = `\nexport * from './lib/components/${this.answers.compName}/${this.answers.compName}.component';`;
      this._private_updateFile(
        `${this.libPath}/../public-api.ts`,
        [
          {
            search: "// yo:modules",
            content: MODULE_CONTENT
          },
          {
            search: '// yo:components',
            content: COMPONENT_CONTENT
          },
        ]
      );
    } catch (e) {
      this.log('☢️ Error al modificar el public-api.ts. ERROR: ', e.message);
    }
    this.log('Listo 👌');
  }

  /** Crea los archivos para la libreria */
  async _private_createDummyFiles() {
    this.log(`🌱 Creando archivos al app_dummy...`);
    // Creacion de ts y html
    try {
      this.fs.copyTpl(
        this.templatePath('appdummy-component-html.template'),
        this.destinationPath(`${this.dummyAppPath}/components/${this.answers.compName}-imp/${this.answers.compName}-imp.component.html`),
        {
          compName: this.answers.compName,
          nameCamel: this._private_toCamelCase(this.answers.compName),
        }
      );
      this.fs.copyTpl(
        this.templatePath('appdummy-component-ts.template'),
        this.destinationPath(`${this.dummyAppPath}/components/${this.answers.compName}-imp/${this.answers.compName}-imp.component.ts`),
        {
          compName: this.answers.compName,
          nameCamel: this._private_toCamelCase(this.answers.compName),
        }
      );
    } catch (e) {
      this.log('☢️ Error al crear los archivos para el app_dummy. ERROR: ', e.message);
    }
    // Modificacion de app.module.ts
    try {
      const IMPORT_CONTENT = `\r\nimport { ${this._private_toCamelCase(this.answers.compName)}ImpComponent }`
        + ` from './components/${this.answers.compName}-imp/${this.answers.compName}-imp.component';`;
      const ROUTE_CONTENT = `\n\t{ path: '${this.answers.compName}', component: `
        + `${this._private_toCamelCase(this.answers.compName)}ImpComponent },`;
      const DECLARATION_CONTENT = `\n\t\t${this._private_toCamelCase(this.answers.compName)}ImpComponent,`;
      this._private_updateFile(
        `${this.dummyAppPath}/app.module.ts`,
        [
          {
            search: "// yo:components",
            content: IMPORT_CONTENT
          },
          {
            search: "// yo:routes",
            content: ROUTE_CONTENT
          },
          {
            search: "// yo:declarations",
            content: DECLARATION_CONTENT
          },
        ]
      );
    } catch (e) {
      this.log('☢️ Error al modificar el app.module. ERROR: ', e.message);
    }
    // Modificacion de app.component.ts
    try {
      const ROUTERLINK_CONTENT = `\n\t\t{ path: '/${this.answers.compName}', label: '${this.answers.compName}' },`;
      this._private_updateFile(
        `${this.dummyAppPath}/app.component.ts`,
        [{
          search: '// yo:routerlinks',
          content: ROUTERLINK_CONTENT
        }]
      );
    } catch (e) {
      this.log('☢️ Error al modificar el app.component.ts. ERROR: ', e.message);
    }
    this.log('Listo 👌');
  }

  /**
   * Actualiza el contenido de un archivo agregando por cada change,
   * el contenido en base al search que se le pase en cada objeto de
   * changes.
   * @param filePath ruta del archivo a modificar
   * @param changes array de tipo [{search, content}]. Por cada search
   * encontrado se le agregara como siguiente linea el content.
   */
  async _private_updateFile(filePath, changes) {
    const originalContent = await this.fs.read(filePath);
    let newContent = originalContent;
    changes.forEach(change => {
      let auxIndex = newContent.search(change.search);
      if (auxIndex === -1) {
        throw new Error(`No se encontró el index para el ${change.search}`);
      }
      auxIndex = auxIndex + change.search.length;
      newContent = newContent.substring(0, auxIndex)
        .concat(change.content)
        .concat(newContent.substring(auxIndex));
    });
    await this.fs.write(filePath, newContent);
  }

  /** Realiza la conversion de un text tipo DashCase a UpperCamelCase */
  _private_toCamelCase(text) {
    if (!text) {
      return '';
    }
    let aux = text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
    aux = aux.replace(/-(.)/g, (_, x) => {
      return x.toUpperCase();
    });
    return aux;
  }

};
